package com.junne.myutils.controller;

import com.junne.myutils.qdutils.api.ResultVO;
import com.junne.myutils.qdutils.api.ResultVOUtil;
import com.junne.myutils.dto.SysPermissionDTO;
import com.junne.myutils.entity.SysPermission;
import com.junne.myutils.qdutils.jpa.builder.JpaQueryBuilder;
import com.junne.myutils.service.SysPermissionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 描述:
 * 系统权限Controller
 *
 * @author JUNNE
 * @create 2019-11-18 11:32
 */
@Api(value = "sysPermission",description = "系统权限接口-API")
@RestController
@RequestMapping("/sysPermission")
public class SysPermissionController {

    @Autowired
    private SysPermissionService sysPermissionService;

    @GetMapping("/list")
    @ApiOperation("获取权限列表")
    public ResultVO sysPermissionList(String sysUserId) {
        Map<String, Object> queryMap = new HashMap<>();
        if (sysUserId != null && !"".equalsIgnoreCase(sysUserId.trim()) && !"super_admin".equalsIgnoreCase(sysUserId)){
            queryMap = new JpaQueryBuilder(queryMap).and("roleList-userList:id", sysUserId).build();
        }
        List<SysPermission> sysPermissionList = sysPermissionService.findList(queryMap);
        List<SysPermissionDTO> sysPermissionDTOList = sysPermissionService.convertToSysRoleDTOList(sysPermissionList, new ArrayList<>());
        return ResultVOUtil.success(sysPermissionDTOList);
    }
}
