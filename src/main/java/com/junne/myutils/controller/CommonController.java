package com.junne.myutils.controller;

import com.junne.myutils.common.ConstantsCfg;
import com.junne.myutils.conf.aspect.securityaudit.annotation.SecurityAudit;
import com.junne.myutils.conf.aspect.securityaudit.annotation.SecurityAuditType;
import com.junne.myutils.conf.ehcache.MyEhcacheManager;
import com.junne.myutils.dto.MenuDTO;
import com.junne.myutils.dto.SysUserDTO;
import com.junne.myutils.entity.SysPermission;
import com.junne.myutils.entity.SysUser;
import com.junne.myutils.qdutils.ImgUtil;
import com.junne.myutils.qdutils.ParameterValidator;
import com.junne.myutils.qdutils.SpringUtil;
import com.junne.myutils.qdutils.api.ResultVO;
import com.junne.myutils.qdutils.api.ResultVOUtil;
import com.junne.myutils.qdutils.exception.WarningException;
import com.junne.myutils.service.CommonService;
import com.junne.myutils.service.SysUserService;
import com.junne.myutils.utils.VerifyCodeUtil;
import com.junne.myutils.utils.WebUtil;
import com.junne.myutils.utils.rsa.RSAUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 描述:
 * 公用接口
 *
 * @author JUNNE
 * @create 2019-09-30 14:01
 */
@Api(value = "common",description = "公共接口-API")
@RestController
@RequestMapping("/common")
public class CommonController {

    @Autowired
    private CommonService commonService;

    @Value("${webLogin.limitCount}")
    private Integer limitCount;

    @Value("${webLogin.limitTime}")
    private Integer limitTime;

    @GetMapping("/getRsaPublicKey")
    @ApiOperation("获取RSA公钥")
    public synchronized ResultVO getRsaPublicKey() throws WarningException {
        HttpServletRequest httpServletRequest = WebUtil.getHttpServletRequest();
        if (httpServletRequest != null){
            Map<String, Object> map = null;
            String publicKey = "";
            String privateKey = "";
            try {
                map = RSAUtil.initKey();
                publicKey = RSAUtil.getPublicKey(map);
                privateKey = RSAUtil.getPrivateKey(map);
            } catch (Exception e) {
                throw new WarningException("获取RSA公钥失败");
            }
            httpServletRequest.getSession().setAttribute(ConstantsCfg.RSA_PUBLIC_KEY,publicKey);
            httpServletRequest.getSession().setAttribute(ConstantsCfg.RSA_PRIVATE_KEY,privateKey);
            return ResultVOUtil.success(publicKey);
        }
        return ResultVOUtil.fail("获取RSA公钥失败，请联系管理员");
    }

    @GetMapping("/menuList")
    @ApiOperation("根据登录用户获取菜单列表")
    public ResultVO menuList() throws WarningException {
        SysUserDTO currentLogonUser = WebUtil.getCurrentLogonUser();
        if (currentLogonUser != null){
            String id = currentLogonUser.getId();
            List<SysPermission> sysPermissionList = commonService.menuList(id);
            List<MenuDTO> menuDTOList = new ArrayList<>();
            for (SysPermission sysPermission:sysPermissionList) {
                MenuDTO menuDTO = commonService.convertToMenuDTO(sysPermission, new MenuDTO());
                menuDTOList.add(menuDTO);
            }
            return ResultVOUtil.success(menuDTOList);
        }
        throw new WarningException(HttpStatus.UNAUTHORIZED,"当前用户尚未登录");
    }

    @GetMapping("/getVerifyCode")
    @ApiOperation("获取验证码")
    @ApiImplicitParam(name = "verifyCodeType",value = "验证码类型（0：登录验证码，1：修改密码验证码，不填时默认存到VERIFY_CODE）")
    public ResultVO getVerifyCode(Integer verifyCodeType){
        VerifyCodeUtil verifyCodeUtil = new VerifyCodeUtil();
        //获取验证码图片
        BufferedImage image = verifyCodeUtil.getImage();
        //获取验证码内容
        String verifyCode = verifyCodeUtil.getText();
        // randomCode用于保存随机产生的验证码，以便用户登录后进行验证。
        // 将验证码保存到Session中。
        if (WebUtil.getHttpServletRequest() != null){
            HttpSession session = WebUtil.getHttpServletRequest().getSession();
            if (verifyCodeType != null){
                if (verifyCodeType == 0){
                    session.setAttribute(ConstantsCfg.VERIFY_CODE_TYPE_LOGIN, verifyCode);
                }else if (verifyCodeType == 1){
                    session.setAttribute(ConstantsCfg.VERIFY_CODE_TYPE_CHANGE_PWD, verifyCode);
                }
            }else {
                session.setAttribute(ConstantsCfg.VERIFY_CODE, verifyCode);
            }
            String pngBase64Encode = ImgUtil.getPngBase64Encode(image);
            return ResultVOUtil.success("获取验证码成功",pngBase64Encode);
        }
        return ResultVOUtil.fail("获取验证码失败");
    }

    @PostMapping("/login")
    @ApiOperation("登录系统")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "account",value = "账号"),
            @ApiImplicitParam(name = "password",value = "密码"),
            @ApiImplicitParam(name = "verifyCode",value = "验证码")
    })
    @SecurityAudit(description = "登录系统", securityAuditType = SecurityAuditType.LOGIN)
    public ResultVO login(@RequestParam(value = "account") String account,
                          @RequestParam(value = "password") String password,
                          @RequestParam(value = "verifyCode") String verifyCode){
        HttpServletRequest httpServletRequest = WebUtil.getHttpServletRequest();
        if (httpServletRequest != null){
            //1.登录限制校验
            //前缀，按项目定义，各项目不能重复
            String prefix = ConstantsCfg.EHCACHE_PREFIX;
            String remoteIp = WebUtil.getIpAddress();
            //校验是否已被限制
            String isLimit = (String) SpringUtil.getBean(MyEhcacheManager.class).get(prefix+"_REMOTE_HOST_LIMIT" + "_" + remoteIp);
            if (isLimit != null){
                return ResultVOUtil.fail("登录失败次数过多，登录受限！");
            }
            Integer countNum = (Integer) SpringUtil.getBean(MyEhcacheManager.class).get(prefix+"_LOGON_FAILURE_HOST" + "_" + remoteIp);
            if(countNum == null){
                countNum=0;
            }
            //超出limitCount次后进行登录限制
            if (countNum.equals(limitCount) || countNum>limitCount){
                //限制次数为临界点数
                countNum = limitCount-1;
                //记录登录失败次数
                SpringUtil.getBean(MyEhcacheManager.class).put(prefix+"_LOGON_FAILURE_HOST"+"_"+remoteIp,countNum);
                //设置limitTime秒内限制登录
                SpringUtil.getBean(MyEhcacheManager.class).put(prefix+"_REMOTE_HOST_LIMIT"+"_"+remoteIp,"isLimit",limitTime);
                return ResultVOUtil.fail("登录失败次数过多，登录受限！");
            }
            //2.校验密码有效期
            //3.校验验证码
            String sessionVerifyCode = (String) httpServletRequest.getSession().getAttribute(ConstantsCfg.VERIFY_CODE_TYPE_LOGIN);
            if (sessionVerifyCode == null || !sessionVerifyCode.equalsIgnoreCase(verifyCode)){
                //重置验证码
                httpServletRequest.getSession().removeAttribute(ConstantsCfg.VERIFY_CODE_TYPE_LOGIN);
                //记录登录失败次数
                SpringUtil.getBean(MyEhcacheManager.class).put(prefix+"_LOGON_FAILURE_HOST"+"_"+remoteIp,++countNum);
                return ResultVOUtil.fail("验证码错误");
            }
            //4.账号密码校验
            String rsaPrivateKey = (String) httpServletRequest.getSession().getAttribute(ConstantsCfg.RSA_PRIVATE_KEY);
            String descryPassword = RSAUtil.decryptPassword(rsaPrivateKey, password);
            UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken(account, descryPassword);
            SecurityUtils.getSubject().login(usernamePasswordToken);
            //返回登录用户的相关消息
            SysUser sysUser = (SysUser) SecurityUtils.getSubject().getPrincipal();
            if (sysUser != null){
                SysUserDTO sysUserDTO = SpringUtil.getBean(SysUserService.class).convertToSysUserDTO(sysUser, new SysUserDTO());
                WebUtil.setCurrentLogonUser(sysUserDTO);
                //去掉登录失败次数记录
                SpringUtil.getBean(MyEhcacheManager.class).remove(prefix+"_LOGON_FAILURE_HOST"+"_"+remoteIp);
                //更新当前账号的sessionId
                SpringUtil.getBean(MyEhcacheManager.class).put(ConstantsCfg.SYS_USER_LOGIN_SESSIONID+"_"+sysUserDTO.getUserName(),
                        httpServletRequest.getSession().getId());
                httpServletRequest.getSession().setAttribute(ConstantsCfg.SYS_USER_LOGIN_IP,remoteIp);
                return ResultVOUtil.success(sysUserDTO);
            }
        }
        return ResultVOUtil.fail("登录失败，请联系管理员");
    }

    @GetMapping("/logout")
    @ApiOperation("退出系统")
    @ApiImplicitParam(name = "id",value = "系统用户ID")
    @SecurityAudit(description = "退出系统", securityAuditType = SecurityAuditType.LOGOUT)
    public ResultVO logout(@RequestParam(value = "id") String id) throws WarningException {
        ParameterValidator.paramMustNotBeNull(id);
        HttpServletRequest httpServletRequest = WebUtil.getHttpServletRequest();
        if (httpServletRequest != null){
            SysUserDTO currentLogonUser = WebUtil.getCurrentLogonUser();
            Map<String,Object> map = new HashMap<>();
            map.put("id",id);
            SysUser sysUser = SpringUtil.getBean(SysUserService.class).findOne(map);
            if (sysUser != null && currentLogonUser != null && sysUser.getId().equalsIgnoreCase(currentLogonUser.getId())){
                //shiro的退出方法，调用该方法会重新生成一个session，避免两次登录sessionId重复
                SecurityUtils.getSubject().logout();
                return ResultVOUtil.success("退出系统成功",currentLogonUser);
            }
        }
       return ResultVOUtil.fail("退出系统失败，请联系管理员");
    }

    @GetMapping("/isLogin")
    @ApiOperation("判断用户是否已登录")
    public ResultVO isLogin() throws WarningException {
        HttpServletRequest httpServletRequest = WebUtil.getHttpServletRequest();
        if (httpServletRequest != null){
            SysUserDTO currentLogonUser = WebUtil.getCurrentLogonUser();
            if (currentLogonUser != null){
                String loginSessionId = (String) SpringUtil.getBean(MyEhcacheManager.class).get(ConstantsCfg.SYS_USER_LOGIN_SESSIONID + "_" + currentLogonUser.getUserName());
                String currentSessionId = httpServletRequest.getSession().getId();
                if (currentSessionId.equalsIgnoreCase(loginSessionId)){
                    return ResultVOUtil.success("用户已登录",currentLogonUser);
                }
            }
        }
        throw new WarningException(HttpStatus.UNAUTHORIZED,"当前用户尚未登录");
    }
}
