package com.junne.myutils.controller;

import com.junne.myutils.qdutils.jpa.builder.JpaQueryBuilder;
import com.junne.myutils.qdutils.jpa.builder.QueryType;
import com.junne.myutils.qdutils.api.ResultVO;
import com.junne.myutils.qdutils.api.ResultVOUtil;
import com.junne.myutils.qdutils.exception.WarningException;
import com.junne.myutils.qdutils.jpa.JpaUtil;
import com.junne.myutils.qdutils.ParameterValidator;
import com.junne.myutils.dto.SysJobDTO;
import com.junne.myutils.entity.SysJob;
import com.junne.myutils.service.SysJobService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * 描述:
 * 系统作业Controller
 *
 * @author JUNNE
 * @create 2019-12-02 16:46
 */
@Api(value = "sysJob",description = "系统作业模块-API")
@RestController
@RequestMapping("/sysJob")
public class SysJobController {

    @Autowired
    SysJobService sysJobService;

    /**
     * 按键值查询一个实体对象
     * @param paramKey 键
     * @param paramValue 值
     * @return 返回查询DTO对象
     * @throws WarningException 抛出异常
     */
    @GetMapping("/{paramKey}/{paramValue}")
    @ApiOperation("查询一个系统作业")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "paramKey",value = "进行查询的键，目前仅支持：id"),
            @ApiImplicitParam(name = "paramValue",value = "进行查询的值")
    })
    public ResultVO getOne(@PathVariable("paramKey") String paramKey, @PathVariable("paramValue") String paramValue) throws WarningException {
        Map<String, Object> map = sysJobService.handleParamQueryMap(paramKey, paramValue, new HashMap<>());
        SysJob sysJob = sysJobService.findOne(map);
        SysJobDTO sysJobDTO = sysJobService.convertToSysJobDTO(sysJob, new SysJobDTO());
        return ResultVOUtil.success(sysJobDTO);
    }

    /**
     * 分页查询列表
     * @param map 查询的Map对象
     * @return 返回分页的DTO对象列表
     * @throws WarningException 抛出异常
     */
    @PostMapping("/pageList")
    @ApiOperation("分页查询系统作业列表")
    @ApiImplicitParam(name = "map",value = "分页查询的map查询对象，其中“$currentPage”和“$pageSize”是必填参数")
    public ResultVO getPageList(@RequestBody Map<String,Object> map) throws WarningException {
        ParameterValidator.verifyMapParams(map,"$currentPage","$pageSize");
        Pageable pageable = JpaUtil.getPageable(SysJob.class, map);
        //业务相关查询======开始
        Map<String, Object> queryMap = new JpaQueryBuilder()
                .and("jobName", map.get("jobName"), QueryType.LIKE)
                .and("springBeanName", map.get("springBeanName"), QueryType.LIKE)
                .and("methodName", map.get("methodName"), QueryType.LIKE)
                .build();
        //业务相关查询======结束
        Page<SysJob> pageList = sysJobService.findPageList(queryMap, pageable);
        Page<SysJobDTO> sysJobDTOPage = sysJobService.convertToSysJobDTOPage(pageList);
        return ResultVOUtil.success(sysJobDTOPage);
    }

    /**
     * 添加系统作业
     * @param sysJobDTO 系统作业的DTO对象
     * @return 返回添加的实体对象
     * @throws WarningException 抛出异常
     */
    @PostMapping("/add")
    @ApiOperation("添加系统作业")
    public ResultVO add(@RequestBody SysJobDTO sysJobDTO) throws WarningException {
        boolean checkCron = sysJobService.checkCron(sysJobDTO.getCron());
        if (!checkCron){
            throw new WarningException("非法的时间调度cron公式！");
        }
        SysJob sysJob = sysJobService.convertToSysJob(sysJobDTO, new SysJob());
        sysJob.setId(null);
        SysJob save = sysJobService.save(sysJob);
        SysJobDTO sysJobDTOrs = sysJobService.convertToSysJobDTO(save, new SysJobDTO());
        return ResultVOUtil.success(sysJobDTOrs);
    }

    /**
     * 更新系统作业
     * @param sysJobDTO 系统作业的DTO对象
     * @return 返回更新的实体对象
     * @throws WarningException 抛出异常
     */
    @PutMapping("/update")
    @ApiOperation("更新系统作业")
    public ResultVO update(@RequestBody SysJobDTO sysJobDTO) throws WarningException {
        boolean checkCron = sysJobService.checkCron(sysJobDTO.getCron());
        if (!checkCron){
            throw new WarningException("非法的时间调度cron公式！");
        }
        SysJob sysJob = sysJobService.findById(sysJobDTO.getId());
        if (sysJob != null){
            SysJob sysJobNew = sysJobService.convertToSysJob(sysJobDTO, sysJob);
            SysJob update = sysJobService.update(sysJobNew);
            SysJobDTO sysJobDTOrs = sysJobService.convertToSysJobDTO(update, new SysJobDTO());
            return ResultVOUtil.success(sysJobDTOrs);
        }else {
            throw new WarningException("该主键的对象不存在");
        }
    }

    /**
     * 删除实体对象
     * @param id 主键ID
     * @return 返回操作提示
     * @throws WarningException 抛出异常
     */
    @DeleteMapping("/delete")
    @ApiOperation("删除系统作业")
    @ApiImplicitParam(name = "id",value = "系统作业的主键ID")
    public ResultVO delete(@RequestParam(value = "id") String id) throws WarningException {
        ParameterValidator.paramMustNotBeNull(id);
        boolean existsById = sysJobService.checkIfExistsById(id);
        if (existsById){
            sysJobService.delete(id);
            return ResultVOUtil.success();
        }else {
            throw new WarningException("该主键的对象不存在");
        }
    }

    /**
     * 启动作业
     * @param id 主键ID
     * @return 返回操作提示
     * @throws WarningException 抛出异常
     */
    @GetMapping("/start")
    @ApiOperation("启动系统作业")
    @ApiImplicitParam(name = "id",value = "系统作业的主键ID")
    public ResultVO start(@RequestParam(value = "id") String id) throws WarningException {
        ParameterValidator.paramMustNotBeNull(id);
        boolean existsById = sysJobService.checkIfExistsById(id);
        if (existsById){
            SysJob start = sysJobService.start(id);
            return ResultVOUtil.success(start);
        }else {
            throw new WarningException("该主键的对象不存在");
        }
    }

    /**
     * 停止作业
     * @param id 主键ID
     * @return 返回操作提示
     * @throws WarningException 抛出异常
     */
    @GetMapping("/stop")
    @ApiOperation("停止系统作业")
    @ApiImplicitParam(name = "id",value = "系统作业的主键ID")
    public ResultVO stop(@RequestParam(value = "id") String id) throws WarningException {
        ParameterValidator.paramMustNotBeNull(id);
        boolean existsById = sysJobService.checkIfExistsById(id);
        if (existsById){
            SysJob stop = sysJobService.stop(id);
            return ResultVOUtil.success(stop);
        }else {
            throw new WarningException("该主键的对象不存在");
        }
    }
}
