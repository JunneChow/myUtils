package com.junne.myutils.controller;

import com.junne.myutils.qdutils.jpa.builder.JpaQueryBuilder;
import com.junne.myutils.qdutils.jpa.builder.QueryType;
import com.junne.myutils.qdutils.api.ResultVO;
import com.junne.myutils.qdutils.api.ResultVOUtil;
import com.junne.myutils.qdutils.exception.WarningException;
import com.junne.myutils.dto.SystemLogDTO;
import com.junne.myutils.entity.SystemLog;
import com.junne.myutils.service.SystemLogService;
import com.junne.myutils.qdutils.jpa.JpaUtil;
import com.junne.myutils.qdutils.ParameterValidator;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * 描述:
 * 系统日志Controller
 *
 * @author JUNNE
 * @create 2019-11-14 16:02
 */
@Api(value = "sysLog",description = "系统日志模块-API")
@RestController
@RequestMapping("/sysLog")
public class SystemLogController {

    @Autowired
    private SystemLogService systemLogService;

    @PostMapping("/pageList")
    @RequiresPermissions("sys:log:query")
    @ApiOperation("分页查询系统日志")
    public ResultVO getPageList(@RequestBody Map<String,Object> map) throws WarningException {
        ParameterValidator.verifyMapParams(map,"$currentPage","$pageSize");
        Pageable pageable = JpaUtil.getPageable(SystemLog.class, map);
        //业务相关查询======开始
        Map<String, Object> queryMap = new JpaQueryBuilder()
                .and("actionTime", map.get("startTime"), QueryType.START_TIME)
                .and("actionTime", map.get("endTime"), QueryType.END_TIME)
                .build();
        //业务相关查询======结束
        Page<SystemLog> pageList = systemLogService.findPageList(queryMap, pageable);
        Page<SystemLogDTO> systemLogDTOPage = systemLogService.convertToSystemLogDTOPage(pageList);
        return ResultVOUtil.success(systemLogDTOPage);
    }
}
