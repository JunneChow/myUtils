package com.junne.myutils.controller;

import com.junne.myutils.qdutils.api.ResultVO;
import com.junne.myutils.qdutils.api.ResultVOUtil;
import com.junne.myutils.dto.SysRoleDTO;
import com.junne.myutils.entity.SysRole;
import com.junne.myutils.qdutils.jpa.builder.JpaQueryBuilder;
import com.junne.myutils.service.SysRoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 描述:
 * 系统角色Controller
 *
 * @author JUNNE
 * @create 2019-11-18 11:31
 */
@Api(value = "sysRole",description = "系统角色接口-API")
@RestController
@RequestMapping("/sysRole")
public class SysRoleController {

    @Autowired
    private SysRoleService sysRoleService;

    @GetMapping("/list")
    @ApiOperation("获取角色列表")
    public ResultVO sysRoleList(String sysUserId) {
        Map<String, Object> queryMap = new HashMap<>();
        if (sysUserId != null && !"".equalsIgnoreCase(sysUserId.trim()) && !"super_admin".equalsIgnoreCase(sysUserId)){
            queryMap = new JpaQueryBuilder(queryMap).and("userList:id", sysUserId).build();
        }
        List<SysRole> sysRoleList = sysRoleService.findList(queryMap);
        List<SysRoleDTO> sysRoleDTOList = sysRoleService.convertToSysRoleDTOList(sysRoleList, new ArrayList<>());
        return ResultVOUtil.success(sysRoleDTOList);
    }
}
