package com.junne.myutils.controller;

import com.junne.myutils.qdutils.jpa.builder.JpaQueryBuilder;
import com.junne.myutils.qdutils.jpa.builder.QueryType;
import com.junne.myutils.qdutils.api.ResultVO;
import com.junne.myutils.qdutils.api.ResultVOUtil;
import com.junne.myutils.qdutils.exception.WarningException;
import com.junne.myutils.dto.SysUserDTO;
import com.junne.myutils.entity.SysUser;
import com.junne.myutils.service.SysUserService;
import com.junne.myutils.qdutils.jpa.JpaUtil;
import com.junne.myutils.qdutils.ParameterValidator;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;


/**
 * 描述:
 * 用户模块Controller
 *
 * @author JUNNE
 * @create 2019-08-29 14:24
 */
@Api(value = "sysUser",description = "系统用户模块-API")
@RestController
@RequestMapping("/sysUser")
public class SysUserController {

    @Autowired
    private SysUserService sysUserService;

    /**
     * 按键值查询一个实体对象
     * @param paramKey 键
     * @param paramValue 值
     * @return 返回查询DTO对象
     * @throws WarningException 抛出异常
     */
    @GetMapping("/{paramKey}/{paramValue}")
    @ApiOperation("查询一个系统用户")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "paramKey",value = "进行查询的键，目前仅支持：id"),
            @ApiImplicitParam(name = "paramValue",value = "进行查询的值")
    })
    public ResultVO getOne(@PathVariable("paramKey") String paramKey, @PathVariable("paramValue") String paramValue) throws WarningException {
        Map<String, Object> map = sysUserService.handleParamQueryMap(paramKey, paramValue, new HashMap<>());
        SysUser sysUser = sysUserService.findOne(map);
        SysUserDTO sysUserDTO = sysUserService.convertToSysUserDTO(sysUser, new SysUserDTO());
        return ResultVOUtil.success(sysUserDTO);
    }

    /**
     * 分页查询列表
     * @param map 查询的Map对象
     * @return 返回分页的DTO对象列表
     * @throws WarningException 抛出异常
     */
    @PostMapping("/pageList")
    @ApiOperation("分页查询系统用户列表")
    @ApiImplicitParam(name = "map",value = "分页查询的map查询对象，其中“$currentPage”和“$pageSize”是必填参数")
    public ResultVO getPageList(@RequestBody Map<String,Object> map) throws WarningException {
        ParameterValidator.verifyMapParams(map,"$currentPage","$pageSize");
        Pageable pageable = JpaUtil.getPageable(SysUser.class, map);
        //业务相关查询======开始
        Map<String, Object> queryMap = new JpaQueryBuilder()
                .and("userName", map.get("userName"), QueryType.LIKE)
                .and("nickName", map.get("nickName"), QueryType.LIKE)
                .and("sysRoleList:id", map.get("sysRoleId"), QueryType.LIKE)
                .build();
        //业务相关查询======结束
        Page<SysUser> pageList = sysUserService.findPageList(queryMap, pageable);
        Page<SysUserDTO> sysUserDTOPage = sysUserService.convertToSysUserDTOPage(pageList);
        return ResultVOUtil.success(sysUserDTOPage);
    }

    /**
     * 新增实体对象
     * @param sysUserDTO DTO对象
     * @return 返回新增的DTO对象
     */
    @PostMapping("/add")
    @ApiOperation("添加系统用户")
    public ResultVO add(@RequestBody SysUserDTO sysUserDTO){
        SysUser sysUser = sysUserService.convertToSysUser(sysUserDTO, new SysUser());
        sysUser.setId(null);
        SysUser save = sysUserService.save(sysUser);
        SysUserDTO sysUserDTOrs = sysUserService.convertToSysUserDTO(save, new SysUserDTO());
        return ResultVOUtil.success(sysUserDTOrs);
    }

    /**
     * 更新实体对象
     * @param sysUserDTO DTO对象
     * @return 返回更新的DTO对象
     * @throws WarningException 抛出异常
     */
    @PutMapping("/update")
    @ApiOperation("更新系统用户")
    public ResultVO update(@RequestBody SysUserDTO sysUserDTO) throws WarningException {
        SysUser sysUser = sysUserService.findById(sysUserDTO.getId());
        if (sysUser != null){
            SysUser sysUserNew = sysUserService.convertToSysUser(sysUserDTO, sysUser);
            SysUser update = sysUserService.update(sysUserNew);
            SysUserDTO sysUserDTOrs = sysUserService.convertToSysUserDTO(update, new SysUserDTO());
            return ResultVOUtil.success(sysUserDTOrs);
        }else {
            throw new WarningException("该主键的对象不存在");
        }
    }

    /**
     * 删除实体对象
     * @param id 主键ID
     * @return 返回操作提示
     * @throws WarningException 抛出异常
     */
    @DeleteMapping("/delete")
    @ApiOperation("删除系统用户")
    @ApiImplicitParam(name = "id",value = "系统用户的主键ID")
    public ResultVO delete(@RequestParam(value = "id") String id) throws WarningException {
        ParameterValidator.paramMustNotBeNull(id);
        boolean existsById = sysUserService.checkIfExistsById(id);
        if (existsById){
           sysUserService.deleteById(id);
           return ResultVOUtil.success();
        }else {
            throw new WarningException("该主键的对象不存在");
        }
    }

}
