package com.junne.myutils.etc.algorithm;

/**
 * 描述:
 * 插入排序算法
 *
 * @author JUNNE
 * @create 2019-11-28 15:13
 */
public class InsertionSort extends BaseSort{

    public static void sort(Comparable[] a) {
        int N = a.length;
        for (int i=1;i<N;i++){
            for (int j=i;j>0 && less(a[j],a[j-1]);j--){
                exch(a,j,j-1);
            }
        }
    }

    public static void main(String[] args) {
        int len = 10000;
        Comparable[] a = RandomArr.generate(len, 100);
        Comparable[] b = new Comparable[len];
        for (int i=0;i<len;i++) {
            b[i] = a[i];
        }
        long aStart = System.currentTimeMillis();
        InsertionSort.sort(a);
        long aTotalTime = System.currentTimeMillis() - aStart;
        System.out.println("a执行时间："+aTotalTime+"毫秒！");
        isSorted(a);

        long bStart = System.currentTimeMillis();
        SelectionSort.sort(b);
        long bTotalTime = System.currentTimeMillis() - bStart;
        System.out.println("b执行时间："+bTotalTime+"毫秒！");
        isSorted(b);

    }
}
