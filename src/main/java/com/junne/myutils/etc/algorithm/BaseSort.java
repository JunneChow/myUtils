package com.junne.myutils.etc.algorithm;

/**
 * 描述:
 * 排序算法抽象类
 *
 * @author JUNNE
 * @create 2019-11-28 14:29
 */
public class BaseSort {

    /**
     * 判断是否小于
     * @param v 比较数V
     * @param w 比较数W
     * @return 返回布尔值：true:小于，false:大于
     */
    public static boolean less(Comparable v,Comparable w){
        return v.compareTo(w) < 0;
    }

    /**
     * 数组中交换位置
     * @param a 数组
     * @param i 位置i
     * @param j 位置j
     */
    public static void exch(Comparable[] a,int i,int j){
        Comparable t = a[i];
        a[i] = a[j];
        a[j] = t;
    }

    /**
     * 显示数组数据
     * @param a 数组
     */
    public static void show(Comparable[] a){
        for(int i=0;i<a.length;i++){
            System.out.print(a[i]+" ");
        }
    }

    /**
     * 判断数组是否已排序
     * @param a 数组
     * @return 返回布尔值：true:已排序，false:未排序
     */
    public static boolean isSorted(Comparable[] a){
        for(int i=1;i<a.length;i++){
            if (BaseSort.less(a[i],a[i-1])){
                System.out.println("isSorted:false");
                return false;
            }
        }
        System.out.println("isSorted:true");
        return true;
    }
}
