package com.junne.myutils.etc.algorithm;

/**
 * 描述:
 * 随机数组
 *
 * @author JUNNE
 * @create 2019-11-28 15:18
 */
public class RandomArr {

    public static Comparable[] generate(int len, int max){
        Comparable[] arr = new Comparable[len];
        for (int i=0;i<len;i++){
            arr[i] = (int)(Math.random()*max);
        }
        return arr;
    }
}
