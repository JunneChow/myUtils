package com.junne.myutils.etc.algorithm;

/**
 * 描述:
 * 快速排序算法
 *
 * @author JUNNE
 * @create 2019-11-28 17:22
 */
public class QuickSort extends BaseSort {

    public static void sort(Comparable[] a){
        QuickSort.sort(a,0,a.length-1);
    }

    public static void sort(Comparable[] a,int lo,int hi){
        //当比较到最后的时候
        if(hi<=lo) {
            return;
        }
        //找出切分元素
        int j = partition(a,lo,hi);
        //将左部排序
        sort(a,lo,j-1);
        //将右部排序
        sort(a,j+1,hi);
    }

    //切分方法
    private static int partition(Comparable[] a, int lo, int hi) {
        int i = lo, j = hi+1;
        Comparable v = a[lo];
        while(true) {
            //找出左边区域小于切分元素的元素(i<v)
            while(less(a[++i],v)) {
                if(i == hi) {
                    break;
                }
            }
            //找出右边区域大于切分元素的元素(v<j)
            while(less(v,a[--j])) {
                if(j == lo) {
                    break;
                }
            }
            //当左右对比元素相遇时退出循环
            if(i>=j) {
                break;
            }
            //将左边小于切分元素的元素与右边大于切分元素的元素进行交互，保证：左边元素!>切分元素，切分元素!>右边元素
            exch(a,i,j);
        }
        //排序最后交互切分元素，即确定切分元素的准确位置并变更下一个切分元素
        exch(a,lo,j);
        return j;
    }

    public static void main(String[] args) {
        int len = 10000;
        Comparable[] a = RandomArr.generate(len, 100);
        Comparable[] b = new Comparable[len];
        System.arraycopy(a, 0, b, 0, len);

        long aStart = System.currentTimeMillis();
        QuickSort.sort(a);
        long aTotalTime = System.currentTimeMillis() - aStart;
        System.out.println("a执行时间："+aTotalTime+"毫秒！");
        isSorted(a);

        long bStart = System.currentTimeMillis();
        MergeSort.sort(b);
        long bTotalTime = System.currentTimeMillis() - bStart;
        System.out.println("b执行时间："+bTotalTime+"毫秒！");
        isSorted(b);
    }
}
