package com.junne.myutils.etc.algorithm;

/**
 * 描述:
 * 选择排序算法
 *
 * @author JUNNE
 * @create 2019-11-28 14:46
 */
public class SelectionSort extends BaseSort {

    public static void sort(Comparable[] a) {
        int N = a.length;
        for (int i=0;i<N;i++){
            int min = i;
            for (int j=i+1;j<N;j++){
                if (less(a[j],a[min])){
                    min = j;
                }
            }
            exch(a,i,min);
        }
    }

    public static void main(String[] args) {
        Comparable[] a = RandomArr.generate(10000, 100);
        long start = System.currentTimeMillis();
        SelectionSort.sort(a);
        long totalTime = System.currentTimeMillis() - start;
        System.out.println("执行时间："+totalTime+"毫秒！");
        isSorted(a);
    }
}
