package com.junne.myutils.etc.algorithm;

/**
 * 描述:
 * 希尔排序
 *
 * @author JUNNE
 * @create 2019-11-28 15:44
 */
public class ShellSort extends BaseSort{

    public static void sort(Comparable[] a) {
        int N = a.length;
        int h = 1;
        while(h<N/3){
            h=3*h+1;
        }
        while (h>=1){
            //将数组变为h有序
            for (int i=1;i<N;i++){
                for (int j=i;j>=h && less(a[j],a[j-h]);j-=h){
                    exch(a,j,j-h);
                }
            }
            h=h/3;
        }
    }

    public static void main(String[] args) {
        int len = 10000;
        Comparable[] a = RandomArr.generate(len, 100);
        Comparable[] b = new Comparable[len];
        for (int i=0;i<len;i++) {
            b[i] = a[i];
        }
        long aStart = System.currentTimeMillis();
        ShellSort.sort(a);
        long aTotalTime = System.currentTimeMillis() - aStart;
        System.out.println("a执行时间："+aTotalTime+"毫秒！");
        isSorted(a);

        long bStart = System.currentTimeMillis();
        InsertionSort.sort(b);
        long bTotalTime = System.currentTimeMillis() - bStart;
        System.out.println("b执行时间："+bTotalTime+"毫秒！");
        isSorted(b);

    }
}
