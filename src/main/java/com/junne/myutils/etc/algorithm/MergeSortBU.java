package com.junne.myutils.etc.algorithm;

/**
 * 描述:
 * 自底向上归并排序
 *
 * @author JUNNE
 * @create 2019-11-28 17:05
 */
public class MergeSortBU extends BaseSort{

    private static Comparable[] aux;

    public static void sort(Comparable[] a){
        int N = a.length;
        aux = new Comparable[N];
        //控制归并的跨度：1,2,4,8,16,32...
        for (int sz=1;sz<N;sz=sz*2){
            for (int lo=0;lo<N-sz;lo+=sz*2){
                MergeSortBU.merge(a,lo,lo+sz-1,Math.min(lo+sz*2-1,N-1));
            }
        }
    }

    //原地归并
    public static void merge(Comparable[] a,int lo,int mid,int hi) {
        int i=lo,j=mid+1;
        for(int k=lo;k<=hi;k++) {
            aux[k]=a[k];
        }
        for(int k=lo;k<=hi;k++) {
            //1.左尽取右
            if(i>mid) {
                a[k]=aux[j++];
                //2.右尽取左
            }else if(j>hi) {
                a[k]=aux[i++];
                //3.左比右小，取左
            }else if(less(aux[j],aux[i])) {
                a[k]=aux[j++];
                //4.右小或等左，取右
            }else {
                a[k]=aux[i++];
            }
        }
    }

    public static void main(String[] args) {
        int len = 10000;
        Comparable[] a = RandomArr.generate(len, 100);
        Comparable[] b = new Comparable[len];
        System.arraycopy(a, 0, b, 0, len);

        long aStart = System.currentTimeMillis();
        MergeSortBU.sort(a);
        long aTotalTime = System.currentTimeMillis() - aStart;
        System.out.println("a执行时间："+aTotalTime+"毫秒！");
        isSorted(a);

        long bStart = System.currentTimeMillis();
        MergeSort.sort(b);
        long bTotalTime = System.currentTimeMillis() - bStart;
        System.out.println("b执行时间："+bTotalTime+"毫秒！");
        isSorted(b);
    }
}
