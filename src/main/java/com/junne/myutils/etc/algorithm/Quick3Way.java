package com.junne.myutils.etc.algorithm;

/**
 * 描述:
 * 三向切分快速排序
 *
 * @author JUNNE
 * @create 2019-11-28 17:58
 */
public class Quick3Way extends BaseSort {

    public static void sort(Comparable[] a){
        QuickSort.sort(a,0,a.length-1);
    }

    public static void sort(Comparable[] a,int lo,int hi){
        if (hi<=lo){
            return;
        }
        int lt=lo,i=lo+1,gt=hi;
        Comparable v = a[lo];
        while(i<=gt) {
            int cmp = a[i].compareTo(v);
            //i<切分元素v，i与lt交换，保证lo->lt内的元素<切分元素v
            if(cmp<0) {
                exch(a,lt++,i++);
                //i>切分元素v，i与gt交换，保证gt->hi内的元素>切分元素v
            }else if(cmp>0) {
                exch(a,i,gt--);
                //i=切分元素v,不交换，但i继续+1
            }else {
                i++;
            }
        }
        sort(a,lo,lt-1);
        sort(a,gt+1,hi);
    }

    public static void main(String[] args) {
        int len = 10000;
        Comparable[] a = RandomArr.generate(len, 100);
        Comparable[] b = new Comparable[len];
        System.arraycopy(a, 0, b, 0, len);

        long aStart = System.currentTimeMillis();
        Quick3Way.sort(a);
        long aTotalTime = System.currentTimeMillis() - aStart;
        System.out.println("a执行时间："+aTotalTime+"毫秒！");
        isSorted(a);

        long bStart = System.currentTimeMillis();
        MergeSort.sort(b);
        long bTotalTime = System.currentTimeMillis() - bStart;
        System.out.println("b执行时间："+bTotalTime+"毫秒！");
        isSorted(b);
    }
}
