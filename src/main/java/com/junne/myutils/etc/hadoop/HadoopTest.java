package com.junne.myutils.etc.hadoop;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.*;
import org.apache.hadoop.io.IOUtils;
import org.apache.hadoop.util.Progressable;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.net.URI;

/**
 * 描述:
 * hadoop连接测试
 *
 * @author JUNNE
 * @create 2019-11-26 17:16
 */
public class HadoopTest {

    public static final String HDFS_PATH = "hdfs://172.16.5.171:9000";

    FileSystem fileSystem = null;
    Configuration configuration = null;

    @Test
    public void mkdir() throws Exception{
        fileSystem.mkdirs(new Path("/junne/chow"));
    }

    @Test
    public void create() throws Exception{
        FSDataOutputStream fsDataOutputStream = fileSystem.create(new Path("/junne/chow/test.txt"));
        fsDataOutputStream.write("hello world".getBytes());
        fsDataOutputStream.flush();
        fsDataOutputStream.close();
    }

    @Test
    public void cat() throws Exception{
        FSDataInputStream open = fileSystem.open(new Path("/junne/chow/test.txt"));
        IOUtils.copyBytes(open,System.out,1024);
        System.out.println();
        open.close();
    }

    @Test
    public void rename() throws Exception{
        Path oldPath = new Path("/junne/chow/test.txt");
        Path newPath = new Path("/junne/chow/hello.txt");
        fileSystem.rename(oldPath,newPath);
    }

    @Test
    public void copyFromLocalFile() throws Exception{
        Path localPath = new Path("D:/迅雷下载/jdk-8u231-linux-x64.tar.gz");
        Path hdfsPath = new Path("/junne/chow");
        fileSystem.copyFromLocalFile(localPath,hdfsPath);
    }

    @Test
    public void copyFromLocalFileWithProgress() throws Exception{
        BufferedInputStream in = new BufferedInputStream(new FileInputStream(new File("D:/迅雷下载/jdk-8u231-linux-x64.tar.gz")));
        FSDataOutputStream out = fileSystem.create(new Path("/junne/chow/jdk1.8.tar.gz"), new Progressable() {
            @Override
            public void progress() {
                System.out.print("=");
            }
        });
        IOUtils.copyBytes(in,out,4096);
    }

    @Test
    public void copyToLocalFile() throws Exception{
        Path localPath = new Path("D:/test");
        Path hdfsPath = new Path("/junne/chow/hello.txt");
        fileSystem.copyToLocalFile(hdfsPath,localPath);
    }

    @Test
    public void listFiles() throws Exception{
        FileStatus[] fileStatuses = fileSystem.listStatus(new Path("/junne/chow"));
        for (FileStatus fileStatus:fileStatuses) {
            String isFloder = fileStatus.isDirectory() ? "文件夹" : "文件";
            short replication = fileStatus.getReplication();
            long len = fileStatus.getLen();
            String path = fileStatus.getPath().toString();
            System.out.println(isFloder + "\t"
                    + replication + "\t"
                    + len + "\t"
                    + path + "\t");
        }
    }

    @Test
    public void delete() throws Exception{
        boolean delete = fileSystem.delete(new Path("/junne/chow"), true);
        if (delete){
            System.out.println("删除成功！");
        }else {
            System.out.println("删除失败！");
        }
    }

    @Before
    public void setUp() throws Exception {
        System.out.println("HadoopTest...setUp");
        configuration = new Configuration();
        fileSystem = FileSystem.get(new URI(HDFS_PATH),configuration,"root");
    }

    @After
    public void tearDown(){
        configuration = null;
        fileSystem = null;
        System.out.println("HadoopTest...tearDown");
    }
}
