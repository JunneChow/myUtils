package com.junne.myutils.etc.hadoop;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;
import java.net.URI;

/**
 * 描述:
 * 单词计算MapReduce测试
 *
 * @author JUNNE
 * @create 2019-11-27 14:22
 */
public class WordCountTest {

    public static final String HDFS_PATH = "hdfs://172.16.5.171:9000";

    /**
     * Map：读取文件
     */
    public static class MyMapper extends Mapper<LongWritable,Text,Text,LongWritable> {
        LongWritable one  = new LongWritable(1);

        @Override
        protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
            String line = value.toString();
            String[] split = line.split(" ");
            for (String word:split) {
                context.write(new Text(word),one);
            }
        }
    }

    /**
     * Reduce：归并操作
     */
    public static class MyReducer extends Reducer<Text,LongWritable,Text,LongWritable>{
        @Override
        protected void reduce(Text key, Iterable<LongWritable> values, Context context) throws IOException, InterruptedException {
            long sum  = 0;
            for (LongWritable value:values) {
                sum += value.get();
            }
            context.write(key,new LongWritable(sum));
        }
    }

    /**
     * 定义主函数，进行mapReduce
     * @param args 入参
     * @throws Exception 抛出异常
     */
    public static void main(String[] args) throws Exception{

        //创建Configuration
        Configuration configuration = new Configuration();

        //清理已存在的输出文件
        Path outPutPath = new Path(args[1]);
        FileSystem fileSystem = FileSystem.get(new URI(HDFS_PATH), configuration, "root");
        if (fileSystem.exists(outPutPath)){
            fileSystem.delete(outPutPath,true);
            System.out.println("outPut file is exist,but it has deleted");
        }

        //创建Job
        Job job = Job.getInstance(configuration, "wordCount");

        //设置Job的处理类
        job.setJarByClass(WordCountTest.class);

        //设置作业的处理路径
        FileInputFormat.setInputPaths(job,new Path(args[0]));

        //设置map相关的参数
        job.setMapperClass(MyMapper.class);
        job.setMapOutputKeyClass(Text.class);
        job.setOutputValueClass(LongWritable.class);

        //设置reduce相关的参数
        job.setReducerClass(MyReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(LongWritable.class);

        //设置作业处理的输出路径
        FileOutputFormat.setOutputPath(job,new Path(args[1]));

        //判断退出
        System.exit(job.waitForCompletion(true)?0:1);
    }
}
