package com.junne.myutils.service;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigInteger;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * 描述:
 * 实体管理service
 *
 * @author JUNNE
 * @create 2020-03-25 11:21
 */
@Service
public class EntityManagerService {
    @PersistenceContext
    private EntityManager entityManager;

    /**
     * 原生SQL分页查询
     * @param querySql 查询的SQL
     * @param pageable 分页对象，值为null时返回全部
     * @return 返回查询结果
     */
    @SuppressWarnings("deprecation")
    public Page<Map<String, Object>> findPageListBySql(StringBuilder querySql, Pageable pageable){
        String countStr = "select count(1) from ("+querySql.toString()+") sda";
        Query dataQuery = entityManager.createNativeQuery(querySql.toString());
        dataQuery.unwrap(SQLQuery.class).setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
        Query countQuery = entityManager.createNativeQuery(countStr);
        BigInteger count  = (BigInteger) countQuery.getSingleResult();
        long total = count.longValue();
        if (pageable == null){
            pageable = PageRequest.of(0,total==0?1:count.intValue());
        }
        dataQuery.setFirstResult((int)pageable.getOffset());
        dataQuery.setMaxResults(pageable.getPageSize());
        List<Map<String, Object>> content = total > pageable.getOffset() ? dataQuery.getResultList() : Collections.emptyList();
        return new PageImpl<>(content, pageable, total);
    }
}
