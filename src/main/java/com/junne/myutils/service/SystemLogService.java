package com.junne.myutils.service;

import com.junne.myutils.dto.SystemLogDTO;
import com.junne.myutils.entity.SystemLog;
import com.junne.myutils.repository.SystemLogRepository;
import com.junne.myutils.qdutils.jpa.JpaUtil;
import com.junne.myutils.qdutils.SpringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 描述:
 * 安全审计日志
 *
 * @author JUNNE
 * @create 2019-10-29 15:56
 */
@Service
public class SystemLogService {
    @Autowired
    private SystemLogRepository systemLogRepository;

    /**
     * 查询一条记录
     * @param map 查询的map，一般是查询对象的字段及查询值
     * @return 返回最多一条记录
     */
    public SystemLog findOne(Map<String, Object> map){
        Pageable pageable = PageRequest.of(0,1);
        Specification<SystemLog> specification = JpaUtil.getSpecification(SystemLog.class, map);
        Page<SystemLog> page = systemLogRepository.findAll(specification, pageable);
        return JpaUtil.getOneResult(page);
    }

    /**
     * 查询返回list
     * @param map 查询的map，一般是查询对象的字段及查询值
     * @return 返回list结果
     */
    public List<SystemLog> findList(Map<String, Object> map){
        Sort sort = new Sort(Sort.Direction.ASC, "id");
        Specification<SystemLog> specification = JpaUtil.getSpecification(SystemLog.class, map);
        return systemLogRepository.findAll(specification,sort);
    }

    /**
     * 分页查询
     * @param map 查询的map，一般是查询对象的字段及查询值
     * @param pageable 分页对象
     * @return 返回分页查询列表
     */
    public Page<SystemLog> findPageList(Map<String, Object> map, Pageable pageable){
        Specification<SystemLog> specification = JpaUtil.getSpecification(SystemLog.class, map);
        return systemLogRepository.findAll(specification, pageable);
    }

    /**
     * 保存数据
     * @param systemLog 系统日志对象
     */
    @Transactional(rollbackFor = Exception.class)
    public void save(SystemLog systemLog){
        systemLogRepository.save(systemLog);
    }


    /**
     * 实体对象Page转DTO对象Page
     * @param systemLogPage 实体对象Page
     * @return 返回DTO对象Page
     */
    public Page<SystemLogDTO> convertToSystemLogDTOPage(Page<SystemLog> systemLogPage){
        if (systemLogPage == null){
            return null;
        }
        List<SystemLog> content = systemLogPage.getContent();
        if (content.size() == 0){
            return null;
        }
        List<SystemLogDTO> SystemLogDTOList = this.convertToSystemLogDTOList(content, new ArrayList<>());
        return new PageImpl<>(SystemLogDTOList,systemLogPage.getPageable(),systemLogPage.getTotalElements());
    }

    /**
     * 实体对象列表转DTO对象列表
     * @param systemLogList 实体对象列表
     * @param systemLogDTOList DTO对象列表
     * @return 返回DTO对象列表
     */
    public List<SystemLogDTO> convertToSystemLogDTOList(List<SystemLog> systemLogList,List<SystemLogDTO> systemLogDTOList){
        if (systemLogList == null || systemLogList.size() == 0){
            return null;
        }
        for (SystemLog systemLog:systemLogList) {
            SystemLogDTO systemLogDTO = this.convertToSystemLogDTO(systemLog, new SystemLogDTO());
            if (systemLogDTO != null){
                systemLogDTOList.add(systemLogDTO);
            }
        }
        return systemLogDTOList;
    }

    /**
     * 实体对象转DTO对象
     * @param systemLog 实体对象
     * @param systemLogDTO DTO对象
     * @return 返回DTO对象
     */
    public SystemLogDTO convertToSystemLogDTO(SystemLog systemLog,SystemLogDTO systemLogDTO){
        if (systemLog == null || systemLog.getId() == null || "".equals(systemLog.getId())){
            return null;
        }
        SpringUtil.copyPropertiesIgnoreNull(systemLog,systemLogDTO);
        return systemLogDTO;
    }
}
