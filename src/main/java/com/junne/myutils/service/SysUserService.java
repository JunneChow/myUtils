package com.junne.myutils.service;

import com.junne.myutils.common.ConstantsCfg;
import com.junne.myutils.conf.shiro.MyShiroRealm;
import com.junne.myutils.qdutils.exception.WarningException;
import com.junne.myutils.dto.SysUserDTO;
import com.junne.myutils.entity.SysPermission;
import com.junne.myutils.entity.SysRole;
import com.junne.myutils.entity.SysUser;
import com.junne.myutils.repository.SysPermissionRepository;
import com.junne.myutils.repository.SysRoleRepository;
import com.junne.myutils.repository.SysUserRepository;
import com.junne.myutils.qdutils.jpa.JpaUtil;
import com.junne.myutils.qdutils.SpringUtil;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;


/**
 * 描述:
 * 用户模块Service
 *
 * @author JUNNE
 * @create 2019-08-29 14:43
 */
@Service
public class SysUserService {
    
    @Autowired
    private SysUserRepository sysUserRepository;

    /**
     * MD5密码加盐加密
     * @param credentials 凭证（密码）
     * @param salt 盐值
     * @return 返回加密后的信息
     */
    public String passwordEncrypt(String credentials, String salt){
        Object object = new SimpleHash("MD5", credentials, salt, ConstantsCfg.HASH_ITERATIONS);
        return object.toString();
    }

    /**
     * 查询系统用户权限
     * @param id 系统用户ID
     * @return 返回系统用户权限集合
     */
    public Set<SysPermission> findSysPermissionById(String id){
        Set<SysPermission> sysPermissionSet = new HashSet<>();
        if ("super_admin".equalsIgnoreCase(id)){
            List<SysPermission> permissionList = SpringUtil.getBean(SysPermissionRepository.class).findAll();
            sysPermissionSet.addAll(permissionList);
            return sysPermissionSet;
        }else {
            Set<SysRole> sysRoleSet = this.findSysRoleById(id);
            if (sysRoleSet != null){
                for (SysRole sysRole:sysRoleSet) {
                    List<SysPermission> permissionList = sysRole.getPermissionList();
                    sysPermissionSet.addAll(permissionList);
                }
                return sysPermissionSet;
            }
        }
        return null;
    }

    /**
     * 查询系统用户角色
     * @param id 系统用户ID
     * @return 返回系统用户角色集合
     */
    public Set<SysRole> findSysRoleById(String id){
        Map<String,Object> map = new HashMap<>();
        map.put("id",id);
        SysUser sysUser = this.findOne(map);
        List<SysRole> sysRoleList = sysUser.getSysRoleList();
        if ("super_admin".equalsIgnoreCase(id)){
            sysRoleList = SpringUtil.getBean(SysRoleRepository.class).findAll();
        }
        if (sysRoleList != null && sysRoleList.size()>0){
            return new HashSet<>(sysRoleList);
        }
        return null;
    }

    /**
     * 按主键判断实体对象是否存在
     * @param id 主键ID
     * @return 返回布尔值
     */
    public boolean checkIfExistsById(String id){
        return sysUserRepository.existsById(id);
    }

    /**
     * 按主键查找对象
     * @param id 主键ID
     * @return 返回查询对象
     */
    public SysUser findById(String id){
        return sysUserRepository.findById(id).orElse(null);
    }

    /**
     * 保存实体对象
     * @param sysUser 实体对象
     * @return 返回保存后的实体对象
     */
    @Transactional(rollbackFor = Exception.class)
    public SysUser save(SysUser sysUser){
        SysUser save = sysUserRepository.save(sysUser);
        SpringUtil.getBean(MyShiroRealm.class).clearCachedAuthorizationInfo(save.getUserName());
        return save;
    }

    /**
     * 更新实体对象
     * @param sysUser 实体对象
     * @return 返回更新后的实体对象
     */
    @Transactional(rollbackFor = Exception.class)
    public SysUser update(SysUser sysUser) throws WarningException {
        SysUser byIdForUpdate = sysUserRepository.findByIdForUpdate(sysUser.getId());
        if (byIdForUpdate == null){
            throw new WarningException("当前记录已失效！");
        }
        SysUser newSysUser = sysUserRepository.saveAndFlush(sysUser);
        SpringUtil.getBean(MyShiroRealm.class).clearCachedAuthorizationInfo(newSysUser.getUserName());
        return newSysUser;
    }

    /**
     * 按主键删除实体对象
     * @param id 表主键ID
     */
    @Transactional(rollbackFor = Exception.class)
    public void deleteById(String id) throws WarningException {
        SysUser byIdForUpdate = sysUserRepository.findByIdForUpdate(id);
        if (byIdForUpdate == null){
            throw new WarningException("当前记录已失效！");
        }
        sysUserRepository.deleteById(id);
    }

    /**
     * 处理请求参数Map对象
     * @param paramKey 请求参数键
     * @param paramValue 请求参数值
     * @param map 请求参数Map对象
     * @return 返回用于查询的Map对象
     * @throws WarningException 抛出异常
     */
    public Map<String, Object> handleParamQueryMap(String paramKey, String paramValue,Map<String, Object> map) throws WarningException {
        if ("id".equals(paramKey)){
            map.put("id",paramValue);
        }else {
            throw new WarningException("请求参数异常");
        }
        return map;
    }

    /**
     * 查询一条记录
     * @param map 查询的map，一般是查询对象的字段及查询值
     * @return 返回最多一条记录
     */
    public SysUser findOne(Map<String, Object> map){
        Pageable pageable = PageRequest.of(0,1);
        Specification<SysUser> specification = JpaUtil.getSpecification(SysUser.class, map);
        Page<SysUser> page = sysUserRepository.findAll(specification, pageable);
        return JpaUtil.getOneResult(page);
    }

    /**
     * 分页查询list
     * @param map 查询的map，一般是查询对象的字段及查询值
     * @param pageable Pageable分页对象
     * @return 返回分页的数据集
     */
    public Page<SysUser> findPageList(Map<String, Object> map, Pageable pageable){
        Specification<SysUser> specification = JpaUtil.getSpecification(SysUser.class, map);
        return sysUserRepository.findAll(specification, pageable);
    }

    /**
     * 查询返回list
     * @param map 查询的map，一般是查询对象的字段及查询值
     * @return 返回list结果
     */
    public List<SysUser> findList(Map<String, Object> map){
        Sort sort = new Sort(Sort.Direction.ASC, "id");
        Specification<SysUser> specification = JpaUtil.getSpecification(SysUser.class, map);
        return sysUserRepository.findAll(specification,sort);
    }

    /**
     * 实体对象分页数据转DTO对象分页数据
     * @param sysUserPage 实体对象分页数据
     * @return 返回DTO对象分页数据
     */
    public Page<SysUserDTO> convertToSysUserDTOPage(Page<SysUser> sysUserPage){
        if (sysUserPage == null){
            return null;
        }
        List<SysUser> content = sysUserPage.getContent();
        if (content.size() == 0){
            return null;
        }
        List<SysUserDTO> sysUserDTOList = this.convertToSysUserDTOList(content, new ArrayList<>());
        return new PageImpl<>(sysUserDTOList,sysUserPage.getPageable(),sysUserPage.getTotalElements());
    }

    /**
     * 转换实体对象列表为DTO对象列表
     * @param sysUserList 实体对象列表
     * @param sysUserDTOList DTO对象列表
     * @return 返回DTO对象列表
     */
    public List<SysUserDTO> convertToSysUserDTOList(List<SysUser> sysUserList,List<SysUserDTO> sysUserDTOList){
        if (sysUserList == null || sysUserList.size() == 0){
            return null;
        }
        for (SysUser sysUser:sysUserList) {
            SysUserDTO sysUserDTO = this.convertToSysUserDTO(sysUser, new SysUserDTO());
            if (sysUserDTO != null){
                sysUserDTOList.add(sysUserDTO);
            }
        }
        return sysUserDTOList;
    }

    /**
     * 转换系统用户DTO
     * @param sysUser 系统用户对象
     * @param sysUserDTO 系统用户DTO
     * @return 返回系统用户DTO
     */
    public SysUserDTO convertToSysUserDTO(SysUser sysUser,SysUserDTO sysUserDTO){
        if (sysUser == null || sysUser.getId() == null || "".equals(sysUser.getId())){
            return null;
        }
        SpringUtil.copyPropertiesIgnoreNull(sysUser,sysUserDTO);
        Set<SysRole> allSysRole = this.findSysRoleById(sysUser.getId());
        if (allSysRole != null){
            StringBuilder roleNameBuilder = new StringBuilder();
            for (SysRole sysRole:allSysRole) {
                String roleName = sysRole.getRoleName();
                if (roleName != null && !"".equals(roleName)){
                    roleNameBuilder.append(roleName).append(",");
                }
            }
            roleNameBuilder.deleteCharAt(roleNameBuilder.length()-1);
            sysUserDTO.setRoleName(roleNameBuilder.toString());
        }
        return sysUserDTO;
    }

    /**
     * DTO对象转实体对象
     * @param sysUserDTO DTO对象
     * @param sysUser 实体对象
     * @return 返回实体对象
     */
    public SysUser convertToSysUser(SysUserDTO sysUserDTO,SysUser sysUser) {
        SpringUtil.copyPropertiesIgnoreNull(sysUserDTO,sysUser);
        return sysUser;
    }
}
