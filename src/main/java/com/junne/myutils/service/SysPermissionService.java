package com.junne.myutils.service;

import com.junne.myutils.dto.SysPermissionDTO;
import com.junne.myutils.entity.SysPermission;
import com.junne.myutils.repository.SysPermissionRepository;
import com.junne.myutils.qdutils.jpa.JpaUtil;
import com.junne.myutils.qdutils.SpringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 描述:
 * 系统用户权限Service
 *
 * @author JUNNE
 * @create 2019-10-24 17:26
 */
@Service
public class SysPermissionService {
    
    @Autowired
    private SysPermissionRepository sysPermissionRepository;

    /**
     * 查询一条记录
     * @param map 查询的map，一般是查询对象的字段及查询值
     * @return 返回最多一条记录
     */
    public SysPermission findOne(Map<String, Object> map){
        Pageable pageable = PageRequest.of(0,1);
        Specification<SysPermission> specification = JpaUtil.getSpecification(SysPermission.class, map);
        Page<SysPermission> page = sysPermissionRepository.findAll(specification, pageable);
        return JpaUtil.getOneResult(page);
    }

    /**
     * 查询返回list
     * @param map 查询的map，一般是查询对象的字段及查询值
     * @return 返回list结果
     */
    public List<SysPermission> findList(Map<String, Object> map){
        Sort sort = new Sort(Sort.Direction.ASC, "id");
        Specification<SysPermission> specification = JpaUtil.getSpecification(SysPermission.class, map);
        return sysPermissionRepository.findAll(specification,sort);
    }

    /**
     * 实体对象列表转DTO对象列表
     * @param sysPermissionList 实体对象列表
     * @param sysPermissionDTOList DTO对象列表
     * @return 返回DTO对象列表
     */
    public List<SysPermissionDTO> convertToSysRoleDTOList(List<SysPermission> sysPermissionList, List<SysPermissionDTO> sysPermissionDTOList){
        for (SysPermission sysPermission:sysPermissionList) {
            SysPermissionDTO sysPermissionDTO = this.convertToSysRoleDTO(sysPermission, new SysPermissionDTO());
            if (sysPermissionDTO != null){
                sysPermissionDTOList.add(sysPermissionDTO);
            }
        }
        return sysPermissionDTOList;
    }

    /**
     * 实体对象转DTO对象
     * @param sysPermission 实体对象
     * @param sysPermissionDTO DTO对象
     * @return 返回DTO对象
     */
    public SysPermissionDTO convertToSysRoleDTO(SysPermission sysPermission,SysPermissionDTO sysPermissionDTO){
        SpringUtil.copyPropertiesIgnoreNull(sysPermission,sysPermissionDTO);
        return sysPermissionDTO;
    }
}
