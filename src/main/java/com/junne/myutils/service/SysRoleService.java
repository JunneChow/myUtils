package com.junne.myutils.service;

import com.junne.myutils.dto.SysRoleDTO;
import com.junne.myutils.entity.SysRole;
import com.junne.myutils.repository.SysRoleRepository;
import com.junne.myutils.qdutils.jpa.JpaUtil;
import com.junne.myutils.qdutils.SpringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 描述:
 * 系统用户角色Service
 *
 * @author JUNNE
 * @create 2019-10-24 17:28
 */
@Service
public class SysRoleService {
    
    @Autowired
    private SysRoleRepository sysRoleRepository;

    /**
     * 查询一条记录
     * @param map 查询的map，一般是查询对象的字段及查询值
     * @return 返回最多一条记录
     */
    public SysRole findOne(Map<String, Object> map){
        Pageable pageable = PageRequest.of(0,1);
        Specification<SysRole> specification = JpaUtil.getSpecification(SysRole.class, map);
        Page<SysRole> page = sysRoleRepository.findAll(specification, pageable);
        return JpaUtil.getOneResult(page);
    }

    /**
     * 查询返回list
     * @param map 查询的map，一般是查询对象的字段及查询值
     * @return 返回list结果
     */
    public List<SysRole> findList(Map<String, Object> map){
        Sort sort = new Sort(Sort.Direction.ASC, "id");
        Specification<SysRole> specification = JpaUtil.getSpecification(SysRole.class, map);
        return sysRoleRepository.findAll(specification,sort);
    }

    /**
     * 实体对象列表转DTO对象列表
     * @param sysRoleList 实体对象列表
     * @param sysRoleDTOList DTO对象列表
     * @return 返回DTO对象列表
     */
    public List<SysRoleDTO> convertToSysRoleDTOList(List<SysRole> sysRoleList,List<SysRoleDTO> sysRoleDTOList){
        for (SysRole sysRole:sysRoleList) {
            SysRoleDTO sysRoleDTO = this.convertToSysRoleDTO(sysRole, new SysRoleDTO());
            if (sysRoleDTO != null){
                sysRoleDTOList.add(sysRoleDTO);
            }
        }
        return sysRoleDTOList;
    }

    /**
     * 实体对象转DTO对象
     * @param sysRole 实体对象
     * @param sysRoleDTO DTO对象
     * @return 返回DTO对象
     */
    public SysRoleDTO convertToSysRoleDTO(SysRole sysRole,SysRoleDTO sysRoleDTO){
        SpringUtil.copyPropertiesIgnoreNull(sysRole,sysRoleDTO);
        return sysRoleDTO;
    }

}
