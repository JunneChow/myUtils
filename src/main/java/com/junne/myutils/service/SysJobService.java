package com.junne.myutils.service;

import com.junne.myutils.qdutils.exception.WarningException;
import com.junne.myutils.conf.quartz.MyQuartzJobBean;
import com.junne.myutils.dto.SysJobDTO;
import com.junne.myutils.entity.SysJob;
import com.junne.myutils.repository.SysJobRepository;
import com.junne.myutils.qdutils.jpa.JpaUtil;
import com.junne.myutils.qdutils.SpringUtil;
import org.quartz.CronExpression;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 描述:
 * 系统作业Service
 *
 * @author JUNNE
 * @create 2019-12-02 14:51
 */
@Service
public class SysJobService {

    @Autowired
    private SysJobRepository sysJobRepository;

    @Autowired
    private MyQuartzJobBean myQuartzJobBean;

    /**
     * 处理请求参数Map对象
     * @param paramKey 请求参数键
     * @param paramValue 请求参数值
     * @param map 请求参数Map对象
     * @return 返回用于查询的Map对象
     * @throws WarningException 抛出异常
     */
    public Map<String, Object> handleParamQueryMap(String paramKey, String paramValue,Map<String, Object> map) throws WarningException {
        if ("id".equals(paramKey)){
            map.put("id",paramValue);
        }else {
            throw new WarningException("请求参数异常");
        }
        return map;
    }

    /**
     * 查询一条记录
     * @param map 查询的map，一般是查询对象的字段及查询值
     * @return 返回最多一条记录
     */
    public SysJob findOne(Map<String, Object> map){
        Pageable pageable = PageRequest.of(0,1);
        Specification<SysJob> specification = JpaUtil.getSpecification(SysJob.class, map);
        Page<SysJob> page = sysJobRepository.findAll(specification, pageable);
        return JpaUtil.getOneResult(page);
    }

    /**
     * 分页查询list
     * @param map 查询的map，一般是查询对象的字段及查询值
     * @param pageable Pageable分页对象
     * @return 返回分页的数据集
     */
    public Page<SysJob> findPageList(Map<String, Object> map, Pageable pageable){
        Specification<SysJob> specification = JpaUtil.getSpecification(SysJob.class, map);
        return sysJobRepository.findAll(specification, pageable);
    }

    /**
     * 查询返回list
     * @param map 查询的map，一般是查询对象的字段及查询值
     * @return 返回list结果
     */
    public List<SysJob> findList(Map<String, Object> map){
        Sort sort = new Sort(Sort.Direction.ASC, "id");
        Specification<SysJob> specification = JpaUtil.getSpecification(SysJob.class, map);
        return sysJobRepository.findAll(specification,sort);
    }

    /**
     * 按主键判断实体对象是否存在
     * @param id 主键ID
     * @return 返回布尔值
     */
    public boolean checkIfExistsById(String id){
        return sysJobRepository.existsById(id);
    }

    /**
     * 按主键查找对象
     * @param id 主键ID
     * @return 返回查询对象
     */
    public SysJob findById(String id){
        return sysJobRepository.findById(id).orElse(null);
    }

    /**
     * 校验时间调度Cron公式
     * @param cron 时间调度Cron公式
     * @return 返回布尔值（true：正确，false：错误）
     */
    public boolean checkCron(String cron){
        return CronExpression.isValidExpression(cron);
    }

    /**
     * 添加实体对象
     * @param sysJob 实体对象
     * @return 返回添加后的实体对象
     */
    @Transactional(rollbackFor = Exception.class)
    public SysJob save(SysJob sysJob) {
        return sysJobRepository.save(sysJob);
    }

    /**
     * 更新实体对象
     * @param sysJob 实体对象
     * @return 返回更新后的实体对象
     */
    @Transactional(rollbackFor = Exception.class)
    public SysJob update(SysJob sysJob) throws WarningException {
        //获得行记录的悲观锁
        SysJob byIdForUpdate = sysJobRepository.findByIdForUpdate(sysJob.getId());
        if (byIdForUpdate == null){
            throw new WarningException("当前记录已失效！");
        }
        return sysJobRepository.saveAndFlush(sysJob);
    }

    @Transactional(rollbackFor = Exception.class)
    public void delete(String id) throws WarningException {
        //获得行记录的悲观锁
        SysJob sysJob = sysJobRepository.findByIdForUpdate(id);
        if (sysJob == null){
            throw new WarningException("当前记录已失效！");
        }
        sysJobRepository.delete(sysJob);
        myQuartzJobBean.unScheduleJob(sysJob);
    }

    @Transactional(rollbackFor = Exception.class)
    public SysJob start(String id) throws WarningException {
        SysJob sysJob = sysJobRepository.findByIdForUpdate(id);
        if (sysJob == null){
            throw new WarningException("当前记录已失效！");
        }
        sysJob.setStatus("1");
        SysJob sysJobRs = sysJobRepository.saveAndFlush(sysJob);
        myQuartzJobBean.scheduleJob(sysJob);
        return sysJobRs;
    }

    @Transactional(rollbackFor = Exception.class)
    public SysJob stop(String id) throws WarningException {
        SysJob sysJob = sysJobRepository.findByIdForUpdate(id);
        if (sysJob == null){
            throw new WarningException("当前记录已失效！");
        }
        sysJob.setStatus("0");
        SysJob sysJobRs = sysJobRepository.saveAndFlush(sysJob);
        myQuartzJobBean.unScheduleJob(sysJob);
        return sysJobRs;
    }

    /**
     * 实体对象分页数据转DTO对象分页数据
     * @param SysJobPage 实体对象分页数据
     * @return 返回DTO对象分页数据
     */
    public Page<SysJobDTO> convertToSysJobDTOPage(Page<SysJob> SysJobPage){
        if (SysJobPage == null){
            return null;
        }
        List<SysJob> content = SysJobPage.getContent();
        if (content.size() == 0){
            return null;
        }
        List<SysJobDTO> SysJobDTOList = this.convertToSysJobDTOList(content, new ArrayList<>());
        return new PageImpl<>(SysJobDTOList,SysJobPage.getPageable(),SysJobPage.getTotalElements());
    }

    /**
     * 实体对象列表转DTO对象列表
     * @param sysJobList 实体对象列表
     * @param sysJobDTOList DTO对象列表
     * @return 返回DTO对象列表
     */
    public List<SysJobDTO> convertToSysJobDTOList(List<SysJob> sysJobList,List<SysJobDTO> sysJobDTOList){
        for (SysJob sysJob:sysJobList) {
            SysJobDTO sysJobDTO = this.convertToSysJobDTO(sysJob, new SysJobDTO());
            if (sysJobDTO != null){
                sysJobDTOList.add(sysJobDTO);
            }
        }
        return sysJobDTOList;
    }

    /**
     * 实体对象转DTO对象
     * @param sysJob 实体对象
     * @param sysJobDTO DTO对象
     * @return 返回DTO对象
     */
    public SysJobDTO convertToSysJobDTO(SysJob sysJob,SysJobDTO sysJobDTO){
        SpringUtil.copyPropertiesIgnoreNull(sysJob,sysJobDTO);
        return sysJobDTO;
    }

    /**
     * DTO对象转实体对象
     * @param sysJobDTO DTO对象
     * @param sysJob 实体对象
     * @return 返回实体对象
     */
    public SysJob convertToSysJob(SysJobDTO sysJobDTO,SysJob sysJob) {
        SpringUtil.copyPropertiesIgnoreNull(sysJobDTO,sysJob);
        return sysJob;
    }
}
