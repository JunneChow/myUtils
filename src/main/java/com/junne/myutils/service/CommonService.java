package com.junne.myutils.service;

import com.junne.myutils.dto.MenuDTO;
import com.junne.myutils.entity.SysPermission;
import com.junne.myutils.repository.SysPermissionRepository;
import com.junne.myutils.qdutils.jpa.JpaUtil;
import com.junne.myutils.qdutils.SpringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 描述:
 * 公共service
 *
 * @author JUNNE
 * @create 2019-09-30 14:02
 */
@Service
public class CommonService {

    @Autowired
    private SysPermissionRepository sysPermissionRepository;

    /**
     * 返回菜单列表
     * @return 菜单列表
     */
    public List<SysPermission> menuList(String id){
        Sort sort = new Sort(Sort.Direction.ASC,"sort");
        Map<String,Object> map = new HashMap<>();
        map.put("parentId","0");
        if (!"super_admin".equalsIgnoreCase(id)){
            map.put("roleList-userList:id",id);
        }
        Specification<SysPermission> specification = JpaUtil.getSpecification(SysPermission.class, map);
        return sysPermissionRepository.findAll(specification, sort);
    }

    /**
     * 权限信息转换为菜单
     * @param sysPermission 权限对象
     * @param menuDTO 菜单DTO
     * @return 菜单DTO
     */
    public MenuDTO convertToMenuDTO(SysPermission sysPermission,MenuDTO menuDTO){
        SpringUtil.copyPropertiesIgnoreNull(sysPermission,menuDTO);
        //处理子数据
        List<SysPermission> sysPermissionChildList = sysPermission.getChildList();
        if (sysPermissionChildList != null && sysPermissionChildList.size() > 0){
            List<MenuDTO> childMenuDTOList = new ArrayList<>();
            for (SysPermission child:sysPermissionChildList) {
                //提取类型为1的项
                if(child.getType()==1){
                    MenuDTO childMenuDTO = this.convertToMenuDTO(child, new MenuDTO());
                    childMenuDTOList.add(childMenuDTO);
                }
            }
            menuDTO.setChildList(childMenuDTOList);
        }
        return menuDTO;
    }
}
