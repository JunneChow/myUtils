package com.junne.myutils;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * 描述:
 * springBoot主方法
 *
 * @author JUNNE
 * @create 2019-08-28 12:50
 */

@SpringBootApplication
@EnableScheduling
@ServletComponentScan
@EnableJpaAuditing
public class MyUtilsApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(MyUtilsApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(MyUtilsApplication.class);
    }
}
