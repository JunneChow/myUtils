package com.junne.myutils.exception;

import com.junne.myutils.common.ConstantsCfg;
import com.junne.myutils.conf.ehcache.MyEhcacheManager;
import com.junne.myutils.qdutils.SpringUtil;
import com.junne.myutils.qdutils.api.ResultVO;
import com.junne.myutils.qdutils.api.ResultVOUtil;
import com.junne.myutils.qdutils.api.enums.ResultEnum;
import com.junne.myutils.qdutils.exception.WarningException;
import com.junne.myutils.utils.WebUtil;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authz.UnauthorizedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.PessimisticLockingFailureException;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;

/**
 * 描述:
 * 异常处理
 *
 * @author JUNNE
 * @create 2019-09-06 14:39
 */
@ControllerAdvice
public class MyExceptionHandler {

    private Logger log = LoggerFactory.getLogger(MyExceptionHandler.class);

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public ResultVO handleException(Exception e){
        if (WebUtil.getHttpServletRequest() != null){
            log.info("系统服务异常："+WebUtil.getHttpServletRequest().getRequestURI(),e);
        }else {
            log.info("系统服务异常：",e);
        }
        return ResultVOUtil.fail(ResultEnum.SERVER_ERROR.getCode(),ResultEnum.SERVER_ERROR.getMsg(),null);
    }

    @ExceptionHandler(WarningException.class)
    @ResponseBody
    public ResultVO handleWarning(WarningException e){
        if (e.getHttpStatusCode() != null && WebUtil.getHttpServletResponse() != null){
            WebUtil.getHttpServletResponse().setStatus(e.getHttpStatusCode());
        }
        String exceptionMsg = e.getMessage();
        if (e.getCode() != null){
            return ResultVOUtil.fail(e.getCode(),exceptionMsg,null);
        }
        return ResultVOUtil.fail(exceptionMsg);
    }

    @ExceptionHandler({ UnknownAccountException.class, IncorrectCredentialsException.class })
    @ResponseBody
    public ResultVO loginException(Exception e) {
        HttpServletRequest httpServletRequest = WebUtil.getHttpServletRequest();
        //1.重置验证码
        if (httpServletRequest != null){
            httpServletRequest.getSession().removeAttribute(ConstantsCfg.VERIFY_CODE_TYPE_LOGIN);
        }
        //2.登录失败次数加1
        String remoteIp = WebUtil.getIpAddress();
        Integer countNum = (Integer) SpringUtil.getBean(MyEhcacheManager.class).get(ConstantsCfg.EHCACHE_PREFIX+"_LOGON_FAILURE_HOST"+"_"+remoteIp);
        if(countNum == null){
            countNum=0;
        }
        //记录登录失败次数
        SpringUtil.getBean(MyEhcacheManager.class).put(ConstantsCfg.EHCACHE_PREFIX+"_LOGON_FAILURE_HOST"+"_"+remoteIp,++countNum);
        return ResultVOUtil.fail(e.getMessage());
    }

    @ExceptionHandler(UnauthorizedException.class)
    @ResponseBody
    public ResultVO authorizeException(UnauthorizedException e){
        return ResultVOUtil.fail("该用户不具备当前操作的权限");
    }

    @ExceptionHandler(PessimisticLockingFailureException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public ResultVO pessimisticLockingFailureException(PessimisticLockingFailureException e){
        log.info("悲观锁异常：当前记录正被操作，请稍后重试！");
        return ResultVOUtil.fail("当前记录正被操作，请稍后重试！");
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ResultVO handleValidationError(MethodArgumentNotValidException e){
        BindingResult bindingResult = e.getBindingResult();
        ObjectError objectError = bindingResult.getAllErrors().stream().findFirst().orElse(null);
        if (objectError != null){
            return ResultVOUtil.fail(objectError.getDefaultMessage());
        }
        return null;
    }
}
