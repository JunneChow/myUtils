package com.junne.myutils.dto;

import java.io.Serializable;

/**
 * 描述:
 * 系统作业DTO
 *
 * @author JUNNE
 * @create 2019-12-02 16:39
 */
public class SysJobDTO implements Serializable {

    private static final long serialVersionUID = -8417406271044234761L;

    private String id;                  //数据库ID编号
    private String jobName;
    private String description;
    private String cron;
    private String springBeanName;
    private String methodName;
    private String status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCron() {
        return cron;
    }

    public void setCron(String cron) {
        this.cron = cron;
    }

    public String getSpringBeanName() {
        return springBeanName;
    }

    public void setSpringBeanName(String springBeanName) {
        this.springBeanName = springBeanName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
