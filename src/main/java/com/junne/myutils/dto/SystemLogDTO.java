package com.junne.myutils.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * 描述:
 * 系统日志DTO
 *
 * @author JUNNE
 * @create 2019-09-18 14:43
 */
public class SystemLogDTO implements Serializable {
    private static final long serialVersionUID = -786026044618815702L;

    private String id;                  //数据库ID编号
    private Date actionTime;          //请求的动作时间
    private String actionClass;         //请求的动作类
    private String actionMethod;        //请求的动作方法名
    private String description;         //请求的操作描述
    private String operationType;       //请求的操作类型
    private String url;                 //请求的URL
    private String parameters;          //请求的参数
    private String ip;                  //请求的IP
    private String userId;              //请求用户ID
    private String userName;            //请求的用户名称
    private String resultCode;          //返回的代码
    private String resultMsg;           //返回的提示信息

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getActionTime() {
        return actionTime;
    }

    public void setActionTime(Date actionTime) {
        this.actionTime = actionTime;
    }

    public String getActionClass() {
        return actionClass;
    }

    public void setActionClass(String actionClass) {
        this.actionClass = actionClass;
    }

    public String getActionMethod() {
        return actionMethod;
    }

    public void setActionMethod(String actionMethod) {
        this.actionMethod = actionMethod;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOperationType() {
        return operationType;
    }

    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getParameters() {
        return parameters;
    }

    public void setParameters(String parameters) {
        this.parameters = parameters;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getResultCode() {
        return resultCode;
    }

    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }

    public String getResultMsg() {
        return resultMsg;
    }

    public void setResultMsg(String resultMsg) {
        this.resultMsg = resultMsg;
    }

    @Override
    public String toString() {
        return "SystemLogDTO{" +
                "id='" + id + '\'' +
                ", actionTime=" + actionTime +
                ", actionClass='" + actionClass + '\'' +
                ", actionMethod='" + actionMethod + '\'' +
                ", description='" + description + '\'' +
                ", operationType='" + operationType + '\'' +
                ", url='" + url + '\'' +
                ", parameters='" + parameters + '\'' +
                ", ip='" + ip + '\'' +
                ", userId='" + userId + '\'' +
                ", userName='" + userName + '\'' +
                ", resultCode='" + resultCode + '\'' +
                ", resultMsg='" + resultMsg + '\'' +
                '}';
    }
}
