package com.junne.myutils.dto;

import io.swagger.annotations.ApiModel;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 描述:
 * 系统用户DTO
 *
 * @author JUNNE
 * @create 2019-10-24 17:07
 */
@ApiModel
public class SysUserDTO implements Serializable {
    private static final long serialVersionUID = 4300010845164823383L;

    private String id;
    private String userName;
    private String nickName;
    private String roleName;    //系统角色
    private int state;  //用户状态（0：未验证使用，1：正常使用，2：用户被锁定）
    private Date updateTime; //更新时间

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
