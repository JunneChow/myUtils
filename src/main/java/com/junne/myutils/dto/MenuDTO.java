package com.junne.myutils.dto;

import java.io.Serializable;
import java.util.List;

/**
 * 描述:
 * 菜单DTO
 *
 * @author JUNNE
 * @create 2019-09-30 14:09
 */
public class MenuDTO implements Serializable {
    private static final long serialVersionUID = 9073389265011506887L;

    private String id;
    private String enable;  //是否启用（1：启用，0：禁用）
    private String parentId;    //父级ID
    private String permissionName;  //名称
    private String css; //样式
    private String href; //URL地址
    private Integer type;   //类型
    private String permission;  //权限
    private Integer sort;   //序号
    private List<MenuDTO> childList;    //子列表

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEnable() {
        return enable;
    }

    public void setEnable(String enable) {
        this.enable = enable;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getPermissionName() {
        return permissionName;
    }

    public void setPermissionName(String permissionName) {
        this.permissionName = permissionName;
    }

    public String getCss() {
        return css;
    }

    public void setCss(String css) {
        this.css = css;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public List<MenuDTO> getChildList() {
        return childList;
    }

    public void setChildList(List<MenuDTO> childList) {
        this.childList = childList;
    }
}
