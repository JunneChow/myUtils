package com.junne.myutils.dto;

import java.io.Serializable;

/**
 * 描述:
 * 角色DTO
 *
 * @author JUNNE
 * @create 2019-11-18 10:04
 */
public class SysRoleDTO  implements Serializable {
    private static final long serialVersionUID = 6535577761610402807L;

    private String id;                  //数据库ID编号
    private String roleName;            //角色
    private String description;         //描述

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

