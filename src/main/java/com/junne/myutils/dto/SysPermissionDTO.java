package com.junne.myutils.dto;

import java.io.Serializable;

/**
 * 描述:
 * 系统权限DTO
 *
 * @author JUNNE
 * @create 2019-11-18 11:21
 */
public class SysPermissionDTO implements Serializable {
    private static final long serialVersionUID = -3287060736944459081L;

    private String id;                  //数据库ID编号
    private String parentId;
    private String permissionName;
    private String css;
    private String href;
    private Integer type;
    private String permission;
    private Integer sort;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getPermissionName() {
        return permissionName;
    }

    public void setPermissionName(String permissionName) {
        this.permissionName = permissionName;
    }

    public String getCss() {
        return css;
    }

    public void setCss(String css) {
        this.css = css;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }
}
