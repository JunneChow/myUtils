package com.junne.myutils.conf;

import com.junne.myutils.entity.SysUser;
import org.apache.shiro.SecurityUtils;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;

import java.util.Optional;

/**
 * 描述:
 * 自动注入审计员-用于@LastModifiedBy等注解
 *
 * @author JUNNE
 * @create 2019-09-17 14:59
 */
@Configuration
public class MyAuditorAware implements AuditorAware<String> {

    @Override
    public Optional<String> getCurrentAuditor() {
        if (SecurityUtils.getSubject() != null){
            SysUser sysUser = (SysUser) SecurityUtils.getSubject().getPrincipal();
            if (sysUser != null){
                return Optional.of(sysUser.getUserName());
            }
        }
        return Optional.empty();
    }
}
