package com.junne.myutils.conf.shiro;

import com.junne.myutils.common.ConstantsCfg;
import com.junne.myutils.dto.SysUserDTO;
import com.junne.myutils.entity.SysPermission;
import com.junne.myutils.entity.SysRole;
import com.junne.myutils.entity.SysUser;
import com.junne.myutils.qdutils.SpringUtil;
import com.junne.myutils.service.SysUserService;
import com.junne.myutils.utils.WebUtil;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.SimplePrincipalCollection;
import org.apache.shiro.util.ByteSource;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * 描述:
 * shiro安全领域
 *
 * @author JUNNE
 * @create 2019-10-10 18:18
 */
public class MyShiroRealm extends AuthorizingRealm {
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        //==================授权操作==================
        SysUserDTO currentUser = WebUtil.getCurrentLogonUser();
        if (currentUser != null && currentUser.getId() != null){
            String sysUserId = currentUser.getId();
            SimpleAuthorizationInfo simpleAuthorizationInfo = new SimpleAuthorizationInfo();
            Set<SysRole> allSysRole = SpringUtil.getBean(SysUserService.class).findSysRoleById(sysUserId);
            if (allSysRole != null){
                //获取并设置角色信息
                Set<String> sysRoleSet = new HashSet<>();
                for (SysRole sysRole:allSysRole) {
                    String roleName = sysRole.getRoleName();
                    if (roleName != null && !"".equals(roleName)){
                        sysRoleSet.add(roleName);
                    }
                }
                simpleAuthorizationInfo.setRoles(sysRoleSet);
            }
            //获取并设置权限信息
            Set<SysPermission> allSysPermissionSet = SpringUtil.getBean(SysUserService.class).findSysPermissionById(sysUserId);
            if (allSysPermissionSet != null){
                WebUtil.setPermissionSet(allSysPermissionSet);
                Set<String> sysPermissionSet = new HashSet<>();
                for (SysPermission sysPermission:allSysPermissionSet) {
                    String permission = sysPermission.getPermission();
                    if(permission != null && !"".equals(permission)){
                        sysPermissionSet.add(permission);
                    }
                }
                simpleAuthorizationInfo.setStringPermissions(sysPermissionSet);
            }
            return simpleAuthorizationInfo;
        }
        return null;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        //==================认证操作==================
        UsernamePasswordToken usernamePasswordToken = (UsernamePasswordToken) authenticationToken;
        String username = usernamePasswordToken.getUsername();
        Map<String,Object> map = new HashMap<>();
        map.put("userName",username);
        SysUser sysUser = SpringUtil.getBean(SysUserService.class).findOne(map);
        //判断用户是否存在
        if (sysUser == null){
            throw new UnknownAccountException("账号或密码错误");
        }
        String passwordEncrypt = SpringUtil.getBean(SysUserService.class)
                .passwordEncrypt(new String(usernamePasswordToken.getPassword()), sysUser.getSalt());
        //判断密码
        if (!sysUser.getPassword().equalsIgnoreCase(passwordEncrypt)){
            throw new IncorrectCredentialsException("账号或密码错误");
        }
        if(sysUser.getState() != ConstantsCfg.USER_STATE_VALID){
            throw new IncorrectCredentialsException(ConstantsCfg.USER_DISABLED_OR_LOCKED);
        }
        return new SimpleAuthenticationInfo(sysUser,sysUser.getPassword(),
                ByteSource.Util.bytes(sysUser.getSalt()),getName());
    }

    /**
     * 更新用户授权信息缓存.
     */
    public void clearCachedAuthorizationInfo(String principal) {
        SimplePrincipalCollection principals = new SimplePrincipalCollection(principal, getName());
        this.clearCachedAuthorizationInfo(principals);
    }
}
