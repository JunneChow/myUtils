package com.junne.myutils.conf.filter;

import com.google.gson.Gson;
import com.junne.myutils.qdutils.api.ResultVO;
import com.junne.myutils.common.ConstantsCfg;
import com.junne.myutils.conf.ehcache.MyEhcacheManager;
import com.junne.myutils.dto.SysUserDTO;
import com.junne.myutils.qdutils.SpringUtil;
import com.junne.myutils.utils.WebUtil;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authc.UserFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 描述:
 * ${DESCRIPTION}
 *
 * @author JUNNE
 * @create 2019-09-18 15:28
 */
public class MyFilter extends UserFilter {

    private static Logger log = LoggerFactory.getLogger(MyFilter.class);

    @Override
    protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) {
        if (this.isLoginRequest(request, response)) {
            return true;
        } else {
            Subject subject = this.getSubject(request, response);
            if(subject.getPrincipal() != null){
                return checkIfOnline();
            }
        }
        return false;
    }

    @Override
    protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;
        String header = httpServletRequest.getHeader(ConstantsCfg.API_REQUEST);
        if(header != null && header.equals("yes")){
            ResultVO resultVO = new ResultVO();
            resultVO.setResultCode("401");
            resultVO.setResultData(HttpStatus.UNAUTHORIZED);
            resultVO.setResultMsg("登录授权失败，请重新登录！");
            Gson gson = new Gson();
            String apiResultJson = gson.toJson(resultVO);
            WebUtil.writeResponse(httpServletResponse,HttpStatus.UNAUTHORIZED.value(),apiResultJson);
            //强制删除当前会话
            WebUtil.setCurrentLogonUser(null);
            return false;
        }
        return super.onAccessDenied(request,response);
    }

    /**
     * 判断用户是否在线
     * @return 返回判断结果
     */
    private boolean checkIfOnline(){
        SysUserDTO sysUserDTO = WebUtil.getCurrentLogonUser();
        if (sysUserDTO != null && !"".equals(sysUserDTO.getId()) && WebUtil.getHttpServletRequest() != null){
            String loginSessionid = (String) SpringUtil.getBean(MyEhcacheManager.class)
                    .get(ConstantsCfg.SYS_USER_LOGIN_SESSIONID+"_"+sysUserDTO.getUserName());
            String currentSessionId = WebUtil.getCurrentSessionId();
            if (loginSessionid != null && loginSessionid.equals(currentSessionId)){
                String requestPre = WebUtil.getHttpServletRequest().getRequestURL().substring(0, WebUtil.getHttpServletRequest()
                        .getRequestURL().indexOf(WebUtil.getHttpServletRequest().getContextPath()));
                String referer = WebUtil.getHttpServletRequest().getHeader("Referer");
                String upgrade = WebUtil.getHttpServletRequest().getHeader("Upgrade");
                if (referer != null && referer.trim().startsWith(requestPre)){
                    //放行
                    return true;
                }else if ("websocket".equals(upgrade)){
                    //websocket放行
                    return true;
                }else {
                    String remoteAddr = WebUtil.getHttpServletRequest().getRemoteAddr();
                    String remoteHost = WebUtil.getHttpServletRequest().getRemoteHost();
                    log.info("请求中含非本系统referer：【"+referer+"】的主机Host为：【"+remoteHost+"】，IP地址为：【"+remoteAddr+"】");
                    ResultVO resultVO = new ResultVO();
                    resultVO.setResultCode("403");
                    resultVO.setResultData(HttpStatus.FORBIDDEN);
                    resultVO.setResultMsg("拒绝含非本系统Referer的请求！");
                    Gson gson = new Gson();
                    String apiResultJson = gson.toJson(resultVO);
                    if (WebUtil.getHttpServletResponse() != null){
                        WebUtil.writeResponse(WebUtil.getHttpServletResponse(),HttpStatus.FORBIDDEN.value(),apiResultJson);
                    }
                }
            }
        }
        return false;
    }
}
