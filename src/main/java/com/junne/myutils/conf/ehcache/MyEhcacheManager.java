package com.junne.myutils.conf.ehcache;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import net.sf.ehcache.config.CacheConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 描述:
 * ehcache缓存管理工具
 *
 * @author JUNNE
 * @create 2019-07-25 10:48
 */
@Component
public class MyEhcacheManager {

    private CacheManager cacheManager;

    private static Map<String,Object> conMap = new ConcurrentHashMap<String,Object>();

    private static String MAP_NAME_PREFIX = "concurrent";

    @Autowired
    public MyEhcacheManager(CacheManager cacheManager) {
        this.cacheManager = cacheManager;
    }

    /**
     * 简单（不支持多线程）-取值
     * @param key 键
     * @return 返回值
     */
    public Object get(String key) {
        String cacheName = (String) conMap.get(key);
        if(cacheName == null){
            return this.cacheGet("", key);
        }else{
            return this.cacheGet(cacheName, key);
        }
    }

    /**
     * 多线程的的数据-取值
     * @param key 键
     * @return 返回值对象
     */
    @SuppressWarnings("unchecked")
    public Object getByConcurrent(String key){
        String mapName = MAP_NAME_PREFIX+key;
        String cacheName = (String) conMap.get(mapName);
        if(cacheName == null){
            Map<String,Object> map = (Map<String, Object>) this.cacheGet("", mapName);
            if (map != null && key != null){
                return map.get(key);
            }
        }else{
            Map<String,Object> map = (Map<String, Object>) this.cacheGet(cacheName, mapName);
            if (map != null && key != null){
                return map.get(key);
            }
        }
        return null;
    }

    /**
     * 简单（不支持多线程）-存值
     * @param key 键
     * @param value 值
     */
    public void put(String key, Object value) {
        this.put(key, value, 0);
    }

    /**
     * 简单（不支持多线程）-限时存值
     * @param key 键
     * @param value 值
     * @param expireTime 过期时间（单位：秒）
     */
    public void put(String key, Object value, long expireTime) {
        this.cachePut("", key, value, expireTime);
    }

    /**
     * 多线程的的数据-存值
     * @param key 键
     * @param value 值
     */
    @SuppressWarnings("unchecked")
    public void putByConcurrent(String key, Object value){
        this.putByConcurrent(key,value,0);
    }

    /**
     * 多线程的的数据-存值
     * @param key 键
     * @param value 值
     * @param expireTime 过期时间（单位：秒）
     */
    @SuppressWarnings("unchecked")
    public void putByConcurrent(String key, Object value, long expireTime){
        String mapName = MAP_NAME_PREFIX+key;
        Map<String,Object> map = (Map<String, Object>) this.get(mapName);
        if (map == null){
            map = new ConcurrentHashMap<>();
        }
        map.put(key,value);
        this.cachePut("", mapName, map, expireTime);
    }

    /**
     * 简单（不支持多线程）-删除
     * @param key 键
     */
    public void remove(String key) {
        String cacheName = (String) conMap.get(key);
        if(cacheName != null) {
            this.cacheRemove(cacheName, key);
            //删除值
            conMap.remove(key);
        }
    }

    /**
     * 多线程的的数据-删除
     * @param key 键
     */
    @SuppressWarnings("unchecked")
    public void removeByConcurrent(String key){
        String mapName = MAP_NAME_PREFIX+key;
        Map<String,Object> map = (Map<String, Object>) this.get(mapName);
        if (map != null && key != null){
            map.remove(key);
        }
    }

    /**
     * 直接调用cache存储（内部方法，慎用）
     * @param key 键
     * @param value 值
     * @param expireTime 失效时间，0：从不失效(指定cacheName的情况下，该参数失效)
     */
    private void cachePut(String cacheName, String key, Object value, long expireTime) {
        if(cacheName == null){
            cacheName = "";
        }
        Element element = new Element(key, value);
        Cache cache = cacheManager.getCache(cacheName);
        if(cache == null) {
            cache = cacheManager.getCache("myEhCache");
            //判断是否已存在myEhCache（配置文件时会自动创建），不存在则硬编码创建
            if(cache == null) {
                //0：表示永久生效，但是myEhCache有默认配置，默认最长只能存储86400秒（一天）
                cache = new Cache("myEhCache", 10000, false, false, 86400, 86400);
                cacheManager.addCache(cache);
            }
            //限时存储的cache是新建的cache对象，其命名规则是："myEhCache"+"_"+过期时间，如30秒的cache命名：【myEhCache_30】
            if(expireTime > 0) {
                CacheConfiguration oldConf = cache.getCacheConfiguration();
                String cacheNameWithExpireTime = cache.getName() + "_" + expireTime;
                Cache cacheWithExpireTime = cacheManager.getCache(cacheNameWithExpireTime);
                if(cacheWithExpireTime == null){
                    CacheConfiguration cacheConfiguration = new CacheConfiguration(cacheNameWithExpireTime, (int) oldConf.getMaxEntriesLocalHeap());
                    cacheConfiguration.setTimeToIdleSeconds(0);
                    cacheConfiguration.setTimeToLiveSeconds(expireTime);
                    cacheWithExpireTime = new Cache(cacheConfiguration);
                    cacheManager.addCache(cacheWithExpireTime);
                }
                cacheWithExpireTime.put(element);
                //删除旧值
                conMap.remove(key);
                //新增新值
                conMap.put(key, cacheNameWithExpireTime);
            }else{
                cache.put(element);
                //删除旧值
                conMap.remove(key);
                //新增新值
                conMap.put(key, cache.getName());
            }
        }else {
            cache.put(element);
            //删除旧值
            conMap.remove(key);
            //新增新值
            conMap.put(key, cacheName);
        }
    }

    /**
     * 获取指定cache的键值（内部方法，慎用）
     * @param cacheName cache名称
     * @param key 键
     * @return 返回指定cache的键值
     */
    private Object cacheGet(String cacheName, String key) {
        if(cacheName == null){
            cacheName = "";
        }
        Cache cache = cacheManager.getCache(cacheName);
        if (cache != null){
            Element element = cache.get(key);
            return element == null ? null : element.getObjectValue();
        }
        return null;
    }

    /**
     * 删除指定cache的键值（内部方法，慎用）
     * @param cacheName cache名称
     * @param key 键
     */
    private void cacheRemove(String cacheName, String key) {
        Cache cache = cacheManager.getCache(cacheName);
        cache.remove(key);
    }
}
