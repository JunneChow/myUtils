package com.junne.myutils.conf.ehcache;

import net.sf.ehcache.CacheManager;
import org.apache.shiro.cache.ehcache.EhCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.net.URL;

/**
 * 描述:
 * 缓存配置类
 *
 * @author JUNNE
 * @create 2019-03-06 11:20
 */
@Configuration
public class CacheManagerConfig {
    private static final String PATH = "/ehcache.xml";

    @Bean
    public CacheManager getCacheManager(){
        URL url = getClass().getResource(PATH);
        return CacheManager.create(url);
    }

    @Bean("ehCacheManager")
    public EhCacheManager cacheManager() {
        EhCacheManager cacheManager = new EhCacheManager();
        cacheManager.setCacheManagerConfigFile("classpath:ehcache.xml");
        return cacheManager;
    }
}
