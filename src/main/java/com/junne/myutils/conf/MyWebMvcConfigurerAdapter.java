package com.junne.myutils.conf;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.ResourceUtils;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.io.File;

/**
 * 描述:
 * MVC配置
 *
 * @author JUNNE
 * @create 2019-09-18 11:11
 */
@Configuration
@ConfigurationProperties(prefix = "upload")
@EnableConfigurationProperties({MyWebMvcConfigurerAdapter.class})
public class MyWebMvcConfigurerAdapter implements WebMvcConfigurer {

    @Value("${upload.filepath}")
    private String filepath;

    @Value("${upload.uploadFilePrefix}")
    private String uploadFilePrefix;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/webapp/resources")
                .addResourceLocations(ResourceUtils.CLASSPATH_URL_PREFIX + "/resources/");
        //上传文件路径映射
        File fileFolder = new File(filepath);
        if (!fileFolder.exists()){
            fileFolder.mkdirs();
        }
        registry.addResourceHandler("/"+uploadFilePrefix+"/**")
                .addResourceLocations(ResourceUtils.FILE_URL_PREFIX + filepath + "/");
    }
}
