package com.junne.myutils.conf.aspect.securityaudit.annotation;

/**
 * 描述:
 * 安全审计类型
 *
 * @author JUNNE
 * @create 2019-12-11 9:32
 */
public enum SecurityAuditType {
    LOGIN(0,"login","登入系统"),
    LOGOUT(1,"logout","登出系统"),
    CHANGE_PWD(2,"changePwd","修改密码"),
    CREATE(3,"create","创建数据"),
    RETRIEVE(4,"retrieve","检索数据"),
    UPDATE(5,"update","更新数据"),
    DELETE(6,"delete","删除数据");

    private int index;
    private String name;
    private String description;

    SecurityAuditType(int index,String name,String description){
        this.index = index;
        this.name = name;
        this.description = description;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
