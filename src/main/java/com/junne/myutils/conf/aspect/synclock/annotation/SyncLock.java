package com.junne.myutils.conf.aspect.synclock.annotation;

import java.lang.annotation.*;

/**
 * 描述:
 * 同步锁
 *
 * @author JUNNE
 * @create 2019-12-16 11:27
 */
//注解的期限
@Retention(RetentionPolicy.RUNTIME)
//注解的目标
@Target(ElementType.METHOD)
@Documented
public @interface SyncLock {

    /**
     * 超时毫秒(默认0)
     * @return 返回超时的毫秒数
     */
    long timeOutMillisecond() default 0;

}
