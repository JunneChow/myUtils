package com.junne.myutils.conf.aspect.securityaudit.annotation;

import java.lang.annotation.*;

/**
 * 描述:
 * 安全审计注解
 *
 * @author JUNNE
 * @create 2019-08-27 14:11
 */
//注解的期限
@Retention(RetentionPolicy.RUNTIME)
//注解的目标
@Target(ElementType.METHOD)
@Documented
public @interface SecurityAudit {

    /**
     * 描述
     * @return 返回描述信息
     */
    String description();

    /**
     * 安全审计类型
     */
    SecurityAuditType securityAuditType();
}
