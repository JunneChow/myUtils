package com.junne.myutils.conf.aspect.securityaudit;

import com.google.gson.Gson;
import com.junne.myutils.conf.aspect.securityaudit.annotation.SecurityAudit;
import com.junne.myutils.conf.aspect.securityaudit.annotation.SecurityAuditType;
import com.junne.myutils.dto.SysUserDTO;
import com.junne.myutils.entity.SysUser;
import com.junne.myutils.entity.SystemLog;
import com.junne.myutils.qdutils.SpringUtil;
import com.junne.myutils.qdutils.api.ResultVO;
import com.junne.myutils.qdutils.jpa.builder.JpaQueryBuilder;
import com.junne.myutils.service.SysUserService;
import com.junne.myutils.service.SystemLogService;
import com.junne.myutils.utils.WebUtil;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * 描述:
 * 安全审计切面
 *
 * @author JUNNE
 * @create 2019-08-27 10:28
 */
@Aspect
@Component
public class SecurityAuditAspect {

    private static Logger log = LoggerFactory.getLogger(SecurityAuditAspect.class);

    /**
     * 安全审计的后置处理
     * @param joinPoint 连接点（这里是本系统中@SystemLog注解的方法）
     * @param ret 返回结果
     */
    @AfterReturning(value = "@annotation(com.junne.myutils.conf.aspect.securityaudit.annotation.SecurityAudit)",returning = "ret")
    public void doAfterReturning(JoinPoint joinPoint, Object ret){
        this.handleSecurityAuditJoinPoint(joinPoint,ret);
    }

    /**
     * 处理安全审计连接点
     * @param joinPoint 连接点
     * @param ret 返回结果
     */
    private void handleSecurityAuditJoinPoint(JoinPoint joinPoint, Object ret){
        SystemLog systemLog = new SystemLog();
        log.debug("后置返回通知============start");
        log.debug("请求时间："+new Date());
        //获取目标方法的参数信息
        Object[] args = joinPoint.getArgs();
        //用的最多 通知的签名
        Signature signature = joinPoint.getSignature();
        MethodSignature methodSignature = (MethodSignature) signature;//获取参数名
        SecurityAudit securityAudit = methodSignature.getMethod().getAnnotation(SecurityAudit.class);
        String description = securityAudit.description();
        log.debug("请求执行方法的描述："+description);
        String operationType = securityAudit.securityAuditType().getName();
        log.debug("请求执行方法的类型："+operationType);
        //代理的是哪一个方法
        String actionMethod = signature.getName();
        log.debug("请求执行的代理方法："+actionMethod);
        //AOP代理类的名字
        String actionClass = signature.getDeclaringTypeName();
        log.debug("请求执行的代理类："+actionClass);
        //获取RequestAttributes
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        //从获取RequestAttributes中获取HttpServletRequest的信息
        HttpServletRequest httpServletRequest = (HttpServletRequest) requestAttributes.resolveReference(RequestAttributes.REFERENCE_REQUEST);
        //获取请求的路径URL
        StringBuffer requestURL = httpServletRequest.getRequestURL();
        String url = requestURL.toString();
        log.debug("请求的URL路径："+url);
        //获取请求用户的IP地址
        String remoteAddr = null;
        remoteAddr = WebUtil.getIpAddress();
        log.debug("请求用户的IP地址："+remoteAddr);
        //如果要获取Session信息的话，可以这样写：
        HttpSession session = (HttpSession) requestAttributes.resolveReference(RequestAttributes.REFERENCE_SESSION);
        //获取请求用户的信息
        SysUserDTO sysUserDTO = WebUtil.getCurrentLogonUser();
        if(sysUserDTO != null){
            systemLog.setUserId(sysUserDTO.getId());
            systemLog.setUserName(sysUserDTO.getUserName());
            systemLog.setRoleName(sysUserDTO.getRoleName());
            log.debug("请求的用户ID："+sysUserDTO.getId());
            log.debug("请求的用户名称："+sysUserDTO.getUserName());
            log.debug("请求的用户角色："+sysUserDTO.getRoleName());
        }else {
            systemLog.setUserId("-");
            systemLog.setUserName("非法用户");
            systemLog.setRoleName("-");
        }
        //获取请求参数
        Enumeration<String> parameterNames = httpServletRequest.getParameterNames();
        Map<String,String> parameterMap = new HashMap<>();
        while (parameterNames.hasMoreElements()){
            String parameter = parameterNames.nextElement();
            SysUserService sysUserService = SpringUtil.getBean(SysUserService.class);
            SysUser opSysUser = null;
            //如果是登录或者退出操作
            if (sysUserDTO == null ){
                if ((securityAudit.securityAuditType().equals(SecurityAuditType.LOGIN) && "account".equals(parameter))
                        || (securityAudit.securityAuditType().equals(SecurityAuditType.CHANGE_PWD) && "userName".equals(parameter))){
                    Map<String, Object> queryMap = new JpaQueryBuilder().and("userName", httpServletRequest.getParameter(parameter)).build();
                    opSysUser = sysUserService.findOne(queryMap);
                } else if (securityAudit.securityAuditType().equals(SecurityAuditType.LOGOUT) && "id".equals(parameter)){
                    Map<String, Object> queryMap = new JpaQueryBuilder().and("id", httpServletRequest.getParameter(parameter)).build();
                    opSysUser = sysUserService.findOne(queryMap);
                }
                if (opSysUser != null){
                    SysUserDTO opSysUserDTO = sysUserService.convertToSysUserDTO(opSysUser, new SysUserDTO());
                    systemLog.setUserId(opSysUserDTO.getId());
                    systemLog.setUserName(opSysUserDTO.getUserName());
                    systemLog.setRoleName(opSysUserDTO.getRoleName());
                    log.debug("请求的用户ID："+opSysUserDTO.getId());
                    log.debug("请求的用户名称："+opSysUserDTO.getUserName());
                    log.debug("请求的用户角色："+opSysUserDTO.getRoleName());
                }
            }
            parameterMap.put(parameter,httpServletRequest.getParameter(parameter));
        }
        Gson gson = new Gson();
        String parameters = gson.toJson(parameterMap);
        if(args.length > 0) {
            log.debug("请求的参数内容："+parameters);
        }
        ResultVO apiResult = null;
        if (ret != null){
            apiResult = (ResultVO) ret;
            systemLog.setResultCode(apiResult.getResultCode());
            systemLog.setResultMsg(apiResult.getResultMsg());
            log.debug("后台返回的代码："+apiResult.getResultCode());
            log.debug("后台返回的提示："+apiResult.getResultMsg());
        }
        log.debug("后台返回的结果："+ret);
        systemLog.setActionClass(actionClass);
        systemLog.setActionMethod(actionMethod);
        systemLog.setActionTime(new Date());
        systemLog.setDescription(description);
        systemLog.setOperationType(operationType);
        systemLog.setUrl(url);
        if(parameters.length() >= 800) {
            parameters = parameters.substring(0, 797) + "...";
        }
        systemLog.setParameters(parameters);
        systemLog.setIp(remoteAddr);
        SpringUtil.getBean(SystemLogService.class).save(systemLog);
        log.debug("后置返回通知============end");
    }
}
