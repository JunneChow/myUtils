package com.junne.myutils.conf.aspect.synclock;

import com.junne.myutils.conf.aspect.synclock.annotation.SyncLock;
import com.junne.myutils.qdutils.api.ResultVOUtil;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


/**
 * 描述:
 * 同步锁切面
 *
 * @author JUNNE
 * @create 2019-12-16 11:25
 */
@Aspect
@Component
public class SyncLockAspect {

    private Map<String,Object> map = new ConcurrentHashMap<>();

    @Around(value = "@annotation(com.junne.myutils.conf.aspect.synclock.annotation.SyncLock)")
    public Object doAround(ProceedingJoinPoint pjp) throws Throwable {
        StringBuilder stringBuilder = new StringBuilder();
        Signature signature = pjp.getSignature();
        //获取代理类
        String actionClass = signature.getDeclaringTypeName();
        stringBuilder.append(actionClass);
        //获取代理方法
        String actionMethod = signature.getName();
        stringBuilder.append(actionMethod);
        MethodSignature methodSignature = (MethodSignature) signature;
        //获取方法参数类型
        Class[] parameterTypes = methodSignature.getParameterTypes();
        for (Class clazz:parameterTypes) {
            stringBuilder.append(clazz.getSimpleName());
        }
        String key = stringBuilder.toString();
        //提取超时毫秒数
        SyncLock syncLock = methodSignature.getMethod().getAnnotation(SyncLock.class);
        long timeOutMillisecond = syncLock.timeOutMillisecond();
        return this.handleProcess(pjp,key,timeOutMillisecond);
    }

    /**
     * 处理过程
     * @param pjp 待处理连接点
     * @param key 键值
     * @param timeOutMillisecond 超时毫秒数
     * @return 返回处理后的结果
     * @throws Throwable 抛出异常
     */
    private Object handleProcess(ProceedingJoinPoint pjp, String key,long timeOutMillisecond) throws Throwable {
        boolean success = false;
        try {
            if (timeOutMillisecond > 0){
                long ctm = System.currentTimeMillis();
                long limitTime = ctm + timeOutMillisecond;
                //timeOutMillisecond毫秒内不断重试
                while(limitTime > System.currentTimeMillis()){
                    if (this.tryLock(key,pjp)){
                        success = true;
                        return pjp.proceed();
                    }
                }
            }else {
                if (this.tryLock(key,pjp)){
                    success = true;
                    return pjp.proceed();
                }
            }
            return ResultVOUtil.fail("当前操作忙，请稍候！");
        }finally {
            if (success){
                map.remove(key);
            }
        }
    }

    /**
     * 尝试加锁
     * @param key 键值
     * @return 返回是否成功加锁
     */
    private boolean tryLock(String key,ProceedingJoinPoint pjp){
        MethodSignature signature = (MethodSignature) pjp.getSignature();
        synchronized (signature.getMethod()){
            if (!map.containsKey(key)){
                map.put(key,true);
                return true;
            }
        }
        return false;
    }
}
