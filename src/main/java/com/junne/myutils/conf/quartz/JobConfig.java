package com.junne.myutils.conf.quartz;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

import javax.sql.DataSource;
import java.io.IOException;

/**
 * 描述:
 * Quartz配置类
 *
 * @author JUNNE
 * @create 2019-12-02 10:53
 */
@Configuration
public class JobConfig {

    @Bean
    public SchedulerFactoryBean scheduler(@Qualifier("dataSource") DataSource dataSource) {
        SchedulerFactoryBean quartzScheduler = new SchedulerFactoryBean();

        try {
            quartzScheduler.setQuartzProperties(
                    PropertiesLoaderUtils.loadProperties(new ClassPathResource("quartz.properties")));
        } catch (IOException e) {
            e.printStackTrace();
        }
        quartzScheduler.setDataSource(dataSource);
        // 用于quartz集群,QuartzScheduler 启动时更新己存在的Job
        quartzScheduler.setOverwriteExistingJobs(true);
        //延长启动
        quartzScheduler.setStartupDelay(10);
        return quartzScheduler;
    }
}
