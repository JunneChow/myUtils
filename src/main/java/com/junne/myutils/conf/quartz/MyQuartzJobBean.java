package com.junne.myutils.conf.quartz;

import com.junne.myutils.qdutils.exception.WarningException;
import com.junne.myutils.entity.SysJob;
import com.junne.myutils.qdutils.SpringUtil;
import org.quartz.*;
import org.springframework.aop.support.AopUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Set;

/**
 * 描述:
 * 系统作业bean
 *
 * @author JUNNE
 * @create 2019-12-02 14:43
 */
@Component
@DisallowConcurrentExecution
public class MyQuartzJobBean extends QuartzJobBean {

    @Autowired
    private Scheduler scheduler;

    private static final String JOB_DATA_KEY = "JOB_DATA_KEY";

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) {
        this.executeJob(jobExecutionContext.getJobDetail());
    }

    /**
     * 安排作业
     * @param sysJob 作业实体
     * @throws WarningException 抛出异常
     */
    public void scheduleJob (SysJob sysJob) throws WarningException {
        this.checkJobModel(sysJob);
        String jobName = sysJob.getJobName();

        JobKey jobKey = JobKey.jobKey(jobName);
        JobDetail jobDetail = JobBuilder.newJob(MyQuartzJobBean.class).storeDurably()
                .withDescription(sysJob.getDescription()).withIdentity(jobKey).build();
        jobDetail.getJobDataMap().put(JOB_DATA_KEY, sysJob);

        CronScheduleBuilder cronScheduleBuilder = CronScheduleBuilder.cronSchedule(sysJob.getCron());
        CronTrigger cronTrigger = TriggerBuilder.newTrigger().withIdentity(jobName)
                .withSchedule(cronScheduleBuilder).forJob(jobKey).build();
        try {
            boolean isJobKeyExist = scheduler.checkExists(jobKey);
            if (isJobKeyExist){
                scheduler.rescheduleJob(new TriggerKey(jobName),cronTrigger);
                scheduler.addJob(jobDetail,true);
            }else {
                scheduler.scheduleJob(jobDetail,cronTrigger);
            }
        } catch (SchedulerException e) {
            e.printStackTrace();
        }
    }

    /**
     * 解除作业
     * @param sysJob 作业实体
     * @throws WarningException 抛出异常
     */
    public void unScheduleJob(SysJob sysJob) throws WarningException {
        this.checkJobModel(sysJob);
        String jobName = sysJob.getJobName();
        JobKey jobKey = JobKey.jobKey(jobName);

        try {
            scheduler.pauseJob(jobKey);
            scheduler.unscheduleJob(new TriggerKey(jobName));
            scheduler.deleteJob(jobKey);
        } catch (SchedulerException e) {
            e.printStackTrace();
        }
    }

    /**
     * 校验作业实体
     * @param sysJob 作业实体
     * @throws WarningException 抛出异常
     */
    public void checkJobModel(SysJob sysJob) throws WarningException {
        String springBeanName = sysJob.getSpringBeanName();
        boolean isBeanExist = SpringUtil.getApplicationContext().containsBean(springBeanName);
        if (!isBeanExist){
            throw new WarningException("该作业的bean类不存在！");
        }
        Object object = SpringUtil.getApplicationContext().getBean(springBeanName);
        Class<?> clazz = object.getClass();
        //如果是代理对象则取其父类
        if (AopUtils.isAopProxy(object)){
            clazz = clazz.getSuperclass();
        }
        String methodName = sysJob.getMethodName();
        Method[] methods = clazz.getMethods();
        Set<String> methodSet = new HashSet<>();
        for (Method method:methods) {
            Class<?>[] parameterTypes = method.getParameterTypes();
            if (parameterTypes.length == 0){
                methodSet.add(method.getName());
            }
        }
        if (methodSet.size()==0){
            throw new WarningException("该作业bean类不存在无参方法！");
        }
        if (!methodSet.contains(methodName)){
            throw new WarningException("未能找到方法名为"+methodName+"的无参方法！");
        }
    }

    /**
     * 执行作业
     * @param jobDetail 作业详情
     */
    public void executeJob(JobDetail jobDetail) {
        SysJob sysJob = (SysJob) jobDetail.getJobDataMap().get(JOB_DATA_KEY);

        String springBeanName = sysJob.getSpringBeanName();
        String methodName = sysJob.getMethodName();
        Object object = SpringUtil.getApplicationContext().getBean(springBeanName);

        try {
            Method method = object.getClass().getDeclaredMethod(methodName);
            method.invoke(object);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }
}
