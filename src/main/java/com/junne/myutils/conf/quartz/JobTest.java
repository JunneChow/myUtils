package com.junne.myutils.conf.quartz;

import org.springframework.stereotype.Component;

/**
 * 描述:
 * 作业测试
 *
 * @author JUNNE
 * @create 2019-12-02 17:14
 */
@Component
public class JobTest {

    public void printSomething(){
        System.out.println("hello world1111");
    }

    public void printSomething2(){
        System.out.println("hello world2222");
    }

    public void printSomething3(){
        System.out.println("hello world3333");
    }
}
