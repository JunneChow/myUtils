package com.junne.myutils.common;

/**
 * 描述:
 * 公共配置信息
 *
 * @author JUNNE
 * @create 2019-10-24 17:13
 */
public class ConstantsCfg {
    public static final String EHCACHE_PREFIX = "MyUtils";
    public static final String VERIFY_CODE = "verifyCode";
    public static final String VERIFY_CODE_TYPE_LOGIN = "loginVerifyCode";
    public static final String VERIFY_CODE_TYPE_CHANGE_PWD = "changePwdVerifyCode";

    public static final String CURRENT_LOGON_USER = "SysUser";
    public static final String CURRENT_LOGON_USER_PERMISSION_LIST = "SysUserPermissionList";
    public static final int HASH_ITERATIONS = 3;

    public static final int USER_STATE_DISABLED = 0;
    public static final int USER_STATE_VALID = 1;
    public static final int USER_STATE_LOCKED = 2;
    public static final String USER_DISABLED_OR_LOCKED = "该用户未激活或被锁定，请联系管理员！";

    public static final String SYS_USER_LOGIN_SESSIONID = "SysUserLoginSession";
    public static final String SYS_USER_LOGIN_IP = "SysUserLoginIp";

    public static final String RSA_PUBLIC_KEY = "RsaPublicKey";
    public static final String RSA_PRIVATE_KEY = "RsaPrivateKey";

    public static final String API_REQUEST = "Api-Request";
}
