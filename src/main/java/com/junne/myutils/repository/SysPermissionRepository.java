package com.junne.myutils.repository;

import com.junne.myutils.entity.SysPermission;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;

/**
 * 描述:
 * ${DESCRIPTION}
 *
 * @author JUNNE
 * @create 2019-09-19 17:22
 */
public interface SysPermissionRepository extends JpaRepositoryImplementation<SysPermission,String> {
}
