package com.junne.myutils.repository;

import com.junne.myutils.entity.SystemLog;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;

/**
 * 描述:
 * ${DESCRIPTION}
 *
 * @author JUNNE
 * @create 2019-09-19 17:20
 */
public interface SystemLogRepository extends JpaRepositoryImplementation<SystemLog,String> {
}
