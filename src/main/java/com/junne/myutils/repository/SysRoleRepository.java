package com.junne.myutils.repository;

import com.junne.myutils.entity.SysRole;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;

/**
 * 描述:
 * ${DESCRIPTION}
 *
 * @author JUNNE
 * @create 2019-09-19 17:21
 */
public interface SysRoleRepository extends JpaRepositoryImplementation<SysRole,String> {
}
