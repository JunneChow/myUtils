package com.junne.myutils.repository;

import com.junne.myutils.entity.SysUser;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import org.springframework.data.repository.query.Param;

import javax.persistence.LockModeType;
import javax.persistence.QueryHint;

/**
 * 描述:
 * ${DESCRIPTION}
 *
 * @author JUNNE
 * @create 2019-09-19 17:18
 */
public interface SysUserRepository extends JpaRepositoryImplementation<SysUser,String> {

    @Lock(value = LockModeType.PESSIMISTIC_WRITE)
    @QueryHints({@QueryHint(name = "javax.persistence.lock.timeout",value = "3000")})
    @Query("select t from SysUser t where t.id = :id")
    SysUser findByIdForUpdate(@Param("id") String id);
}
