package com.junne.myutils.repository;

import com.junne.myutils.entity.SysJob;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import org.springframework.data.repository.query.Param;

import javax.persistence.LockModeType;
import javax.persistence.QueryHint;

/**
 * 描述:
 * 系统作业Repository
 *
 * @author JUNNE
 * @create 2019-12-02 15:36
 */
public interface SysJobRepository extends JpaRepositoryImplementation<SysJob,String> {

    @Lock(value = LockModeType.PESSIMISTIC_WRITE)
    @QueryHints({@QueryHint(name = "javax.persistence.lock.timeout",value = "3000")})
    @Query("select t from SysJob t where t.id = :id")
    SysJob findByIdForUpdate(@Param("id") String id);
}
