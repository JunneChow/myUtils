package com.junne.myutils.entity;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * 描述:
 * 实体基础类
 *
 * @author JUNNE
 * @create 2019-09-19 16:06
 */
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public class BaseEntity implements Serializable  {

    private static final long serialVersionUID = -8068511913219016617L;

    @GenericGenerator(name = "sys-uuid",strategy = "uuid")
    @GeneratedValue(generator = "sys-uuid")
    @Id
    @Column(name = "ID",columnDefinition = "varchar(32) COMMENT '表ID'")
    private String id;

    @Column(columnDefinition = "int COMMENT '是否启用（1：启用，0：禁用）'")
    private int enable=1;

    @LastModifiedBy
    @Column(columnDefinition = "varchar(32) COMMENT '更新者'")
    private String updateBy;

    @LastModifiedDate
    @Column(columnDefinition = "TIMESTAMP(6) COMMENT '更新时间'")
    private Date updateTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getEnable() {
        return enable;
    }

    public void setEnable(int enable) {
        this.enable = enable;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
