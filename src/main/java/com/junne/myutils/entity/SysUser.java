package com.junne.myutils.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.List;

/**
 * 描述:
 * 系统用户
 *
 * @author JUNNE
 * @create 2019-08-29 14:48
 */
@Entity
@Table(name = "sys_user")
public class SysUser extends BaseEntity{

    private static final long serialVersionUID = -5156640151903880313L;

    @Column(columnDefinition = "varchar(30) COMMENT '账号'")
    private String userName;

    @Column(columnDefinition = "varchar(50) COMMENT '密码'")
    private String password;

    @JsonIgnore
    @Column(columnDefinition = "varchar(50) COMMENT '盐值'")
    private String salt;

    @Column(columnDefinition = "varchar(30) COMMENT '昵称'")
    private String nickName;

    @Column(columnDefinition = "int COMMENT '用户状态（0：未验证使用，1：正常使用，2：用户被锁定）'")
    private int state=0;  //用户状态（0：未验证使用，1：正常使用，2：用户被锁定）

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "SysUserRole",
            joinColumns = {@JoinColumn(name = "userId",foreignKey = @ForeignKey(name = "none",value = ConstraintMode.NO_CONSTRAINT))},
            inverseJoinColumns = {@JoinColumn(name = "roleId",foreignKey = @ForeignKey(name = "none",value = ConstraintMode.NO_CONSTRAINT))})
    @JsonIgnoreProperties(value = "userList")
    private List<SysRole> sysRoleList;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public List<SysRole> getSysRoleList() {
        return sysRoleList;
    }

    public void setSysRoleList(List<SysRole> sysRoleList) {
        this.sysRoleList = sysRoleList;
    }
}
