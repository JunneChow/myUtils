package com.junne.myutils.entity;

/**
 * 描述:
 * ${DESCRIPTION}
 *
 * @author JUNNE
 * @create 2019-12-18 12:25
 */
public class Student extends BaseEntity {
    String name;
    String sex;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }
}
