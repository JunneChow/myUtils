package com.junne.myutils.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

/**
 * 描述:
 * 系统日志实体类
 *
 * @author JUNNE
 * @create 2019-08-27 16:26
 */
@Entity
@Table(name = "sys_log")
public class SystemLog extends BaseEntity{

    private static final long serialVersionUID = 638426917930678472L;

    @Column(columnDefinition = "TIMESTAMP(6) null COMMENT '请求的动作时间'")
    private Date actionTime=new Date();          //请求的动作时间
    @Column(columnDefinition = "varchar(80) COMMENT '请求的动作类'")
    private String actionClass;         //请求的动作类
    @Column(columnDefinition = "varchar(30) COMMENT '请求的动作方法名'")
    private String actionMethod;        //请求的动作方法名
    @Column(columnDefinition = "varchar(100) COMMENT '请求的操作描述'")
    private String description;         //请求的操作描述
    @Column(columnDefinition = "varchar(10) COMMENT '请求的操作类型'")
    private String operationType;       //请求的操作类型
    @Column(columnDefinition = "varchar(300) COMMENT '请求的URL'")
    private String url;                 //请求的URL
    @Column(columnDefinition = "varchar(800) COMMENT '请求的参数'")
    private String parameters;          //请求的参数
    @Column(columnDefinition = "varchar(20) COMMENT '请求的IP'")
    private String ip;                  //请求的IP
    @Column(columnDefinition = "varchar(32) COMMENT '请求用户ID'")
    private String userId;              //请求用户ID
    @Column(columnDefinition = "varchar(30) COMMENT '请求的用户名称'")
    private String userName;            //请求的用户名称
    @Column(columnDefinition = "varchar(50)")
    private String roleName;    //系统角色
    @Column(columnDefinition = "varchar(5) COMMENT '返回的代码'")
    private String resultCode;          //返回的代码
    @Column(columnDefinition = "varchar(100) COMMENT '返回的提示信息'")
    private String resultMsg;           //返回的提示信息

    public Date getActionTime() {
        return actionTime;
    }

    public void setActionTime(Date actionTime) {
        this.actionTime = actionTime;
    }

    public String getActionClass() {
        return actionClass;
    }

    public void setActionClass(String actionClass) {
        this.actionClass = actionClass;
    }

    public String getActionMethod() {
        return actionMethod;
    }

    public void setActionMethod(String actionMethod) {
        this.actionMethod = actionMethod;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOperationType() {
        return operationType;
    }

    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getParameters() {
        return parameters;
    }

    public void setParameters(String parameters) {
        this.parameters = parameters;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getResultCode() {
        return resultCode;
    }

    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }

    public String getResultMsg() {
        return resultMsg;
    }

    public void setResultMsg(String resultMsg) {
        this.resultMsg = resultMsg;
    }
}
