package com.junne.myutils.entity;

import javax.persistence.*;
import java.util.List;

/**
 * 描述:
 * ${DESCRIPTION}
 *
 * @author JUNNE
 * @create 2019-09-19 16:31
 */
@Entity
@Table(name = "sys_permission")
public class SysPermission extends BaseEntity {

    private static final long serialVersionUID = -5510555127695858909L;

    @Column(name = "parent_id",columnDefinition = "varchar(32) COMMENT '父级ID'")
    private String parentId;

    @Column(columnDefinition = "varchar(30) COMMENT '名称'")
    private String permissionName;

    @Column(columnDefinition = "varchar(100) COMMENT '样式'")
    private String css;

    @Column(columnDefinition = "varchar(300) COMMENT 'href地址'")
    private String href;

    @Column(columnDefinition = "int COMMENT '类型'")
    private Integer type;

    @Column(columnDefinition = "varchar(30) COMMENT '权限'")
    private String permission;

    @Column(columnDefinition = "int COMMENT '序号'")
    private Integer sort;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "SysRolePermission",
            joinColumns = {@JoinColumn(name = "permissionId", foreignKey = @ForeignKey(name = "none",value = ConstraintMode.NO_CONSTRAINT))},
            inverseJoinColumns = {@JoinColumn(name = "roleId", foreignKey = @ForeignKey(name = "none",value = ConstraintMode.NO_CONSTRAINT))})
    private List<SysRole> roleList;

    @OneToMany(cascade = CascadeType.DETACH,fetch = FetchType.LAZY)
    @OrderBy("sort ASC")
    @JoinColumn(name = "parent_id", foreignKey = @ForeignKey(name = "none",value = ConstraintMode.NO_CONSTRAINT))
    private List<SysPermission> childList;

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getPermissionName() {
        return permissionName;
    }

    public void setPermissionName(String permissionName) {
        this.permissionName = permissionName;
    }

    public String getCss() {
        return css;
    }

    public void setCss(String css) {
        this.css = css;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public List<SysRole> getRoleList() {
        return roleList;
    }

    public void setRoleList(List<SysRole> roleList) {
        this.roleList = roleList;
    }

    public List<SysPermission> getChildList() {
        return childList;
    }

    public void setChildList(List<SysPermission> childList) {
        this.childList = childList;
    }
}
