package com.junne.myutils.entity;

import javax.persistence.*;


/**
 * 描述:
 * 系统作业实体类
 *
 * @author JUNNE
 * @create 2019-12-02 14:29
 */
@Entity
@Table(name = "sys_job")
public class SysJob extends BaseEntity {

    private static final long serialVersionUID = 3243773452927735008L;

    @Column(columnDefinition = "varchar(30) COMMENT '作业名称'")
    private String jobName;

    @Column(columnDefinition = "varchar(30) COMMENT '作业描述'")
    private String description;

    @Column(columnDefinition = "varchar(30) COMMENT '作业时间调度'")
    private String cron;

    @Column(columnDefinition = "varchar(30) COMMENT '作业类bean'")
    private String springBeanName;

    @Column(columnDefinition = "varchar(30) COMMENT '作业方法名称'")
    private String methodName;

    @Column(columnDefinition = "varchar(1) COMMENT '作业状态（1：启用，0：停止）'")
    private String status;

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCron() {
        return cron;
    }

    public void setCron(String cron) {
        this.cron = cron;
    }

    public String getSpringBeanName() {
        return springBeanName;
    }

    public void setSpringBeanName(String springBeanName) {
        this.springBeanName = springBeanName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
