package com.junne.myutils.websocket;

import com.google.gson.Gson;
import com.junne.myutils.qdutils.api.ResultVO;
import com.junne.myutils.qdutils.api.ResultVOUtil;

import javax.websocket.CloseReason;
import javax.websocket.Session;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 描述:
 * webSocket管理器
 *
 * @author JUNNE
 * @create 2020-01-16 15:43
 */
public class MyWebsocketManager {

    private Map<String,Session> sessionMap = new ConcurrentHashMap<>();

    public void putSession(String key,Session session){
        sessionMap.put(key,session);
    }

    public Session getSession(String key){
        return sessionMap.get(key);
    }

    public void removeSession(String key){
        sessionMap.remove(key);
    }

    public int getOnlineCount(){
        return sessionMap.size();
    }

    public void online(String key,Session session) throws IOException {
        Session oldSession = this.getSession(key);
        if (oldSession != null){
            ResultVO fail = ResultVOUtil.fail("您的账号在异地登录，当前操作被迫下线！");
            this.sendObject(oldSession,fail);
            oldSession.close(new CloseReason(CloseReason.CloseCodes.CLOSED_ABNORMALLY,fail.getResultMsg()));
        }
        this.putSession(key,session);
    }

    public void offline(String key){
        this.removeSession(key);
    }

    public void broadcastMsg(String message){
        for (Map.Entry<String,Session> entry:sessionMap.entrySet()) {
            Session session = entry.getValue();
            this.sendMsg(session,message);
        }
    }

    public void sendPrivateMsg(String key,String message){
        Session session = this.getSession(key);
        this.sendMsg(session,message);
    }

    private synchronized void sendObject(Session session,Object object){
        Gson gson = new Gson();
        String json = gson.toJson(object);
        try {
            session.getBasicRemote().sendText(json);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private synchronized void sendMsg(Session session,String message){
        try {
            session.getBasicRemote().sendText(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
