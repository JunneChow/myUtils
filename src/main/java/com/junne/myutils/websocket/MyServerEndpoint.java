package com.junne.myutils.websocket;

import com.junne.myutils.common.ConstantsCfg;
import com.junne.myutils.conf.aspect.securityaudit.annotation.SecurityAuditType;
import com.junne.myutils.dto.SysUserDTO;
import com.junne.myutils.entity.SystemLog;
import com.junne.myutils.qdutils.SpringUtil;
import com.junne.myutils.qdutils.api.enums.ResultEnum;
import com.junne.myutils.qdutils.exception.WarningException;
import com.junne.myutils.repository.SystemLogRepository;
import com.junne.myutils.utils.WebUtil;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpSession;
import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import javax.websocket.server.ServerEndpointConfig;
import java.io.IOException;
import java.util.Date;

/**
 * 描述:
 * webSocket服务端点
 *
 * @author JUNNE
 * @create 2020-01-15 14:45
 */
@Component
@ServerEndpoint(value = "/myWebSocketServer/{key}",configurator = MyServerEndpointConfig.class)
public class MyServerEndpoint {

    private String key;
    private Session session;
    private MyWebsocketManager myWebsocketManager;

    private HttpSession httpSession;

    @OnOpen
    public void handleOpen(@PathParam(value = "key") String key,
                           Session session,
                           EndpointConfig endpointConfig) throws WarningException {
        //初始化httpSession
        this.httpSession = (HttpSession) endpointConfig.getUserProperties().get(HttpSession.class.getName());
        //通过配置类注入管理器
        ServerEndpointConfig serverEndpointConfig = (ServerEndpointConfig) endpointConfig;
        MyServerEndpointConfig myServerEndpointConfig = (MyServerEndpointConfig) serverEndpointConfig.getConfigurator();
        this.myWebsocketManager = myServerEndpointConfig.getMyWebsocketManager();
        if ("normal".equals(key)){
            this.key = session.getId();
        }else {
            this.key = key;
        }
        this.session = session;
        try {
            this.myWebsocketManager.online(this.key, session);
        } catch (IOException e) {
            throw new WarningException("websocket登录失败！请联系管理员！");
        }
        String successMsg = "【"+this.key+"】已上线！";
        String broadcastMsg = successMsg+"当前在线人数：【"+ this.myWebsocketManager.getOnlineCount()+"】";
        this.myWebsocketManager.broadcastMsg(broadcastMsg);
    }

    @OnMessage
    public void handleMessage(String message){
        if ("[/hello]".equals(message)){
            String returnMsg = "您好！欢迎上线！";
            this.myWebsocketManager.sendPrivateMsg(this.key,returnMsg);
        }
    }

    @OnError
    public void handleError(Throwable t){
        boolean open = this.session.isOpen();
        if (open){
            System.out.println("服务异常："+t.getMessage());
            t.printStackTrace();
        }
    }

    @OnClose
    public void handleClose(){
        SysUserDTO sysUserDTO = (SysUserDTO) this.httpSession.getAttribute(ConstantsCfg.CURRENT_LOGON_USER);
        String ip = (String) this.httpSession.getAttribute(ConstantsCfg.SYS_USER_LOGIN_IP);
        SysUserDTO currentLogonUser = WebUtil.getCurrentLogonUser();
        this.myWebsocketManager.offline(this.key);
        try {
            Thread.sleep(5000);
            Session session = this.myWebsocketManager.getSession(this.key);
            if (session == null){
                //记录退出操作，正常退出情况下currentLogonUser不为null
                if (currentLogonUser == null && sysUserDTO != null){
                    SystemLog systemLog = new SystemLog();
                    systemLog.setUserId(sysUserDTO.getId());
                    systemLog.setUserName(sysUserDTO.getUserName());
                    systemLog.setRoleName(sysUserDTO.getRoleName());
                    systemLog.setActionTime(new Date());
                    systemLog.setDescription("非正常退出系统");
                    systemLog.setOperationType(SecurityAuditType.LOGOUT.getName());
                    systemLog.setIp(ip);
                    systemLog.setResultCode(ResultEnum.SUCCESS.getCode());
                    systemLog.setResultMsg("注销成功！");
                    //SpringUtil.getBean(SystemLogRepository.class).saveAndFlush(systemLog);
                }
                String successMsg = "【"+this.key+"】已下线！";
                String broadcastMsg = successMsg+"当前在线人数：【"+ this.myWebsocketManager.getOnlineCount()+"】";
                this.myWebsocketManager.broadcastMsg(broadcastMsg);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
