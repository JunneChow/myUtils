package com.junne.myutils.websocket;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.http.HttpSession;
import javax.websocket.HandshakeResponse;
import javax.websocket.server.HandshakeRequest;
import javax.websocket.server.ServerEndpointConfig;

/**
 * 描述:
 * websocket配置类
 *
 * @author JUNNE
 * @create 2019-10-30 17:22
 */
@Configuration
public class MyServerEndpointConfig  extends ServerEndpointConfig.Configurator {
    private static MyWebsocketManager myWebsocketManager;

    public MyServerEndpointConfig() {
        myWebsocketManager = new MyWebsocketManager();
    }

    @Bean(name = "myWebsocketManager")
    public MyWebsocketManager getMyWebsocketManager(){
        return myWebsocketManager;
    }

    @Override
    public void modifyHandshake(ServerEndpointConfig sec, HandshakeRequest request, HandshakeResponse response) {
        HttpSession httpSession = (HttpSession) request.getHttpSession();
        sec.getUserProperties().put(HttpSession.class.getName(),httpSession);
    }
}
