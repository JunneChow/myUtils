package com.junne.myutils.utils;

import java.util.Random;

/**
 * 描述:
 * 重新排序工具
 *
 * @author JUNNE
 * @create 2020/10/24 12:04
 */
public class ReorderUtil {

    /**
     * 获取乱序因子
     * @param len 长度
     * @return 返回结果
     */
    public static String getFactor(int len){
        //交换个数
        int n = (len+1)/2;
        Random r = new Random();
        StringBuilder k = new StringBuilder();
        for (int i=1;i<=n;i++){
            int rk = r.nextInt(len);
            k.append(rk).append(",");
        }
        k.deleteCharAt(k.length()-1);
        return k.toString();
    }

    /**
     * 按乱序因子进行重排序
     * @param strArr 数组
     * @param factor 乱序因子
     */
    public static void reorderArray(String[] strArr,String factor){
        int len = strArr.length;
        //交换个数
        int n = (len+1)/2;
        String[] fs = factor.split(",");
        for (int i=1;i<=n;i++){
            //交互位置
            int p = 2*i-2;
            String temp = strArr[p];
            Integer cn = Integer.valueOf(fs[i-1]);
            strArr[p] = strArr[cn];
            strArr[cn] = temp;
        }
    }

    /**
     * 获取重排序后的答案
     * @param len 长度
     * @param factor 乱序因子
     * @param answer 原答案
     * @return 返回乱序后的答案
     */
    public static String getReorderAnswer(int len,String factor,String answer){
        String[] oa = ReorderUtil.handleReorder(len, factor);
        String[] aa = answer.split("");
        String na="";
        for (String a: aa) {
            //转换成数字
            a = ReorderUtil.letterToNum(a);
            for(int j=0;j<oa.length;j++){
                if (oa[j].equals(a)){
                    //数字转字母
                    String jst = ReorderUtil.numToLetter(j+"");
                    na = na+jst;
                }
            }
        }
        return ReorderUtil.sort(na);
    }

    /**
     * 获取原答案
     * @param len 长度
     * @param factor 乱序因子
     * @param reOrderAnswer 重排序后的答案
     * @return 返回原答案
     */
    public static String getRightAnswer(int len,String factor,String reOrderAnswer){
        String[] oa = ReorderUtil.handleReorder(len, factor);
        String[] aa = reOrderAnswer.split("");
        String na = "";
        for (String a: aa) {
            //转换成数字
            a = ReorderUtil.letterToNum(a);
            String jst = ReorderUtil.numToLetter(oa[Integer.valueOf(a)]+"");
            na = na+jst;
        }
        return ReorderUtil.sort(na);
    }

    /**
     * 字符串排序
     * @param str 字符串
     * @return 返回排序后结果
     */
    private static String sort(String str){
        String[] split = str.split("");
        ReorderUtil.insertSort(split);
        String rs = "";
        for (String s: split) {
            rs = rs+s;
        }
        return rs;
    }

    /**
     * 判断是否小于
     * @param v 比较数V
     * @param w 比较数W
     * @return 返回布尔值：true:小于，false:大于
     */
    public static boolean less(Comparable v,Comparable w){
        return v.compareTo(w) < 0;
    }

    /**
     * 数组中交换位置
     * @param a 数组
     * @param i 位置i
     * @param j 位置j
     */
    public static void exch(Comparable[] a,int i,int j){
        Comparable t = a[i];
        a[i] = a[j];
        a[j] = t;
    }

    /**
     * 插入排序
     * @param sa 数组
     */
    public static void insertSort(Comparable[] sa){
        for (int i=0;i<sa.length;i++){
            for (int j=i;j>0&&ReorderUtil.less(sa[j],sa[j-1]);j--){
                ReorderUtil.exch(sa,j,j-1);
            }
        }
    }

    /**
     * 处理重排序
     * @param len 长度
     * @param factor 乱序因子
     * @return 返回排序的数组
     */
    private static String[] handleReorder(int len,String factor){
        String[] oa = new String[len];
        //还原选项
        for(int i=0;i<len;i++){
            oa[i] = i+"";
        }
        //重排序
        ReorderUtil.reorderArray(oa,factor);
        return oa;
    }

    /**
     * 数字转字母
     * @param numStr 数字字符串
     * @return 返回字母
     */
    private static String numToLetter(String numStr){
        if (numStr == null){
            return null;
        }
        String letter = null;
        switch (numStr){
            case "0": letter = "A";break;
            case "1": letter = "B";break;
            case "2": letter = "C";break;
            case "3": letter = "D";break;
            case "4": letter = "E";break;
            case "5": letter = "F";break;
            case "6": letter = "G";break;
            case "7": letter = "H";break;
            case "8": letter = "I";break;
            case "9": letter = "J";break;
            case "10": letter = "K";break;
            default:letter=null;
        }
        return letter;
    }

    /**
     * 字母转数字
     * @param letter 字母
     * @return 返回数字
     */
    private static String letterToNum(String letter){
        if (letter == null){
            return null;
        }
        //强制换大写进行比较
        letter = letter.toUpperCase();
        String num = null;
        switch (letter){
            case "A": num = "0";break;
            case "B": num = "1";break;
            case "C": num = "2";break;
            case "D": num = "3";break;
            case "E": num = "4";break;
            case "F": num = "5";break;
            case "G": num = "6";break;
            case "H": num = "7";break;
            case "I": num = "8";break;
            case "J": num = "9";break;
            case "K": num = "10";break;
            default:num=null;
        }
        return num;
    }

    public static void main(String[] args) {
        long l = System.currentTimeMillis();
        System.out.println(l);
        String factor = ReorderUtil.getFactor(4);
        System.out.println("factor:"+factor);
        String question = "3天,,5天,,7天,,9天";
        String[] qsa = question.split(",,");
        ReorderUtil.reorderArray(qsa, factor);
        String reorderAnswer = ReorderUtil.getReorderAnswer(qsa.length, factor, "AC");
        System.out.println(reorderAnswer);
        String rightAnswer = ReorderUtil.getRightAnswer(qsa.length, factor, reorderAnswer);
        System.out.println(rightAnswer);
        System.out.println("总耗时："+(System.currentTimeMillis()-l)+"毫秒！");
    }
}
