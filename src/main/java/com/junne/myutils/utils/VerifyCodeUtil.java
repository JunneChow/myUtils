package com.junne.myutils.utils;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Random;

/**
 * 描述:
 * 验证码生成工具
 *
 * @author JUNNE
 * @create 2019-08-26 17:54
 */
public class VerifyCodeUtil {
    private int weight = 116;             //验证码图片的长和宽
    private int height = 45;
    private String text;                //用来保存验证码的文本内容
    private Random random = new Random();      //获取随机数对象
    private String[] fontNames = {"Arial","Times New Roman","黑体"};   //字体数组

    /**
     * 带宽高的构造器
     * @param weight 宽
     * @param height 高
     */
    public VerifyCodeUtil(int weight, int height) {
        this.weight = weight;
        this.height = height;
    }

    /**
     * 默认构造器
     */
    public VerifyCodeUtil() {
    }

    /**
     * 获取随机字符
     * @return 返回字符
     */
    private char randomChar() {
        //验证码数组
        String codeStr = "23456789abcdefghjkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ";
        int index = random.nextInt(codeStr.length());
        return codeStr.charAt(index);
    }

    /**
     * 获取随机颜色
     * @return 返回颜色对象
     */
    private Color randomColor() {
        //这里为什么是150，因为当r，g，b都为255时，即为白色，为了好辨认，需要颜色深一点。
        int random = this.random.nextInt(150);
        int g = this.random.nextInt(150);
        int b = this.random.nextInt(150);
        //返回一个随机颜色
        return new Color(random, g, b);
    }

    /**
     * 给定范围获得随机颜色
     * @param min 最小值
     * @param max 最大值
     * @return 返回颜色对象
     */
    private Color getRandColor(int min, int max) {
        Random random = new Random();
        if (min > 255) {
            min = 255;
        }
        if (max > 255) {
            max = 255;
        }
        int r = min + random.nextInt(max - min);
        int g = min + random.nextInt(max - min);
        int b = min + random.nextInt(max - min);
        return new Color(r, g, b);
    }

    /**
     * 获取随机字体
     * @return 返回字体对象
     */
    private Font randomFont() {
        //获取随机的字体
        int index = random.nextInt(fontNames.length);
        String fontName = fontNames[index];
        //随机获取字体的样式，0是无样式，1是加粗，2是斜体，3是加粗加斜体
        int style = random.nextInt(4);
        //随机获取字体的大小
        int size = random.nextInt(5) + 35;
        //返回一个随机的字体
        return new Font(fontName, style, size);
    }

    /**
     * 创建图片
     * @return 返回图形对象
     */
    private BufferedImage createImage() {
        //创建图片缓冲区
        BufferedImage image = new BufferedImage(weight, height, BufferedImage.TYPE_INT_RGB);
        //获取画笔
        Graphics2D g = (Graphics2D) image.getGraphics();
        // 设定图像背景色(因为是做背景，所以偏淡)
        g.setColor(getRandColor(200, 250));
        g.fillRect(0, 0, weight, height);
        //返回一个图片
        return image;
    }

    /**
     * 画验证码文字
     * @param image 图形对象
     */
    private void drawText(BufferedImage image){
        //获取画笔
        Graphics2D g = (Graphics2D) image.getGraphics();
        StringBuilder sb = new StringBuilder();
        //画四个字符即可
        for (int i = 0; i < 4; i++) {
            //随机生成字符，因为只有画字符串的方法，没有画字符的方法，所以需要将字符变成字符串再画
            String s = randomChar() + "";
            //添加到StringBuilder里面
            sb.append(s);
            //定义字符的x坐标
            float x = i * 1.0F * weight / 4;
            //设置字体，随机
            g.setFont(randomFont());
            //设置颜色，随机
            g.setColor(randomColor());
            g.drawString(s, x, height - 4);
        }
        this.text = sb.toString();
    }

    /**
     * 画干扰线
     * @param image 图形对象
     */
    private void drawLine(BufferedImage image) {
        int num = 8;
        //定义干扰线的数量
        Graphics2D g = (Graphics2D) image.getGraphics();
        for (int i = 0; i < num; i++) {
            int randomSeed = random.nextInt(2)+1;
            int x = random.nextInt(weight);
            int y = random.nextInt(height);
            int xl = random.nextInt(weight);
            int yl = random.nextInt(height);
            g.setColor(randomColor());
            g.setStroke(new BasicStroke(randomSeed));
            g.drawLine(x, y, x + xl, y + yl);
        }
    }

    /**
     * 画干扰圆
     * @param image 图形对象
     */
    private void drawOval(BufferedImage image){
        int num = 10;
        //定义干扰圆的数量
        Graphics2D g = (Graphics2D) image.getGraphics();
        for (int i = 0; i < num; i++){
            int randomSeed = random.nextInt(3)+1;
            int x = random.nextInt(weight);
            int y = random.nextInt(height);
            int z = random.nextInt(6);
            g.setColor(randomColor());
            g.setStroke(new BasicStroke(randomSeed));
            g.drawOval(x, y, z, z);
        }
    }

    /**
     * 获取验证码图片
     * @return 返回验证码的图形对象
     */
    public BufferedImage getImage() {
        BufferedImage image = createImage();
        drawText(image);
        drawLine(image);
        drawOval(image);
        return image;
    }

    /**
     * 获取验证码文本
     * @return 返回验证码字符串
     */
    public String getText() {
        return text;
    }
}
