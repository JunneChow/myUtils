package com.junne.myutils.utils;

import com.junne.myutils.common.ConstantsCfg;
import com.junne.myutils.dto.SysUserDTO;
import com.junne.myutils.entity.SysPermission;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Set;

/**
 * 描述:
 * 自定义的web工具
 *
 * @author JUNNE
 * @create 2019-09-03 9:58
 */
public class WebUtil {

    /**
     * 获取HttpServletRequest的sessionId
     * @return 返回sessionId
     */
    public static String getCurrentSessionId(){
        HttpServletRequest httpServletRequest = WebUtil.getHttpServletRequest();
        if (httpServletRequest != null){
            return httpServletRequest.getSession().getId();
        }
        return null;
    }

    /**
     * 获取当前登录用户DTO
     * @return 返回当前登录用户DTO
     */
    public static SysUserDTO getCurrentLogonUser() {
        HttpServletRequest httpServletRequest = WebUtil.getHttpServletRequest();
        if (httpServletRequest != null){
            return (SysUserDTO) httpServletRequest.getSession().getAttribute(ConstantsCfg.CURRENT_LOGON_USER);
        }
        return null;
    }

    /**
     * 设置当前登录用户
     * @param sysUserDTO 当前登录用户VO对象
     */
    public static void setCurrentLogonUser(SysUserDTO sysUserDTO){
        HttpServletRequest httpServletRequest = WebUtil.getHttpServletRequest();
        if (httpServletRequest != null){
            httpServletRequest.getSession().setAttribute(ConstantsCfg.CURRENT_LOGON_USER,sysUserDTO);
        }
    }

    /**
     * 设置当前用户的权限信息
     * @param sysPermissionSet 权限信息集合
     */
    public static void setPermissionSet(Set<SysPermission> sysPermissionSet){
        HttpServletRequest httpServletRequest = WebUtil.getHttpServletRequest();
        if (httpServletRequest != null){
            httpServletRequest.getSession().setAttribute(ConstantsCfg.CURRENT_LOGON_USER_PERMISSION_LIST,sysPermissionSet);
        }
    }

    /**
     * 获取HttpServletRequest
     * @return 返回HttpServletRequest对象
     */
    public static HttpServletRequest getHttpServletRequest(){
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (servletRequestAttributes != null){
            return servletRequestAttributes.getRequest();
        }
        return null;
    }

    /**
     * 获取HttpServletResponse
     * @return 返回HttpServletResponse对象
     */
    public static HttpServletResponse getHttpServletResponse(){
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (servletRequestAttributes != null){
            return servletRequestAttributes.getResponse();
        }
        return null;
    }

    /**
     * 编写返回输出流数据
     * @param response HttpServletResponse
     * @param status http状态码
     * @param json json对象
     */
    public static void writeResponse(HttpServletResponse response, int status, String json) {
        try {
            response.setHeader("Access-Control-Allow-Origin", "*");
            response.setHeader("Access-Control-Allow-Methods", "*");
            response.setContentType("application/json;charset=UTF-8");
            response.setStatus(status);
            response.getWriter().write(json);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取客户端IP
     * @return 返回客户端IP
     */
    public static String getIpAddress(){
        HttpServletRequest httpServletRequest = WebUtil.getHttpServletRequest();
        if (httpServletRequest != null){
            String ipAddress;
            ipAddress = httpServletRequest.getHeader("x-forwarded-for");
            if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
                ipAddress = httpServletRequest.getHeader("Proxy-Client-IP");
            }
            if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
                ipAddress = httpServletRequest.getHeader("WL-Proxy-Client-IP");
            }
            if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
                ipAddress = httpServletRequest.getRemoteAddr();
                //这里主要是获取本机的ip
                if (ipAddress.equals("127.0.0.1") || ipAddress.endsWith("0:0:0:0:0:0:1")) {
                    ipAddress = "127.0.0.1";
                    return ipAddress;
                }
            }
            // 对于通过多个代理的情况，第一个IP为客户端真实IP,多个IP按照','分割
            if (ipAddress.length() > 15) { // "***.***.***.***".length()=15
                if (ipAddress.indexOf(",") > 0) {
                    ipAddress = ipAddress.substring(0, ipAddress.indexOf(","));
                }
            }
            return ipAddress;
        }
        return null;
    }
}
