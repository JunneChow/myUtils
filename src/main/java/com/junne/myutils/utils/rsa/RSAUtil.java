package com.junne.myutils.utils.rsa;

import javax.crypto.Cipher;
import javax.servlet.http.HttpServletRequest;
import java.security.*;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 描述:
 * RAS非对称加密
 *
 * @author JUNNE
 * @create 2019-01-10 9:02
 */
public class RSAUtil {
    private static final String ENCRYPTION_ALGORITHM = "RSA";
    private static final String SIGNATURE_ALGORITHM = "MD5withRSA";

    private static final String PUBLIC_KEY = "PublicKey";
    private static final String PRIVATE_KEY = "PrivateKey";
    private static final String KEY_FACTORY = "keyFactory";
    private static final String KEY = "key";


    /**
     * Base64加密
     */
    public static String base64Encode(byte[] key) {
        Base64.Encoder encoder = Base64.getEncoder();
        return encoder.encodeToString(key);
    }

    /**
     * Base64解密
     */
    public static byte[] base64Decode(String key) {
        Base64.Decoder decoder = Base64.getDecoder();
        return decoder.decode(key);
    }

    /**
     * 生成密钥
     */
    public static Map<String, Object> initKey() throws Exception {
        /* 初始化密钥生成器 */
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance(ENCRYPTION_ALGORITHM);
        keyPairGenerator.initialize(1024);

        /* 生成密钥 */
        KeyPair keyPair = keyPairGenerator.generateKeyPair();
        RSAPublicKey publicKey = (RSAPublicKey) keyPair.getPublic();
        RSAPrivateKey privateKey = (RSAPrivateKey) keyPair.getPrivate();

        Map<String, Object> keyMap = new HashMap<String, Object>(2);
        keyMap.put(PUBLIC_KEY, publicKey);
        keyMap.put(PRIVATE_KEY, privateKey);
        return keyMap;
    }

    /**
     * 取得公钥
     */
    public static String getPublicKey(Map<String, Object> keyMap) {
        Key key = (Key) keyMap.get(PUBLIC_KEY);
        return RSAUtil.base64Encode(key.getEncoded());
    }

    /**
     * 取得私钥
     */
    public static String getPrivateKey(Map<String, Object> keyMap) {
        Key key = (Key) keyMap.get(PRIVATE_KEY);
        return RSAUtil.base64Encode(key.getEncoded());
    }

    /**
     * 加密
     */
    public static byte[] encrypt(byte[] data, String keyString, boolean isPublic) throws Exception {
        Map<String, Object> keyAndFactoryMap = RSAUtil.generateKeyAndFactory(keyString, isPublic);
        KeyFactory keyFactory = RSAUtil.getKeyFactory(keyAndFactoryMap);
        Key key = RSAUtil.getKey(keyAndFactoryMap);

        // 对数据加密
        Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
        cipher.init(Cipher.ENCRYPT_MODE, key);

        return cipher.doFinal(data);
    }

    /**
     * 解密
     */
    public static byte[] decrypt(byte[] data, String keyString, boolean isPublic) throws Exception {
        Map<String, Object> keyAndFactoryMap = RSAUtil.generateKeyAndFactory(keyString, isPublic);
        KeyFactory keyFactory = RSAUtil.getKeyFactory(keyAndFactoryMap);
        Key key = RSAUtil.getKey(keyAndFactoryMap);

        // 对数据加密
        Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
        cipher.init(Cipher.DECRYPT_MODE, key);

        return cipher.doFinal(data);
    }

    /**
     * 生成钥匙
     */
    public static Map<String, Object> generateKeyAndFactory(String keyString, boolean isPublic) throws Exception {
        byte[] keyBytes = RSAUtil.base64Decode(keyString);

        KeyFactory keyFactory = KeyFactory.getInstance(ENCRYPTION_ALGORITHM);
        Key key;
        if (isPublic) {
            X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(keyBytes);
            key = keyFactory.generatePublic(x509KeySpec);
        } else {
            PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(keyBytes);
            key = keyFactory.generatePrivate(pkcs8KeySpec);
        }

        Map<String, Object> keyAndFactoryMap = new HashMap<String, Object>(2);
        keyAndFactoryMap.put(KEY, key);
        keyAndFactoryMap.put(KEY_FACTORY, keyFactory);

        return keyAndFactoryMap;
    }

    /**
     * 从指定对象中获取钥匙
     */
    public static Key getKey(Map<String, Object> map) throws Exception {
        if (map.get(KEY) == null) {
            throw new Exception("获取密钥失败！");
        }
        return (Key)map.get(KEY);
    }

    /**
     * 从指定对象中获取钥匙工厂
     */
    private static KeyFactory getKeyFactory(Map<String, Object> map) throws Exception {
        if (map.get(KEY_FACTORY) == null) {
            throw new Exception("获取密钥工厂失败！");
        }
        return (KeyFactory)map.get(KEY_FACTORY);
    }

    /**
     * 对信息生成数字签名（用私钥）
     */
    public static String sign(byte[] data, String keyString) throws Exception {
        Map<String, Object> keyAndFactoryMap = RSAUtil.generateKeyAndFactory(keyString, false);
        Key key = RSAUtil.getKey(keyAndFactoryMap);

        PrivateKey privateKey = (PrivateKey)key;

        // 用私钥对信息生成数字签名
        Signature signature = Signature.getInstance(SIGNATURE_ALGORITHM);
        signature.initSign(privateKey);
        signature.update(data);

        return RSAUtil.base64Encode(signature.sign());
    }

    /**
     * 校验数字签名（用公钥）
     */
    public static boolean verify(byte[] data, String keyString, String sign)
            throws Exception {
        Map<String, Object> keyAndFactoryMap = RSAUtil.generateKeyAndFactory(keyString, true);
        Key key = RSAUtil.getKey(keyAndFactoryMap);

        PublicKey publicKey = (PublicKey)key;

        // 取公钥匙对象
        Signature signature = Signature.getInstance(SIGNATURE_ALGORITHM);
        signature.initVerify(publicKey);
        signature.update(data);

        // 验证签名是否正常
        return signature.verify(RSAUtil.base64Decode(sign));
    }

    /**
     * 返回用私钥解密的数据
     * @param httpServletRequest http请求
     * @param encryptData 加密后的数据
     * @return 返回解密后数据
     */
    public static String getDecryptData(HttpServletRequest httpServletRequest, String encryptData, boolean isPublic){
        String decryptData="";
        String key = "";
        if (isPublic){
            key = (String) httpServletRequest.getSession().getAttribute("publicKey");
        }else {
            key = (String) httpServletRequest.getSession().getAttribute("privateKey");
        }
        try {
            byte[] bytes = RSAUtil.base64Decode(encryptData);
            byte[] decrypt = RSAUtil.decrypt(bytes, key, false);
            decryptData = new String(decrypt);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return decryptData;
    }

    /**
     * 解密密码
     * @param privateKey 私钥
     * @param password 加密的密码
     * @return 返回解密后的密码
     */
    public static String decryptPassword(String privateKey,String password){
        if (password != null && !"".equals(password.trim())){
            //去掉空格换行
            String regex = "\\s*|\t|\r|\n";
            Pattern compile = Pattern.compile(regex);
            Matcher matcher = compile.matcher(password);
            password = matcher.replaceAll("");
        }
        if (privateKey != null && !"".equals(privateKey)){
            try {
                byte[] passwordBytes = RSAUtil.base64Decode(password);
                byte[] decrypt = RSAUtil.decrypt(passwordBytes, privateKey, false);
                password = new String(decrypt);
                return password;
            } catch (Exception e) {
                password = "密文转换出错！";
                return password;
            }
        }else {
            password = "密文转换出错！";
            return password;
        }
    }
}
