package com.junne.myutils.utils.aes;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

/**
 * 描述:
 * AES加密解密工具
 *
 * @author JUNNE
 * @create 2019-10-08 15:18
 */
public class AesUtil {
    /**
     * 算法
     */
    private static final String ENCRYPTION_ALGORITHM = "AES/ECB/PKCS5Padding";
    private static final String AES_KEY = "123456789abcdefg";

    /**
     * AES加密为16进制编码
     * @param content 待加密的内容
     * @param encryptKey 加密密钥
     * @return 加密后的16进制编码
     */
    public static String encryptHex(String content, String encryptKey){
        String aesEncryptStr = "";
        try {
            aesEncryptStr = AesUtil.hex(aesEncryptToBytes(content, encryptKey));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return aesEncryptStr;
    }

    /**
     * 将16进制编码AES解密
     * @param encryptStr 待解密的16进制编码
     * @param decryptKey 解密密钥
     * @return 解密后的string
     */
    public static String decryptHex(String encryptStr, String decryptKey){
        String aesDecryptStr = "";
        try {
            if (encryptStr == null || "".equals(encryptStr)){
                return null;
            }
            aesDecryptStr = aesDecryptByBytes(AesUtil.unHex(encryptStr), decryptKey);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return aesDecryptStr;
    }

    /**
     * AES加密为base 64 code
     * @param content 待加密的内容
     * @param encryptKey 加密密钥
     * @return 加密后的base 64 code
     */
    public static String encryptBase64(String content, String encryptKey){
        String aesEncryptStr = "";
        try {
            aesEncryptStr = AesUtil.base64Encode(aesEncryptToBytes(content, encryptKey));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return aesEncryptStr;
    }

    /**
     * 将base 64 code AES解密
     * @param encryptStr 待解密的base 64 code
     * @param decryptKey 解密密钥
     * @return 解密后的string
     */
    public static String decryptBase64(String encryptStr, String decryptKey){
        String aesDecryptStr = "";
        try {
            if (encryptStr == null || "".equals(encryptStr)){
                return null;
            }
            aesDecryptStr = aesDecryptByBytes(AesUtil.base64Decode(encryptStr), decryptKey);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return aesDecryptStr;
    }

    /**
     * Base64加密
     */
    private static String base64Encode(byte[] key) {
        java.util.Base64.Encoder encoder = java.util.Base64.getEncoder();
        return encoder.encodeToString(key);
    }

    /**
     * Base64解密
     */
    private static byte[] base64Decode(String key) {
        java.util.Base64.Decoder decoder = java.util.Base64.getDecoder();
        return decoder.decode(key);
    }

    /**
     * 数组转16进制字符串
     * @param bytes 字节数组
     * @return 返回16进制的字符串
     */
    private static String hex(byte[] bytes){
        StringBuilder stringBuffer = new StringBuilder(bytes.length);
        String tempStr;
        for (byte aByte : bytes) {
            tempStr = Integer.toHexString(0xFF & aByte);
            if (tempStr.length() < 2) {
                stringBuffer.append(0);
            }
            stringBuffer.append(tempStr.toUpperCase());
        }
        return stringBuffer.toString();
    }

    /**
     * 16进制转数组
     * @param hexStr 16进制的字符串
     * @return 返回数组
     */
    private static byte[] unHex(String hexStr) {
        if (hexStr.length() < 1){
            return null;
        }
        byte[] result = new byte[hexStr.length() / 2];
        for (int i = 0; i < hexStr.length() / 2; i++) {
            int high = Integer.parseInt(hexStr.substring(i * 2, i * 2 + 1), 16);
            int low = Integer.parseInt(hexStr.substring(i * 2 + 1, i * 2 + 2), 16);
            result[i] = (byte) (high * 16 + low);
        }
        return result;
    }

    /**
     * 获取SecretKey
     * @param encryptKey 加密密钥字符串，密钥是16位时基本通用
     * @return 返回SecretKey
     */
    private static SecretKey getSecretKey(String encryptKey){
        if(encryptKey == null){
            encryptKey = AES_KEY;
        }
        SecretKey secretKey = null;
        try {
            if(encryptKey.length() != 16){
                SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
                random.setSeed(encryptKey.getBytes());
                //1.构造密钥生成器，指定为AES算法,不区分大小写
                KeyGenerator keygen=KeyGenerator.getInstance("AES");
                //2.根据encryptKey初始化密钥生成器
                //生成一个128位的随机源,根据传入的字节数组
                keygen.init(128, random);
                //3.产生原始对称密钥
                SecretKey original_key=keygen.generateKey();
                //4.获得原始对称密钥的字节数组
                byte [] raw=original_key.getEncoded();
                //5.根据字节数组生成AES密钥
                secretKey = new SecretKeySpec(raw, "AES");
            }else {
                secretKey = new SecretKeySpec(encryptKey.getBytes(), "AES");
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return secretKey;
    }


    /**
     * AES加密
     * @param content 待加密的内容
     * @param encryptKey 加密密钥
     * @return 加密后的byte[]
     * @throws Exception
     */
    private static byte[] aesEncryptToBytes(String content, String encryptKey) throws Exception {
        KeyGenerator kgen = KeyGenerator.getInstance("AES");
        kgen.init(128);
        Cipher cipher = Cipher.getInstance(ENCRYPTION_ALGORITHM);
        cipher.init(Cipher.ENCRYPT_MODE, getSecretKey(encryptKey));

        return cipher.doFinal(content.getBytes("utf-8"));
    }

    /**
     * AES解密
     * @param encryptBytes 待解密的byte[]
     * @param decryptKey 解密密钥
     * @return 解密后的String
     * @throws Exception
     */
    private static String aesDecryptByBytes(byte[] encryptBytes, String decryptKey) throws Exception {
        KeyGenerator kgen = KeyGenerator.getInstance("AES");
        kgen.init(128);

        Cipher cipher = Cipher.getInstance(ENCRYPTION_ALGORITHM);
        cipher.init(Cipher.DECRYPT_MODE, getSecretKey(decryptKey));
        byte[] decryptBytes = cipher.doFinal(encryptBytes);
        return new String(decryptBytes);
    }
}
