package com.junne.myutils.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * 描述:
 * MD5加密解密工具
 *
 * @author JUNNE
 * @create 2020-03-16 11:02
 */
public class Md5Util {

    private static MessageDigest getDigest() {
        MessageDigest messageDigest = null;
        try {
            messageDigest = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return messageDigest;
    }

    public static String encryptFile(File file) throws IOException {
        FileInputStream fis = null;
        MessageDigest md = Md5Util.getDigest();
        fis = new FileInputStream(file);
        byte[] buffer = new byte[1024];
        int i;
        while ((i=fis.read(buffer))!=-1){
            md.update(buffer,0,i);
        }
        fis.close();
        byte[] digest = md.digest();
        return Md5Util.hex(digest);
    }

    /**
     * 多次加盐加密
     * @param credentials 加密的内容
     * @param salt 盐值
     * @param hashIterations 加密次数
     * @return 返回MD5加密的字符串
     */
    public static String encrypt(String credentials, String salt, int hashIterations){
        MessageDigest digest = Md5Util.getDigest();
        if (salt != null) {
            digest.reset();
            digest.update(salt.getBytes());
        }
        byte[] hashed = digest.digest(credentials.getBytes());
        int iterations = hashIterations - 1;

        for(int i = 0; i < iterations; ++i) {
            digest.reset();
            hashed = digest.digest(hashed);
        }
        return Md5Util.hex(hashed);
    }

    /**
     * 数组转16进制字符串
     * @param bytes 字节数组
     * @return 返回16进制的字符串
     */
    public static String hex(byte[] bytes){
        StringBuilder stringBuffer = new StringBuilder(bytes.length);
        String tempStr;
        for (byte aByte : bytes) {
            tempStr = Integer.toHexString(0xFF & aByte);
            if (tempStr.length() < 2) {
                stringBuffer.append(0);
            }
            stringBuffer.append(tempStr.toUpperCase());
        }
        return stringBuffer.toString();
    }

    /**
     * 16进制转数组
     * @param hexStr 16进制的字符串
     * @return 返回数组
     */
    public static byte[] unHex(String hexStr) {
        if (hexStr.length() < 1){
            return null;
        }
        byte[] result = new byte[hexStr.length() / 2];
        for (int i = 0; i < hexStr.length() / 2; i++) {
            int high = Integer.parseInt(hexStr.substring(i * 2, i * 2 + 1), 16);
            int low = Integer.parseInt(hexStr.substring(i * 2 + 1, i * 2 + 2), 16);
            result[i] = (byte) (high * 16 + low);
        }
        return result;
    }

    public static void main(String[] args) {
        String s = Md5Util.encrypt("123456@qwe", "ED49C3FED75A513A79CB8BD1D4715D57", 3);
        System.out.println(s);
    }
}
