package com.junne.myutils.utils;

/**
 * 描述:
 * 自定义的字符串工具类
 *
 * @author JUNNE
 * @create 2020/9/22 10:07
 */
public class StringUtil {

    /**
     * Levenshtein 距离算法
     * @param s1 字符串1
     * @param s2 字符串2
     * @return 返回比较的相似度
     */
    public static int calculateStringDistance(String s1, String s2) {
        if (s1.length() == 0) {
            //字符串1为空直接返回字符串2的长度
            return s2.length();
        } else if (s2.length() == 0) {
            //字符串2为空直接返回字符串1的长度
            return s1.length();
        } else {
            //初始化一个长度为两字符串长度分别加1的矩阵
            int[][] d = new int[s1.length() + 1][s2.length() + 1];

            int i;
            for(i = 0; i <= s1.length(); d[i][0] = i++) {
            }

            for(i = 0; i <= s2.length(); d[0][i] = i++) {
            }

            for(i = 1; i <= s1.length(); ++i) {
                char s_i = s1.charAt(i - 1);

                for(int j = 1; j <= s2.length(); ++j) {
                    char t_j = s2.charAt(j - 1);
                    byte cost;
                    if (s_i == t_j) {
                        cost = 0;
                    } else {
                        cost = 1;
                    }

                    d[i][j] = Math.min(Math.min(d[i - 1][j] + 1, d[i][j - 1] + 1), d[i - 1][j - 1] + cost);
                }
            }

            return d[s1.length()][s2.length()];
        }
    }

    /**
     * 比较相似度
     * @param s1 字符串1
     * @param s2 字符串2
     * @return 返回相似度
     */
    public static double SimilarityPercentage(String s1,String s2){
        double rs = 0;
        int distance = StringUtil.calculateStringDistance(s1, s2);
        if (s1.length() == 0 ||s2.length() == 0){
            rs = 0;
        }else {
            int max = Math.max(s1.length(), s2.length());
            rs = 1 - (double) distance / max;
        }
        //乘100得到百分比
        return rs*100;
    }

    public static void main(String[] args) {
        String s1="junne";
        String s2="janne";
        int i = StringUtil.calculateStringDistance(s1, s2);
        double v = StringUtil.SimilarityPercentage(s1, s2);
        System.out.println("距离为："+i);
        System.out.println("相似度为："+v);
    }
}
