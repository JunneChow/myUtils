package com.junne.myutils.utils;

import com.junne.myutils.qdutils.TimeUtil;
import com.junne.myutils.qdutils.exception.WarningException;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;

/**
 * 描述:
 * excel工具
 *
 * @author JUNNE
 * @create 2019-09-12 16:16
 */
public class ExcelUtil {

    /**
     * 导出xls格式的excel
     * @param fileName excel文件名称（无需后缀）
     * @param titleList 标题列表
     * @param dataList 数据列表
     * @param httpServletResponse httpServletResponse
     * @return 返回xls格式的excel
     * @throws WarningException 抛出异常
     */
    public static boolean exportHSSFWorkbookExcel(String fileName,List<Object> titleList,List<List<Object>> dataList,
                                                  HttpServletResponse httpServletResponse) throws WarningException {
        Map<Integer,Integer> tempMap = new HashMap<>();
        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet;
        //创建一个Excel表单,参数为sheet的名字
        sheet = workbook.createSheet(fileName);
        ExcelUtil.setTitle(sheet,titleList,tempMap);
        if (dataList != null && dataList.size()>0){
            for (List<Object> contentList:dataList) {
                ExcelUtil.setContent(sheet,contentList,tempMap);
            }
        }
        fileName = fileName+".xls";
        return ExcelUtil.getExcelFromResponse(workbook, httpServletResponse, fileName);
    }

    /**
     * 从httpServletResponse中get请求下载excel
     * @param workbook HSSFWorkbook对象
     * @param httpServletResponse HttpServletResponse请求对象
     * @param fileName 文件名称
     * @throws WarningException 抛出异常
     */
    public static boolean getExcelFromResponse(Workbook workbook, HttpServletResponse httpServletResponse,
                                               String fileName) throws WarningException {
        //清空response
        httpServletResponse.reset();
        //设置response的Header
        try {
            httpServletResponse.addHeader("Content-Disposition", "attachment;filename="
                    + java.net.URLEncoder.encode(fileName, "UTF-8"));
            OutputStream os = null;
            os = new BufferedOutputStream(httpServletResponse.getOutputStream());
            httpServletResponse.setContentType("application/vnd.ms-excel;charset=UTF-8");
            //将excel写入到输出流中
            workbook.write(os);
            os.flush();
            os.close();
        } catch (IOException e) {
            throw new WarningException("导出Excel服务异常！");
        }
        return true;
    }

    /**
     * 写入数据到excel文件
     * @param file excel文件
     * @param titleList 标题内容
     * @param dataList 数据内容
     * @return 返回是否成功
     */
    public static boolean writeExcelFile(File file, List<Object> titleList, List<List<Object>> dataList){
        Map<Integer,Integer> tempMap = new HashMap<>();
        Workbook workbook;
        Sheet sheet;
        if (file.getName().endsWith(".xls")){
            workbook = new HSSFWorkbook();
        }else if (file.getName().endsWith(".xlsx")){
            workbook = new XSSFWorkbook();
        }else {
            return false;
        }
        //创建一个Excel表单,参数为sheet的名字
        sheet = workbook.createSheet(file.getName());
        try {
            ExcelUtil.setTitle(sheet,titleList,tempMap);
            if (dataList != null && dataList.size()>0){
                for (List<Object> contentList:dataList) {
                    ExcelUtil.setContent(sheet,contentList,tempMap);
                }
            }
            OutputStream fos = new FileOutputStream(file);
            workbook.write(fos);
            fos.flush();
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * 设置标题
     * @param sheet 表单对象
     * @param list 内容列表
     * @throws WarningException 抛出异常
     */
    public static void setTitle(Sheet sheet, List<Object> list, Map<Integer,Integer> tempMap) throws WarningException {
        Row row = sheet.createRow(sheet.getLastRowNum());
        ExcelUtil.setData(row,list,tempMap);
    }

    /**
     * 设置内容
     * @param sheet 表单对象
     * @param list 内容列表
     * @throws WarningException 抛出异常
     */
    public static void setContent(Sheet sheet, List<Object> list, Map<Integer,Integer> tempMap) throws WarningException {
        Row row = sheet.createRow((sheet.getLastRowNum()+1));
        ExcelUtil.setData(row,list,tempMap);
    }

    /**
     * 设置数据
     * @param row HSSFRow
     * @param list List<Object>
     * @param tempMap Map<Integer,Integer>
     * @throws WarningException 抛出异常
     */
    private static void setData(Row row, List<Object> list, Map<Integer,Integer> tempMap) throws WarningException {
        int columnIndex = -1;
        for (Object object:list) {
            if (object != null){
                ExcelUtil.setAndAutoSizeColumn(row,++columnIndex,object,tempMap);
            }else {
                ExcelUtil.setAndAutoSizeColumn(row,++columnIndex,"",tempMap);
            }
        }
    }

    /**
     * 设值并调整列宽
     * @param row 行
     * @param columnIndex 列数
     * @param cellValue 表格内容
     */
    private static void setAndAutoSizeColumn(Row row, int columnIndex, Object cellValue, Map<Integer,Integer> tempMap) {
        int unitWidth = 256;
        if (cellValue instanceof Boolean){
            row.createCell(columnIndex).setCellValue((Boolean) cellValue);
        }else if (cellValue instanceof Double){
            row.createCell(columnIndex).setCellValue((Double) cellValue);
        }else if (cellValue instanceof String){
            row.createCell(columnIndex).setCellValue((String) cellValue);
        }else if (cellValue instanceof Calendar){
            row.createCell(columnIndex).setCellValue((Calendar) cellValue);
        }else if (cellValue instanceof Date){
            String cellValueStr = TimeUtil.dateToString("yyyy-MM-dd HH:mm:ss", (Date) cellValue);
            row.createCell(columnIndex).setCellValue(cellValueStr);
        }
        Integer currentColumnWidth = tempMap.get(columnIndex);
        int columnWidth = 0;
        columnWidth = row.getCell(columnIndex).toString().getBytes(StandardCharsets.UTF_8).length;
        if (currentColumnWidth != null)
        {
            if (columnWidth*unitWidth>currentColumnWidth){
                if(columnWidth*unitWidth>65280){
                    row.getSheet().setColumnWidth(columnIndex,65280);
                }else {
                    row.getSheet().setColumnWidth(columnIndex,columnWidth*unitWidth);
                }
                tempMap.put(columnIndex,columnWidth*unitWidth);
            }
        }else{
            row.getSheet().setColumnWidth(columnIndex,columnWidth*unitWidth);
            tempMap.put(columnIndex,columnWidth*unitWidth);
        }
    }

    /**
     * 从excel文件输入流获取数据
     * @param isXLS 是否是xls文件，否则是xlsx文件
     * @param inputStream excel文件输入流
     * @param skipRowNum 跳过的行数（从1开始，比如跳过第一行则skipRowNum=1）
     * @return 返回excel的数据
     * @throws Exception 抛出异常
     */
    public static Map<String,List<List<Object>>> getAllExcelData(boolean isXLS,InputStream inputStream,int skipRowNum) throws Exception {
        Workbook wb;
        try{
            if (isXLS){
                wb = new HSSFWorkbook(inputStream);
            }else {
                wb = new XSSFWorkbook(inputStream);
            }
            int sheetNum = wb.getNumberOfSheets();
            Map<String,List<List<Object>>> map = new HashMap<>();
            for (int i=0;i<sheetNum;i++){
                Sheet sheet = wb.getSheetAt(i);
                if (sheet == null){
                    continue;
                }
                String sheetName = sheet.getSheetName();
                List<List<Object>> dataFromSheet = ExcelUtil.getDataFromSheet(sheet, skipRowNum);
                map.put(sheetName,dataFromSheet);
            }
            return map;
        }catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 从excel文件输入流获取数据
     * @param isXLS 是否是xls文件，否则是xlsx文件
     * @param inputStream excel文件输入流
     * @param sheetNum excel表单页，从0开始
     * @param skipRowNum 跳过的行数（从1开始，比如跳过第一行则skipRowNum=1）
     * @return 返回excel的数据
     * @throws WarningException 抛出异常
     */
    public static List<List<Object>> getExcelData(boolean isXLS,InputStream inputStream,int sheetNum,int skipRowNum) throws WarningException {
        Workbook wb;
        try{
            if (isXLS){
                wb = new HSSFWorkbook(inputStream);
            }else {
                wb = new XSSFWorkbook(inputStream);
            }
            Sheet sheet = wb.getSheetAt(sheetNum);
            if (sheet == null){
                throw new WarningException("excel文件不存在sheet表！");
            }
            return ExcelUtil.getDataFromSheet(sheet,skipRowNum);
        }catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取sheet表的数据
     * @param sheet excel的sheet对象
     * @param skipRowNum 跳过的行数（从1开始，比如跳过第一行则skipRowNum=1）
     * @return 返回sheet表的数据
     * @throws WarningException 抛出异常
     */
    public static List<List<Object>> getDataFromSheet(Sheet sheet, int skipRowNum) throws WarningException {
        List<List<Object>> sheetDataList = new ArrayList<>();
        for (int i=skipRowNum;i<sheet.getLastRowNum()+1;i++){
            Row row = sheet.getRow(i);
            if (row == null){
                continue;
            }
            List<Object> rowDataList = new ArrayList<>();
            for (int j=0;j<row.getLastCellNum();j++){
                if (row.getCell(j) != null){
                    //不同版本可能需要切换，比如3.9的用Cell，而4.1.1用CellType
                    switch (row.getCell(j).getCellType()){
                        case Cell.CELL_TYPE_NUMERIC:
                            //数值型
                            rowDataList.add(row.getCell(j).getNumericCellValue());
                            break;
                        case Cell.CELL_TYPE_STRING:
                            //字符串型
                            rowDataList.add(row.getCell(j).getStringCellValue());
                            break;
                        case Cell.CELL_TYPE_FORMULA:
                            //公式型
                            rowDataList.add(row.getCell(j).getCellFormula());
                            break;
                        case Cell.CELL_TYPE_BLANK:
                            //null值
                            rowDataList.add(null);
                            break;
                        case Cell.CELL_TYPE_BOOLEAN:
                            //布尔型
                            rowDataList.add(row.getCell(j).getBooleanCellValue());
                            break;
                        case Cell.CELL_TYPE_ERROR:
                            //错误
                            throw new WarningException("读取"+(i+1)+"行"+(j+1)+"列"+"的excel数据异常！");
                        default:
                            //默认null
                            rowDataList.add(null);
                            break;
                    }
                }else {
                    rowDataList.add(null);
                }
            }
            //删除null元素
            rowDataList.removeAll(Collections.singleton(null));
            if (rowDataList.size()>0){
                sheetDataList.add(rowDataList);
            }
        }
        return sheetDataList;
    }
}
