package com.junne.myutils.schedule;

import com.junne.myutils.qdutils.TimeUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 描述:
 * 系统内置定时任务
 *
 * @author JUNNE
 * @create 2019-09-09 10:19
 */
@Component
public class ScheduleTask {
    private Logger log = LoggerFactory.getLogger(ScheduleTask.class);

    //每5分钟执行一次
    @Scheduled(cron = "0 0/30 * * * ?")
    public void TestSchedule(){
        int day = 60;
        String daysAgoStr = TimeUtil.getDaysAgoStr("yyyy-MM-dd HH:mm:ss", day);
        log.info("测试定时任务！"+day+"天前的时间字符串："+daysAgoStr);
    }
}
