package com.junne.myutils.qdutils;

import com.junne.myutils.qdutils.exception.WarningException;

import java.io.*;
import java.util.Enumeration;
import java.util.zip.*;

/**
 * 描述:
 * gzip工具类
 *
 * @author JUNNE
 * @create 2020-01-02 11:05
 */
public class ZipUtil {

    /**
     * zip压缩文件（递归压缩，包括空文件夹）
     * @param sourceFilePath 待压缩的文件路径（具体文件或者文件夹）
     * @param targetFilePath 生成的目标文件路径（以“.zip”结尾，不加时默认是“.zip”作为后缀）
     * @throws WarningException 抛出异常
     * @throws IOException 抛出异常
     */
    public static void zipFile(String sourceFilePath, String targetFilePath) throws WarningException, IOException {
        String zipSuffix = ".zip";
        File sourceFile = new File(sourceFilePath);
        if (!sourceFile.exists()){
            throw new WarningException("待压缩的目标文件或文件夹不存在！");
        }
        sourceFilePath = sourceFile.getAbsolutePath();
        if (!targetFilePath.endsWith(zipSuffix)){
            targetFilePath = targetFilePath+zipSuffix;
        }
        File targetFile = new File(targetFilePath);
        ZipOutputStream zipOutputStream;
        if (!targetFile.exists() || targetFile.delete()){
            targetFile.getParentFile().mkdirs();
            targetFile.createNewFile();
        }
        zipOutputStream = new ZipOutputStream(new FileOutputStream(targetFile));
        String baseDir = sourceFilePath.substring(sourceFilePath.lastIndexOf(File.separator));
        ZipUtil.zipCompress(sourceFile,baseDir,zipOutputStream);
        zipOutputStream.close();
    }

    /**
     * zip解压（压缩文件里面若若存在压缩文件时，不会进行解压）
     * @param zipFilePath 待解压的zip文件路径
     * @param unZipFilePath 解压后存放的文件路径
     * @throws WarningException 抛出异常
     * @throws IOException 抛出异常
     */
    public static void unZipFile(String zipFilePath,String unZipFilePath) throws WarningException, IOException {
        int buffer = 2048;
        String zipSuffix = ".zip";
        if (!zipFilePath.endsWith(zipSuffix)){
            throw new WarningException("只支持解压“.zip”后缀的压缩文件！");
        }
        ZipFile zipFile = new ZipFile(zipFilePath);
        Enumeration<? extends ZipEntry> zipEntries = zipFile.entries();
        while(zipEntries.hasMoreElements()){
            ZipEntry zipEntry = zipEntries.nextElement();
            String zipEntryName = zipEntry.getName();
            File tempFile = new File(unZipFilePath + File.separator + zipEntryName);
            if (!tempFile.exists() || tempFile.delete()){
                tempFile.getParentFile().mkdirs();
                if (tempFile.isDirectory() || zipEntryName.endsWith("/")){
                    tempFile.mkdirs();
                }else {
                    InputStream inputStream = zipFile.getInputStream(zipEntry);
                    BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
                    FileOutputStream fileOutputStream = new FileOutputStream(tempFile);
                    BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(fileOutputStream,buffer);
                    int b;
                    byte[] data = new byte[buffer];
                    while ((b=bufferedInputStream.read(data,0,buffer))>0){
                        bufferedOutputStream.write(data,0,b);
                    }
                    bufferedOutputStream.close();
                    fileOutputStream.close();
                    bufferedInputStream.close();
                    inputStream.close();
                }
            }
        }
        //关闭压缩文件
        zipFile.close();
    }

    /**
     * 递归压缩
     * @param file 被压缩的文件
     * @param baseDir 基本路径（用于递归处理）
     * @param zipOutputStream ZipOutputStream zip压缩的数据输出流
     */
    private static void zipCompress(File file,String baseDir,ZipOutputStream zipOutputStream){
        int buffer = 2048;
        try {
            if (file.isDirectory()){
                File[] files = file.listFiles();
                if (files != null && files.length > 0){
                    for (File tempFile:files) {
                        ZipUtil.zipCompress(tempFile,baseDir+File.separator+tempFile.getName()+File.separator,zipOutputStream);
                    }
                }else {
                    zipOutputStream.putNextEntry(new ZipEntry(baseDir+File.separator+"/"));
                    zipOutputStream.closeEntry();
                }
            }else {
                FileInputStream fileInputStream = new FileInputStream(file);
                baseDir = baseDir.substring(0, baseDir.length() - 1);
                baseDir = baseDir.substring(0,baseDir.lastIndexOf(File.separator));
                zipOutputStream.putNextEntry(new ZipEntry(baseDir+File.separator+file.getName()));
                int len;
                byte[] data = new byte[buffer];
                while ((len=fileInputStream.read(data))>0){
                    zipOutputStream.write(data,0,len);
                }
                zipOutputStream.closeEntry();
                fileInputStream.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * GZIP压缩
     * @param byteArr 待压缩的字节数组数据
     * @return 返回压缩后的字节数组数据
     * @throws IOException 抛出异常
     */
    public static byte[] gzipCompress(byte[] byteArr) throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        GZIPOutputStream gzip = new GZIPOutputStream(out);
        gzip.write(byteArr);
        gzip.close();
        return out.toByteArray();
    }

    /**
     * GZIP解压
     * @param byteArr 待解压的字节数组数据
     * @return 返回解压后的字节数组数据
     * @throws IOException 抛出异常
     */
    public static byte[] gzipUncompress(byte[] byteArr) throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ByteArrayInputStream in = new ByteArrayInputStream(byteArr);
        GZIPInputStream gzip = new GZIPInputStream(in);
        byte[] buffer = new byte[256];
        int n;
        while ((n = gzip.read(buffer)) >= 0) {
            out.write(buffer, 0, n);
        }
        return out.toByteArray();
    }

    public static void main(String[] args) throws WarningException, IOException {
        String sourceFilePath = "D:\\uploadImg";
        String targetFilePath = "D:\\uploadImg";
        ZipUtil.zipFile(sourceFilePath,targetFilePath);

        String zipFilePath = "D:\\uploadImg.zip";
        String unZipFilePath = "D:\\test\\zip";
        ZipUtil.unZipFile(zipFilePath,unZipFilePath);
    }
}
