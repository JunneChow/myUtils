package com.junne.myutils.qdutils;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * 描述:
 * 图片处理工具
 *
 * @author JUNNE
 * @create 2019-09-30 10:10
 */
public class ImgUtil {

    /**
     * Base64加密
     */
    public static String base64Encode(byte[] key) {
        java.util.Base64.Encoder encoder = java.util.Base64.getEncoder();
        return encoder.encodeToString(key);
    }

    /**
     * Base64解密
     */
    public static byte[] base64Decode(String key) {
        java.util.Base64.Decoder decoder = java.util.Base64.getDecoder();
        return decoder.decode(key);
    }

    /**
     * 设置图片到HttpServletResponse数据流
     * @param image HttpServletResponse对象
     * @param httpServletResponse HttpServletResponse对象
     */
    public static void setImgToResponse(BufferedImage image,HttpServletResponse httpServletResponse){
        // 禁止图像缓存。
        httpServletResponse.setHeader("Pragma", "no-cache");
        httpServletResponse.setHeader("Cache-Control", "no-cache");
        httpServletResponse.setDateHeader("Expires", 0);
        httpServletResponse.setContentType("image/jpeg");
        // 将图像输出到Servlet输出流中。
        ServletOutputStream sos = null;
        try {
            sos = httpServletResponse.getOutputStream();
            ImageIO.write(image, "jpeg", sos);
            sos.flush();
            sos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 图形对象转Png格式的base64编码
     * @param image BufferedImage对象
     * @return 返回base64编码
     */
    public static String getPngBase64Encode(BufferedImage image){
        //返回图片的base64编码
        String imgBase64Result = "";
        ByteArrayOutputStream baos = new ByteArrayOutputStream();//io流
        try {
            ImageIO.write(image, "png", baos);//写入流中
            byte[] bytes = baos.toByteArray();
            String pngBase64 = ImgUtil.base64Encode(bytes).trim();
            imgBase64Result = "data:image/png;base64,"+pngBase64;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return imgBase64Result;
    }
}
