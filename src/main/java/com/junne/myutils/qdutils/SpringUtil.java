package com.junne.myutils.qdutils;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.beans.BeansException;
import org.springframework.beans.FatalBeanException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.util.ClassUtils;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * 描述:
 * spring获取bean实例工具类
 *
 * @author JUNNE
 * @create 2019-10-11 9:39
 */
@Component
public class SpringUtil implements ApplicationContextAware {

    private static ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        SpringUtil.applicationContext = applicationContext;
    }

    /**
     * 获取上下文对象
     * @return 返回上下文对象
     */
    public static ApplicationContext getApplicationContext(){
        return SpringUtil.applicationContext;
    }

    /**
     * 根据类型获取实例
     * @param tClass 类型
     * @param <T> 泛型
     * @return 返回实例
     */
    public static <T> T getBean(Class<T> tClass){
        return applicationContext.getBean(tClass);
    }

    /**
     * 根据类型和（id或name)获取实例
     * @param idOrName bean的id或name属性
     * @param tClass 类型
     * @param <T> 泛型
     * @return 返回实例
     */
    public static <T> T getBean(String idOrName,Class<T> tClass){
        return applicationContext.getBean(idOrName,tClass);
    }

    /**
     * 根据key获取配置属性的值
     * @param key 配置属性key
     * @return 返回配置数据的值
     */
    public static String getProperty(String key){
        return applicationContext.getBean(Environment.class).getProperty(key);
    }

    /**
     * 带布尔值转换且忽略null值的属性复制(此方法逻辑主要来自Spring的BeanUtils类)
     * @param source 源对象
     * @param target 目标对象
     * @param whenTrueValue 当属性是布尔值：true时被替换的目标值
     * @param whenFalseValue 当属性是布尔值：false时被替换的目标值
     */
    public static void copyPropertiesIgnoreNullAndBooleanConvert(Object source, Object target
            ,String whenTrueValue,String whenFalseValue){
        //获取目标对象的所有属性
        PropertyDescriptor[] targetPdArr = BeanUtils.getPropertyDescriptors(target.getClass());
        //获取忽略复制的属性(目标对象值为null的属性)
        String[] ignorePropertyNameArr = SpringUtil.getNullPropertyNames(source.getClass());
        List<String> ignoreList = ignorePropertyNameArr.length>0 ? Arrays.asList(ignorePropertyNameArr) : null;
        for (PropertyDescriptor targetPd : targetPdArr) {
            Method writeMethod = targetPd.getWriteMethod();
            //如果属性有写方法并且不是忽略的属性
            if (writeMethod != null && (ignoreList == null || !ignoreList.contains(targetPd.getName()))) {
                PropertyDescriptor sourcePd = BeanUtils.getPropertyDescriptor(source.getClass(), targetPd.getName());
                if (sourcePd != null) {
                    Method readMethod = sourcePd.getReadMethod();
                    //此方法直接复制Spring的BeanUtils
                    if (readMethod != null) {
                        try {
                            if (!Modifier.isPublic(readMethod.getDeclaringClass().getModifiers())) {
                                readMethod.setAccessible(true);
                            }

                            Object value = readMethod.invoke(source);
                            if (!Modifier.isPublic(writeMethod.getDeclaringClass().getModifiers())) {
                                writeMethod.setAccessible(true);
                            }
                            //加入自定义的布尔值转换逻辑：String转boolean
                            if (value instanceof String && ClassUtils.isAssignable(writeMethod.getParameterTypes()[0], Boolean.class)) {
                                String s = String.valueOf(value);
                                if (whenTrueValue.equals(s)) {
                                    writeMethod.invoke(target, true);
                                    continue;
                                } else if (whenFalseValue.equals(s)) {
                                    writeMethod.invoke(target, false);
                                    continue;
                                }
                            }
                            //加入自定义的布尔值转换逻辑：boolean转String
                            if (value instanceof Boolean && ClassUtils.isAssignable(writeMethod.getParameterTypes()[0], String.class)) {
                                boolean b = (boolean) value;
                                if (b) {
                                    writeMethod.invoke(target, whenTrueValue);
                                    continue;
                                } else {
                                    writeMethod.invoke(target, whenFalseValue);
                                    continue;
                                }
                            }
                            //默认情况下，如果名称和类型都相同
                            if (ClassUtils.isAssignable(writeMethod.getParameterTypes()[0], readMethod.getReturnType())){
                                writeMethod.invoke(target, value);
                            }
                        } catch (Throwable var15) {
                            throw new FatalBeanException("Could not copy property '" + targetPd.getName() + "' from source to target", var15);
                        }
                    }
                }
            }
        }
    }

    /**
     * 对象复制，排除源对象值为null的字段
     * @param source 被复制的源对象
     * @param target 复制后的目标对象
     */
    public static void copyPropertiesIgnoreNull(Object source, Object target){
        if (source != null && target != null){
            BeanUtils.copyProperties(source,target,SpringUtil.getNullPropertyNames(source));
        }
    }

    /**
     * 获取对象字段值为null的字段名称数组
     * @param object 对象
     * @return 返回字段值为null的字段名称数组
     */
    public static String[]  getNullPropertyNames(Object object){
        if (object == null){
            return null;
        }
        BeanWrapperImpl beanWrapper = new BeanWrapperImpl(object);
        PropertyDescriptor[] propertyDescriptors = beanWrapper.getPropertyDescriptors();
        Set<String> emptyNames = new HashSet<>();
        for (PropertyDescriptor propertyDescriptor:propertyDescriptors) {
            Object propertyValue = beanWrapper.getPropertyValue(propertyDescriptor.getName());
            if (propertyValue == null){
                emptyNames.add(propertyDescriptor.getName());
            }
        }
        String[] strings = new String[emptyNames.size()];
        return emptyNames.toArray(strings);
    }
}
