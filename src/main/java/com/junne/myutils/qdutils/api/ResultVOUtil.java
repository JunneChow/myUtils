package com.junne.myutils.qdutils.api;

import com.junne.myutils.qdutils.api.enums.ResultEnum;

/**
 * 描述:
 * ResultVO工具类
 *
 * @author JUNNE
 * @create 2019-09-06 16:11
 */
public class ResultVOUtil {

    public static ResultVO success(){
        return ResultVOUtil.success(null);
    }

    public static ResultVO success(Object object){
        return ResultVOUtil.success(ResultEnum.SUCCESS.getMsg(),object);
    }

    @SuppressWarnings("unchecked")
    public static ResultVO success(String resultMsg,Object object){
        ResultVO resultVO = new ResultVO();
        resultVO.setResultCode(ResultEnum.SUCCESS.getCode());
        resultVO.setResultMsg(resultMsg);
        resultVO.setResultData(object);
        return resultVO;
    }

    public static ResultVO fail(String resultMsg){
       return ResultVOUtil.fail(ResultEnum.FAIL.getCode(),resultMsg,null);
    }

    @SuppressWarnings("unchecked")
    public static ResultVO fail(String resultCode,String resultMsg,Object object){
        ResultVO resultVO = new ResultVO();
        resultVO.setResultCode(resultCode);
        resultVO.setResultMsg(resultMsg);
        resultVO.setResultData(object);
        return resultVO;
    }
}
