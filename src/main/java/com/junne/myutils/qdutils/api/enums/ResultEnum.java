package com.junne.myutils.qdutils.api.enums;

/**
 * 描述:
 * 返回对象枚举类
 *
 * @author JUNNE
 * @create 2019-10-24 15:24
 */
public enum ResultEnum {
    //成功
    SUCCESS("0","请求成功！"),
    //失败
    FAIL("1","请求失败！"),

    //服务内部错误
    SERVER_ERROR("999","系统服务异常，请联系管理员！"),
    //参数错误
    BAD_REQUEST("10000", "错误的请求方式！"),
    //资源未找到
    NOT_FOUND("10001", "未能找到结果！");

    private String code;
    private String msg;

    ResultEnum(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
