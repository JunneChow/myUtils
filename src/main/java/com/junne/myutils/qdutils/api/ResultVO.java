package com.junne.myutils.qdutils.api;

/**
 * 描述:
 * 请求api处理后的返回对象VO
 *
 * @author JUNNE
 * @create 2019-08-29 14:27
 */
public class ResultVO<T> {
    /**
     * 返回的提示代码
     */
    private String resultCode;

    /**
     * 返回的提示信息
     */
    private String resultMsg;

    /**
     * 返回的数据内容
     */
    private T resultData;

    public String getResultCode() {
        return resultCode;
    }

    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }

    public String getResultMsg() {
        return resultMsg;
    }

    public void setResultMsg(String resultMsg) {
        this.resultMsg = resultMsg;
    }

    public T getResultData() {
        return resultData;
    }

    public void setResultData(T resultData) {
        this.resultData = resultData;
    }

    @Override
    public String toString() {
        return "ResultVO{" +
                "resultCode='" + resultCode + '\'' +
                ", resultMsg='" + resultMsg + '\'' +
                ", resultData=" + resultData +
                '}';
    }
}
