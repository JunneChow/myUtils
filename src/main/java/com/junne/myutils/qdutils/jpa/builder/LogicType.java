package com.junne.myutils.qdutils.jpa.builder;

/**
 * 描述:
 * 逻辑操作类型
 *
 * @author JUNNE
 * @create 2019-12-13 9:49
 */
public enum LogicType {
    OR("or"),
    NOT("not"),
    OR_NOT("orNot");

    private String name;

    LogicType(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
