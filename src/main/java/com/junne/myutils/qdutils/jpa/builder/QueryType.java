package com.junne.myutils.qdutils.jpa.builder;

/**
 * 描述:
 * 查询类型
 *
 * @author JUNNE
 * @create 2019-12-13 9:48
 */
public enum QueryType {
    START_TIME("startTime"),
    END_TIME("endTime"),
    GT("gt"),
    GTE("gte"),
    LT("lt"),
    LTE("lte"),
    LIKE("like"),
    NOT_LIKE("notLike"),
    IN("in"),
    NOT_IN("notIn"),
    IS_NULL("isNull"),
    IS_NOT_NULL("isNotNull"),
    EQUAL("equal"),
    NOT_EQUAL("notEqual");

    private String name;

    QueryType(String name){
        this.name = name;
    }

    /**
     * QueryType值转QueryType
     * @param name QueryType值名称
     * @return 返回QueryType
     */
    public static QueryType toQueryType(String name){
        for (QueryType queryType:QueryType.values()) {
            if (queryType.getName().equalsIgnoreCase(name)){
                return queryType;
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
