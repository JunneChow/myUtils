package com.junne.myutils.qdutils.jpa;

import com.junne.myutils.qdutils.TimeUtil;
import com.junne.myutils.qdutils.exception.WarningException;
import com.junne.myutils.qdutils.jpa.builder.JpaQueryBuilder;
import com.junne.myutils.qdutils.jpa.builder.JpaQueryPath;
import com.junne.myutils.qdutils.jpa.builder.LogicType;
import com.junne.myutils.qdutils.jpa.builder.QueryType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.util.StringUtils;

import javax.persistence.criteria.*;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.*;

/**
 * 描述:
 * Jpa工具
 *
 * @author JUNNE
 * @create 2019-09-05 11:13
 */
public class JpaUtil {

    /**
     * 获取一条记录
     * @param page 分页对象
     * @param <T> 泛型
     * @return 返回一条记录或null
     */
    public static <T> T getOneResult(Page<T> page){
        List<T> content = page.getContent();
        if (content.size() > 0){
            return content.get(0);
        }
        return null;
    }

    /**
     * 获取分页对象
     * @param tClass 实体类
     * @param map 查询的Map对象
     * @return 返回分页对象
     * @throws WarningException 抛出异常
     */
    public static <T> Pageable getPageable(Class<T> tClass, Map<String,Object> map) throws WarningException {
        int currentPage = (Integer) map.get("$currentPage");
        int pageSize = (Integer) map.get("$pageSize");
        String sortStr = (String) map.get("$sort");
        if (sortStr != null && !StringUtils.isEmpty(sortStr)){
            Sort sort = JpaUtil.getSort(tClass, sortStr);
            if (sort != null){
                return PageRequest.of(currentPage,pageSize,sort);
            }
        }
        return PageRequest.of(currentPage, pageSize);
    }

    /**
     * 按字段获取排序对象
     * @param tClass 实体类
     * @param sortStr 排序的基准字段,如字段id【id:desc】,不加“:desc”则默认是递增排序
     *                多个字段用英文逗号","分割。例如："a:desc,b:asc,c"
     * @return 返回排序对象
     * @throws WarningException 抛出异常
     */
    private static <T> Sort getSort(Class<T> tClass, String sortStr) throws WarningException {
        String direction = "asc";
        if (sortStr.contains(",")){
            //多个字段参与排序时
            String[] split = sortStr.split(",");
            List<Sort> sortList = new ArrayList<>();
            for (String sortS : split) {
                if (sortS.contains(":")){
                    String[] sSlit = sortS.split(":");
                    sortS = sSlit[0];
                    direction = sSlit[1];
                }
                Sort sort = JpaUtil.handleSort(tClass, sortS, direction);
                sortList.add(sort);
            }
            if (sortList.size()>0){
                Sort sort = sortList.get(0);
                for (int i=1;i<sortList.size();i++){
                    Sort orders = sortList.get(i);
                    sort.and(orders);
                }
                return sort;
            }
        }else {
            if (sortStr.contains(":")){
                String[] split = sortStr.split(":");
                sortStr = split[0];
                direction = split[1];
            }
            return JpaUtil.handleSort(tClass, sortStr, direction);
        }
        return null;
    }

    /**
     * 出来排序对象
     * @param tClass 对象类
     * @param sortStr 排序字段
     * @param direction 排序方式
     * @param <T> 泛型
     * @return 返回排序对象
     * @throws WarningException 抛出异常
     */
    private static <T> Sort handleSort(Class<T> tClass, String sortStr,String direction) throws WarningException {
        Field sortField = JpaUtil.getField(tClass, sortStr);
        if (sortField == null){
            throw new WarningException("非法的排序字段");
        }
        if ("asc".equalsIgnoreCase(direction)){
            return Sort.by(Sort.Direction.ASC, sortStr);
        }else if ("desc".equalsIgnoreCase(direction)){
            return Sort.by(Sort.Direction.DESC,sortStr);
        }else {
            throw new WarningException("排序方式仅支持【ASC】和【DESC】");
        }
    }

    /**
     * 判断实体对象里的一个或多个字段的值是否已存在，如需要同时校验【姓名】和【学号】时，params可以传(姓名,学号)
     * 需要多个字段确定某个字段的值时，多个字段用“-”分割，要查询的字段后面带“:q”
     * 如需要通过判断【年级】和【班别】的值是否一致，才能确定【姓名】的值是否已存在时，格式如：【年级-班别-姓名:q】
     * @param object 校验属性字段带值的实体对象
     * @param idColumnName 实体对象的主键ID
     * @param jpaSpecificationExecutor JPArepository接口
     * @param params 字段名称，可传一个或多个参数
     * @param <T> 泛型对象
     * @throws WarningException 抛出异常
     */
    public static <T> void checkIfExist(Object object, String idColumnName, JpaSpecificationExecutor jpaSpecificationExecutor, T... params) throws WarningException {
        if (object == null){
            throw new WarningException("查询对象不能为空");
        }
        for (Object obj:params) {
            String key = (String) obj;
            if (key != null && !"".equals(key)){
                if (key.contains("-")){
                    String[] split = key.split("-");
                    JpaUtil.handleCheckColumn(object,idColumnName,jpaSpecificationExecutor, split);
                }else{
                    JpaUtil.handleCheckColumn(object,idColumnName,jpaSpecificationExecutor,key);
                }
            }
        }
    }

    /**
     * 处理字段校验
     * @param object 校验的对象
     * @param idColumnName 校验的对象数据库ID字段名称
     * @param jpaSpecificationExecutor Jpa数据库接口
     * @param params 参数
     * @param <T> 泛型
     * @throws WarningException 抛出警告异常
     */
    @SuppressWarnings("unchecked")
    private static <T> void handleCheckColumn(Object object, String idColumnName, JpaSpecificationExecutor jpaSpecificationExecutor, T... params) throws WarningException {
        Map<String,Object> map = new HashMap<>();
        String column = null;
        String queryValue = null;
        for (Object obj:params) {
            column = (String) obj;
            if (column.contains(":q")){
                String[] split = column.split(":q");
                column = split[0];
            }
            Field field = JpaUtil.getField(object.getClass(), column);
            if (field == null){
                throw new WarningException("非法字段属性【"+column+"】");
            }
            queryValue = (String) JpaUtil.getFieldValue(object, column);
            if (queryValue != null){
                map = new JpaQueryBuilder(map).and(column, queryValue).build();
            }else {
                throw new WarningException("字段【"+column+"】校验相关的值不能为空");
            }
        }
        Specification<?> specification = JpaUtil.getSpecification(object.getClass(), map);
        List<T> list = jpaSpecificationExecutor.findAll(specification);
        if (list.size() > 0){
            //当新增时(即本次传入对象的ID值为null时)
            Object idValue = JpaUtil.getFieldValue(object, idColumnName);
            if (idValue == null){
               throw new WarningException("字段【"+column+"】对应的值【"+queryValue+"】已存在");
            }
            //当修改时(即本次传入对象的ID值不为null时)
            if (!"".equals(idValue)){
                for (Object obj:list) {
                    Object queryIdValue = JpaUtil.getFieldValue(obj, idColumnName);
                    if (queryIdValue != null && !queryIdValue.equals(idValue)){
                        throw new WarningException("字段【"+column+"】对应的值【"+queryValue+"】已存在");
                    }
                }
            }
        }
    }

    /**
     * 判断字段属性是否存在
     * @param tClass 字段所在类
     * @param map 查询的Map对象
     * @param <T> 泛型
     * @throws Exception 抛出异常
     */
    private static <T> void checkFieldIfExist(Class<T> tClass, Map<String,Object> map) throws Exception {
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            //关联查询表的实体对象数组
            String[] entityArr = null;
            String column = entry.getKey();
            //提取表实体类名称
            if (column.contains(":")) {
                String[] split = column.split(":");
                String entityStr = split[0];
                entityArr = entityStr.split("-");
                column = split[1];
            }
            //提取操作
            if (column.contains("#")) {
                String[] split = column.split("#");
                column = split[0];
            }
            Field field = null;
            Class<?> lastClass = tClass;
            if (entityArr != null && entityArr.length>0){
                for (String entiryName:entityArr) {
                    field = JpaUtil.getField(lastClass, entiryName);
                    if (field == null){
                        throw new Exception("["+lastClass+"]不存在字段["+column+"]");
                    }else {
                        Type genericType = field.getGenericType();
                        if(genericType instanceof ParameterizedType){
                            ParameterizedType gType = (ParameterizedType) genericType;
                            lastClass = (Class<?>) gType.getActualTypeArguments()[0];
                        }else{
                            lastClass = field.getType();
                        }
                    }
                }
            }
            field = JpaUtil.getField(lastClass, column);
            if (field == null){
                throw new Exception("["+lastClass+"]不存在字段["+column+"]");
            }
        }
    }

    /**
     * 1.获取Specification对象，查询格式：【实体对象:字段#逻辑操作&查询操作】，当前无需关联对象时，实体对象不用填写，否则会进行关联查询
     *      无逻辑操作时默认是【与操作】，无查询操作默认是【equal查询】，如查询当前实体对象【f】字段值大于15的记录：map.put("f#gt","15");
     * 2.关联对象查询，多个关联对象间用“-”分割，字段紧跟所在的实体对象，
     *      如查当前对象关联的【b】对象关联的【c】对象的【f】字段，其值不为null的记录：map.put("b-c:f#isNotNull","");
     * 3.逻辑操作LogicType【or,not,orNot】，查询【或者】当前实体对象f字段的值是[1，2，3]里的任意一个
     *      map.put("f#or&in","1,2,3")，不加逻辑操作则默认是与查询;
     * @param tClass 要查询的实体类
     * @param map 进行查询的map对象
     * @return 返回Specification
     */
    public static <T> Specification<T> getSpecification(Class<T> tClass, Map<String,Object> map) {
        try {
            //校验字段属性是否存在
            JpaUtil.checkFieldIfExist(tClass,map);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return new Specification<T>() {
            @Override
            public Predicate toPredicate(Root<T> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                List<Predicate> andList = new ArrayList<>();
                List<Predicate> orList = new ArrayList<>();
                //map表示条件关联，后面可根据需要进行相关操作的扩展
                for (Map.Entry<String, Object> entry : map.entrySet()) {
                    //关联查询表的实体对象数组
                    String[] entityArr = null;
                    String column = entry.getKey();
                    String logicOp = null;
                    String queryOp = null;
                    //提取表实体类名称
                    if (column.contains(":")) {
                        String[] split = column.split(":");
                        String entityStr = split[0];
                        entityArr = entityStr.split("-");
                        column = split[1];
                    }
                    //提取操作
                    if (column.contains("#")) {
                        String[] split = column.split("#");
                        column = split[0];
                        queryOp = split[1];
                        if (queryOp.contains("&")) {
                            String[] opStrArr = queryOp.split("&");
                            logicOp = opStrArr[0];
                            queryOp = opStrArr[1];
                        }
                    }
                    Object valueObj = entry.getValue();
                    if (valueObj != null) {
                        //判断字段值是否是动态关联查询对象
                        if (valueObj instanceof JpaQueryPath){
                            JpaQueryPath jpaQueryPath = (JpaQueryPath) valueObj;
                            try {
                                if (LogicType.OR.getName().equalsIgnoreCase(logicOp)) {
                                    orList.add(JpaUtil.getPredicateByPath(root, criteriaBuilder, entityArr, column, jpaQueryPath, queryOp));
                                } else if (LogicType.NOT.getName().equalsIgnoreCase(logicOp)) {
                                    andList.add(criteriaBuilder.not(JpaUtil.getPredicateByPath(root, criteriaBuilder, entityArr, column, jpaQueryPath, queryOp)));
                                } else if (LogicType.OR_NOT.getName().equalsIgnoreCase(logicOp)) {
                                    orList.add(criteriaBuilder.not(JpaUtil.getPredicateByPath(root, criteriaBuilder, entityArr, column, jpaQueryPath, queryOp)));
                                } else {
                                    andList.add(JpaUtil.getPredicateByPath(root, criteriaBuilder, entityArr, column, jpaQueryPath, queryOp));
                                }
                            }catch (Exception e){
                                e.printStackTrace();
                                //去重
                                criteriaQuery.distinct(true);
                                return null;
                            }
                        }else {
                            String value = String.valueOf(valueObj);
                            try {
                                if (LogicType.OR.getName().equalsIgnoreCase(logicOp)) {
                                    orList.add(JpaUtil.getPredicate(root, criteriaBuilder, entityArr, column, value, queryOp));
                                } else if (LogicType.NOT.getName().equalsIgnoreCase(logicOp)) {
                                    andList.add(criteriaBuilder.not(JpaUtil.getPredicate(root, criteriaBuilder, entityArr, column, value, queryOp)));
                                } else if (LogicType.OR_NOT.getName().equalsIgnoreCase(logicOp)) {
                                    orList.add(criteriaBuilder.not(JpaUtil.getPredicate(root, criteriaBuilder, entityArr, column, value, queryOp)));
                                } else {
                                    andList.add(JpaUtil.getPredicate(root, criteriaBuilder, entityArr, column, value, queryOp));
                                }
                            }catch (Exception e){
                                e.printStackTrace();
                                //去重
                                criteriaQuery.distinct(true);
                                return null;
                            }
                        }
                    }
                }
                if (orList.size() > 0) {
                    Predicate orPredicate = criteriaBuilder.or(orList.toArray(new Predicate[0]));
                    andList.add(orPredicate);
                }
                //去重
                criteriaQuery.distinct(true);
                return criteriaBuilder.and(andList.toArray(new Predicate[0]));
            }
        };
    }

    /**
     * 获取Predicate
     * @param root 根对象
     * @param criteriaBuilder CriteriaBuilder对象
     * @param entityArr 实体类对象数组
     * @param column 字段名称
     * @param jpaQueryPath 关联查询字段
     * @param queryOp 操作类型,默认是equal
     * @return 返回Predicate
     */
    private static <Y> Predicate getPredicateByPath(Root<Y> root, CriteriaBuilder criteriaBuilder, String[] entityArr,
                                                    String column, JpaQueryPath jpaQueryPath, String queryOp) {

        QueryType queryType = QueryType.toQueryType(queryOp);
        if (queryType == null){
            queryType = QueryType.EQUAL;
        }
        switch (queryType){
            case START_TIME:
                return criteriaBuilder.greaterThanOrEqualTo(JpaUtil.getPath(root, column, entityArr), JpaUtil.getPath(root, jpaQueryPath.getColumn(), jpaQueryPath.getEntityArr()));
            case END_TIME:
                return criteriaBuilder.lessThanOrEqualTo(JpaUtil.getPath(root, column, entityArr), JpaUtil.getPath(root, jpaQueryPath.getColumn(), jpaQueryPath.getEntityArr()));
            case GT:
                return criteriaBuilder.greaterThan(JpaUtil.getPath(root, column, entityArr),JpaUtil.getPath(root, jpaQueryPath.getColumn(), jpaQueryPath.getEntityArr()));
            case GTE:
                return criteriaBuilder.greaterThanOrEqualTo(JpaUtil.getPath(root, column, entityArr),JpaUtil.getPath(root, jpaQueryPath.getColumn(), jpaQueryPath.getEntityArr()));
            case LT:
                return criteriaBuilder.lessThan(JpaUtil.getPath(root, column, entityArr),JpaUtil.getPath(root, jpaQueryPath.getColumn(), jpaQueryPath.getEntityArr()));
            case LTE:
                return criteriaBuilder.lessThanOrEqualTo(JpaUtil.getPath(root, column, entityArr),JpaUtil.getPath(root, jpaQueryPath.getColumn(), jpaQueryPath.getEntityArr()));
            case LIKE:
                return criteriaBuilder.like(JpaUtil.getPath(root, column, entityArr),"%"+JpaUtil.getPath(root, jpaQueryPath.getColumn(), jpaQueryPath.getEntityArr())+"%");
            case NOT_LIKE:
                return criteriaBuilder.notLike(JpaUtil.getPath(root, column, entityArr),"%"+JpaUtil.getPath(root, jpaQueryPath.getColumn(), jpaQueryPath.getEntityArr())+"%");
            case IS_NULL:
                return criteriaBuilder.isNull(JpaUtil.getPath(root, column, entityArr));
            case IS_NOT_NULL:
                return criteriaBuilder.notEqual(JpaUtil.getPath(root, column, entityArr),JpaUtil.getPath(root, jpaQueryPath.getColumn(), jpaQueryPath.getEntityArr()));
            case NOT_EQUAL:
                return criteriaBuilder.notEqual(JpaUtil.getPath(root, column, entityArr),JpaUtil.getPath(root, jpaQueryPath.getColumn(), jpaQueryPath.getEntityArr()));
            case EQUAL:
                return criteriaBuilder.equal(JpaUtil.getPath(root, column, entityArr),JpaUtil.getPath(root, jpaQueryPath.getColumn(), jpaQueryPath.getEntityArr()));
            default:
                return criteriaBuilder.equal(JpaUtil.getPath(root, column, entityArr),JpaUtil.getPath(root, jpaQueryPath.getColumn(), jpaQueryPath.getEntityArr()));
        }
    }

    /**
     * 获取Predicate
     * @param root 根对象
     * @param criteriaBuilder CriteriaBuilder对象
     * @param entityArr 实体类对象数组
     * @param column 字段名称
     * @param value 字段值
     * @param queryOp 操作类型,默认是equal
     * @return 返回Predicate
     */
    private static <T> Predicate getPredicate(Root<T> root, CriteriaBuilder criteriaBuilder, String[] entityArr,
                                              String column, String value, String queryOp) {

        QueryType queryType = QueryType.toQueryType(queryOp);
        if (queryType == null){
            queryType = QueryType.EQUAL;
        }
        switch (queryType){
            case START_TIME:
                Date startTime = TimeUtil.stringToDate("yyyy-MM-dd HH:mm:ss",value);
                return criteriaBuilder.greaterThanOrEqualTo(JpaUtil.getPath(root, column, entityArr), startTime);
            case END_TIME:
                Date endTime = TimeUtil.stringToDate("yyyy-MM-dd HH:mm:ss",value);
                return criteriaBuilder.lessThanOrEqualTo(JpaUtil.getPath(root, column, entityArr), endTime);
            case GT:
                return criteriaBuilder.greaterThan(JpaUtil.getPath(root, column, entityArr),value);
            case GTE:
                return criteriaBuilder.greaterThanOrEqualTo(JpaUtil.getPath(root, column, entityArr),value);
            case LT:
                return criteriaBuilder.lessThan(JpaUtil.getPath(root, column, entityArr),value);
            case LTE:
                return criteriaBuilder.lessThanOrEqualTo(JpaUtil.getPath(root, column, entityArr),value);
            case LIKE:
                return criteriaBuilder.like(JpaUtil.getPath(root, column, entityArr),"%"+value+"%");
            case NOT_LIKE:
                return criteriaBuilder.notLike(JpaUtil.getPath(root, column, entityArr),"%"+value+"%");
            case IN :case NOT_IN:
                CriteriaBuilder.In<Object> in = criteriaBuilder.in(JpaUtil.getPath(root, column, entityArr));
                if (value.contains(",")){
                    String[] split = value.split(",");
                    for (String str:split) {
                        in.value(str);
                    }
                }else {
                    in.value(value);
                }
                if (QueryType.IN.getName().equalsIgnoreCase(queryOp)){
                    return criteriaBuilder.and(in);
                }else {
                    return criteriaBuilder.not(in);
                }
            case IS_NULL:
                return criteriaBuilder.isNull(JpaUtil.getPath(root, column, entityArr));
            case IS_NOT_NULL:
                return criteriaBuilder.notEqual(JpaUtil.getPath(root, column, entityArr),value);
            case NOT_EQUAL:
                return criteriaBuilder.notEqual(JpaUtil.getPath(root, column, entityArr),value);
            case EQUAL:
                return criteriaBuilder.equal(JpaUtil.getPath(root, column, entityArr),value);
                default:
                    return criteriaBuilder.equal(JpaUtil.getPath(root, column, entityArr),value);
        }
    }

    /**
     * 获取查询对象
     * @param root 查询根对象
     * @param column 查询的字段
     * @param entityName 表的实体类名称
     * @return 返回查询对象
     */
    private static <Y> Path<Y> getPath(Root<?> root, String column, String... entityName) {
        if (entityName != null && entityName.length > 0){
            Join<Object, Object> objectJoin = null;
            for (String table:entityName) {
                if (table != null && !"".equals(table)){
                    if (objectJoin == null){
                        objectJoin = root.join(table);
                    }else {
                        objectJoin = objectJoin.join(table);
                    }
                }
            }
            if (objectJoin != null){
                return objectJoin.get(column);
            }else {
                System.out.println("多表关联查询错误，请检查表对象名称是否正确！");
                return null;
            }
        }
        return root.get(column);
    }

    /**
     * 获取查询对象的字段值
     * @param object 查询的对象
     * @param column 字段名称
     * @return 返回查询值
     */
    public static Object getFieldValue(Object object, String column){
        Field field = JpaUtil.getField(object.getClass(), column);
        if (field != null){
            if(!field.isAccessible()){
                field.setAccessible(true);
            }
            try {
                return field.get(object);
            } catch (IllegalAccessException e) {
                return null;
            }
        }
        return null;
    }

    /**
     * 获取指定对象的字段域
     * @param tClass 查询的类
     * @param column 字段名称
     * @return 返回字段域
     */
    public static <T> Field getField(Class<T> tClass,String column){
        for (Class<?> superClass = tClass; superClass != Object.class; superClass = superClass.getSuperclass()) {
            try {
                return superClass.getDeclaredField(column);
            } catch (NoSuchFieldException e) {
                // Field不在当前类定义,继续向上转型
            }
        }
        return null;
    }
}
