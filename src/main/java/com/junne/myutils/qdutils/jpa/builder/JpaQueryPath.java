package com.junne.myutils.qdutils.jpa.builder;

/**
 * 描述:
 * JPA关联查询的对象
 *
 * @author JUNNE
 * @create 2020-03-18 16:24
 */
public class JpaQueryPath {
    private  String [] entityArr;
    private String column;
    public JpaQueryPath(String fieldValue){
        if (fieldValue.contains(":")) {
            String[] split = fieldValue.split(":");
            String entityStr = split[0];
            entityArr = entityStr.split("-");
            column = split[1];
        }
    }

    public String[] getEntityArr() {
        return entityArr;
    }

    public String getColumn() {
        return column;
    }
}
