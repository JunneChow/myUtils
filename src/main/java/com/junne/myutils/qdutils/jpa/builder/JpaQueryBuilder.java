package com.junne.myutils.qdutils.jpa.builder;

import java.util.HashMap;
import java.util.Map;

/**
 * 描述:
 * 操作查询builder
 *
 * @author JUNNE
 * @create 2019-12-18 14:17
 */
public class JpaQueryBuilder {

    private Map<String,Object> queryMap;

    /**
     * 默认构造器，生成存放查询表达式的Map对象
     */
    public JpaQueryBuilder(){
        queryMap = new HashMap<>();
    }

    /**
     * 带查询Map对象的构造器
     * @param queryMap 外部的Map对象
     */
    public JpaQueryBuilder(Map<String,Object> queryMap){
        this.queryMap = queryMap;
    }

    /**
     * 逻辑与-查询字段是null值的结果
     * @param fieldName 字段名称，关联字段时需声明关联的实体对象名称，如
     *             1.关联b实体对象的id字段，则为：“b:id”；
     *             2.若关联b实体所关联的c实体的name字段，则为：“b-c:name”
     *             3.多级关联时，实体名称间用“-”分割，最后用“:”连接要查询的字段名称
     * @return 返回builder对象
     */
    public JpaQueryBuilder andIsNull(String fieldName){
        String query = new JpaQueryExpression(fieldName).query(QueryType.IS_NULL);
        queryMap.put(query,"null");
        return this;
    }

    /**
     * 逻辑与-查询字段非null值的结果
     * @param fieldName 字段名称，关联字段时需声明关联的实体对象名称，如
     *             1.关联b实体对象的id字段，则为：“b:id”；
     *             2.若关联b实体所关联的c实体的name字段，则为：“b-c:name”
     *             3.多级关联时，实体名称间用“-”分割，最后用“:”连接要查询的字段名称
     * @return 返回builder对象
     */
    public JpaQueryBuilder andIsNotNull(String fieldName){
        String query = new JpaQueryExpression(fieldName).query(QueryType.IS_NOT_NULL);
        queryMap.put(query,"null");
        return this;
    }

    /**
     * 逻辑或-查询字段是null值的结果
     * @param fieldName 字段名称，关联字段时需声明关联的实体对象名称，如
     *             1.关联b实体对象的id字段，则为：“b:id”；
     *             2.若关联b实体所关联的c实体的name字段，则为：“b-c:name”
     *             3.多级关联时，实体名称间用“-”分割，最后用“:”连接要查询的字段名称
     * @return 返回builder对象
     */
    public JpaQueryBuilder orIsNull(String fieldName){
        String query = new JpaQueryExpression(fieldName,LogicType.OR).query(QueryType.IS_NULL);
        queryMap.put(query,"null");
        return this;
    }

    /**
     * 逻辑或-查询字段非null值的结果
     * @param fieldName 字段名称，关联字段时需声明关联的实体对象名称，如
     *             1.关联b实体对象的id字段，则为：“b:id”；
     *             2.若关联b实体所关联的c实体的name字段，则为：“b-c:name”
     *             3.多级关联时，实体名称间用“-”分割，最后用“:”连接要查询的字段名称
     * @return 返回builder对象
     */
    public JpaQueryBuilder orIsNotNull(String fieldName){
        String query = new JpaQueryExpression(fieldName,LogicType.OR).query(QueryType.IS_NOT_NULL);
        queryMap.put(query,"null");
        return this;
    }

    /**
     * 逻辑与查询
     * @param fieldName 字段名称，关联字段时需声明关联的实体对象名称，如
     *             1.关联b实体对象的id字段，则为：“b:id”；
     *             2.若关联b实体所关联的c实体的name字段，则为：“b-c:name”
     *             3.多级关联时，实体名称间用“-”分割，最后用“:”连接要查询的字段名称
     * @param object 查询内容（建议使用String类型）
     * @return 返回builder对象
     */
    public JpaQueryBuilder and(String fieldName,Object object){
        if (this.isNotEmpty(object)){
            String query = new JpaQueryExpression(fieldName).query();
            queryMap.put(query,object);
        }
        return this;
    }

    /**
     * 与 逻辑查询
     * @param fieldName 字段名称，关联字段时需声明关联的实体对象名称，如
     *             1.关联b实体对象的id字段，则为：“b:id”；
     *             2.若关联b实体所关联的c实体的name字段，则为：“b-c:name”
     *             3.多级关联时，实体名称间用“-”分割，最后用“:”连接要查询的字段名称
     * @param object 查询内容（建议使用String类型）
     * @param queryType 查询类型（枚举类型）
     * @return 返回builder对象
     */
    public JpaQueryBuilder and(String fieldName,Object object,QueryType queryType){
        if (this.isNotEmpty(object)){
            String query = new JpaQueryExpression(fieldName).query(queryType);
            queryMap.put(query,object);
        }
        return this;
    }

    /**
     * 或 逻辑查询
     * @param fieldName 字段名称，关联字段时需声明关联的实体对象名称，如
     *             1.关联b实体对象的id字段，则为：“b:id”；
     *             2.若关联b实体所关联的c实体的name字段，则为：“b-c:name”
     *             3.多级关联时，实体名称间用“-”分割，最后用“:”连接要查询的字段名称
     * @param object 查询内容（建议使用String类型）
     * @return 返回builder对象
     */
    public JpaQueryBuilder or(String fieldName,Object object){
        if (this.isNotEmpty(object)){
            String query = new JpaQueryExpression(fieldName,LogicType.OR).query();
            queryMap.put(query,object);
        }
        return this;
    }

    /**
     * 或 逻辑查询
     * @param fieldName 字段名称，关联字段时需声明关联的实体对象名称，如
     *             1.关联b实体对象的id字段，则为：“b:id”；
     *             2.若关联b实体所关联的c实体的name字段，则为：“b-c:name”
     *             3.多级关联时，实体名称间用“-”分割，最后用“:”连接要查询的字段名称
     * @param object 查询内容（建议使用String类型）
     * @param queryType 查询类型（枚举类型）
     * @return 返回builder对象
     */
    public JpaQueryBuilder or(String fieldName,Object object,QueryType queryType){
        if (this.isNotEmpty(object)){
            String query = new JpaQueryExpression(fieldName,LogicType.OR).query(queryType);
            queryMap.put(query,object);
        }
        return this;
    }

    /**
     * 非 逻辑查询
     * @param fieldName 字段名称，关联字段时需声明关联的实体对象名称，如
     *             1.关联b实体对象的id字段，则为：“b:id”；
     *             2.若关联b实体所关联的c实体的name字段，则为：“b-c:name”
     *             3.多级关联时，实体名称间用“-”分割，最后用“:”连接要查询的字段名称
     * @param object 查询内容（建议使用String类型）
     * @return 返回builder对象
     */
    public JpaQueryBuilder not(String fieldName,Object object){
        if (this.isNotEmpty(object)) {
            String query = new JpaQueryExpression(fieldName,LogicType.NOT).query();
            queryMap.put(query,object);
        }
        return this;
    }

    /**
     * 非 逻辑查询
     * @param fieldName 字段名称，关联字段时需声明关联的实体对象名称，如
     *             1.关联b实体对象的id字段，则为：“b:id”；
     *             2.若关联b实体所关联的c实体的name字段，则为：“b-c:name”
     *             3.多级关联时，实体名称间用“-”分割，最后用“:”连接要查询的字段名称
     * @param object 查询内容（建议使用String类型）
     * @param queryType 查询类型（枚举类型）
     * @return 返回builder对象
     */
    public JpaQueryBuilder not(String fieldName,Object object,QueryType queryType){
        if (this.isNotEmpty(object)){
            String query = new JpaQueryExpression(fieldName,LogicType.NOT).query(queryType);
            queryMap.put(query,object);
        }
        return this;
    }

    /**
     * 或非 逻辑查询
     * @param fieldName 字段名称，关联字段时需声明关联的实体对象名称，如
     *             1.关联b实体对象的id字段，则为：“b:id”；
     *             2.若关联b实体所关联的c实体的name字段，则为：“b-c:name”
     *             3.多级关联时，实体名称间用“-”分割，最后用“:”连接要查询的字段名称
     * @param object 查询内容（建议使用String类型）
     * @return 返回builder对象
     */
    public JpaQueryBuilder orNot(String fieldName,Object object){
        if (this.isNotEmpty(object)){
            String query = new JpaQueryExpression(fieldName,LogicType.OR_NOT).query();
            queryMap.put(query,object);
        }
        return this;
    }

    /**
     * 或非 逻辑查询
     * @param fieldName 字段名称，关联字段时需声明关联的实体对象名称，如
     *             1.关联b实体对象的id字段，则为：“b:id”；
     *             2.若关联b实体所关联的c实体的name字段，则为：“b-c:name”
     *             3.多级关联时，实体名称间用“-”分割，最后用“:”连接要查询的字段名称
     * @param object 查询内容（建议使用String类型）
     * @param queryType 查询类型（枚举类型）
     * @return 返回builder对象
     */
    public JpaQueryBuilder orNot(String fieldName,Object object,QueryType queryType){
        if (this.isNotEmpty(object)){
            String query = new JpaQueryExpression(fieldName,LogicType.OR_NOT).query(queryType);
            queryMap.put(query,object);
        }
        return this;
    }

    /**
     * 构建查询的Map对象
     * @return 返回封装查询表达式的Map对象
     */
    public Map<String,Object> build(){
        return queryMap;
    }

    /**
     * 非空判断
     * @param object 查询值对象
     * @return 返回是否为空（布尔值，true：不为空，false：为空）
     */
    private boolean isNotEmpty(Object object){
        if (object != null){
            if (object instanceof String){
                return !"".equals(((String) object).trim());
            }
            return true;
        }
        return false;
    }
}
