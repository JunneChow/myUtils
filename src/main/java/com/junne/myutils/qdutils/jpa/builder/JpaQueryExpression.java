package com.junne.myutils.qdutils.jpa.builder;

/**
 * 描述:
 * 操作查询表达式
 *
 * @author JUNNE
 * @create 2019-12-13 9:47
 */
public class JpaQueryExpression {
    private String operation;

    public JpaQueryExpression(String fieldName){
        this.operation = fieldName;
    }

    public JpaQueryExpression(String fieldName, LogicType logicOperationType){
        this.operation = fieldName+"#"+logicOperationType.getName();
    }

    public String query(){
        return this.getQueryOperationType(QueryType.EQUAL);
    }

    public String query(QueryType queryType){
        return this.getQueryOperationType(queryType);
    }

    private String getQueryOperationType(QueryType queryType){
        if (this.operation.contains("#")){
            if (this.operation.contains("&")){
                this.operation = this.operation.substring(0,this.operation.indexOf("&"));
            }
            this.operation = this.operation + "&" +  queryType.getName();
        }else {
            this.operation = this.operation + "#" +  queryType.getName();
        }
        return this.operation;
    }
}