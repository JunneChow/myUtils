package com.junne.myutils.qdutils.exception;

import org.springframework.http.HttpStatus;

/**
 * 描述:
 * 警告异常
 *
 * @author JUNNE
 * @create 2019-09-06 9:55
 */
public class WarningException extends Exception{
    private static final long serialVersionUID = 5044881218284278078L;

    private Integer httpStatusCode;
    private String code;
    private String msg;

    public WarningException(String msg) {
        super(msg);
        this.msg = msg;
    }

    public WarningException(String code,String msg){
        super(msg);
        this.code= code;
        this.msg = msg;
    }

    public WarningException(HttpStatus httpStatusCode, String msg){
        super(msg);
        this.httpStatusCode = httpStatusCode.value();
        this.code= String.valueOf(httpStatusCode.value());
        this.msg = msg;
    }

    public WarningException(HttpStatus httpStatusCode, String code, String msg){
        super(msg);
        this.httpStatusCode = httpStatusCode.value();
        this.code= code;
        this.msg = msg;
    }

    public Integer getHttpStatusCode() {
        return httpStatusCode;
    }

    public String getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
