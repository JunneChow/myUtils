package com.junne.myutils.qdutils;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * 描述:
 * 时间工具
 *
 * @author JUNNE
 * @create 2019-08-28 16:35
 */
public class TimeUtil {

    /**
     * 获取数天前至本月的月份数组
     * @param pattern 时间格式（例：yyyy-MM）
     * @param day 天数，几天前
     * @return 返回月份数组
     */
    public static String[] getDaysAgoMonthsArray(String pattern, int day){
        Set<String> monthSet = new LinkedHashSet<>();
        LocalDateTime localDateTime = TimeUtil.getInitLocalDateTime();
        //获取day天前的时间
        LocalDateTime daysAgoLd = localDateTime.plusDays(-day);
        while(daysAgoLd.isBefore(LocalDateTime.now())){
            String monthStr = daysAgoLd.format(DateTimeFormatter.ofPattern(pattern));
            monthSet.add(monthStr);
            daysAgoLd = daysAgoLd.plusDays(1);
        }
        return monthSet.toArray(new String[0]);
    }

    /**
     * 获取起始时间到结束时间的天数间隔
     * @param startDate 起始日期
     * @param endDate 结束日期
     * @return 返回天数间隔
     */
    public static int getDaysInterval(Date startDate,Date endDate){
        return (int) ((startDate.getTime() - endDate.getTime()) / (1000*3600*24));
    }

    /**
     * 获取几月前1日凌晨0点的时间字符串
     * @param pattern 时间格式（例：yyyy-MM-dd HH:mm:ss）
     * @param month 几月前（例：1月前则填1）
     * @return 返回时间字符串
     */
    public static String getMonthsAgoFirstDayStr(String pattern,int month){
        LocalDateTime localDateTime = TimeUtil.getInitLocalDateTime();
        //获取month月前的时间
        return localDateTime.plusMonths(-month).with(TemporalAdjusters.firstDayOfMonth()).format(DateTimeFormatter.ofPattern(pattern));
    }

    /**
     * 获取几天前的凌晨0点的时间字符串
     * @param pattern 时间格式（例：yyyy-MM-dd HH:mm:ss）
     * @param day 几天前（例：60天前则填60）
     * @return 返回时间字符串
     */
    public static String getDaysAgoStr(String pattern,int day){
        LocalDateTime localDateTime = TimeUtil.getInitLocalDateTime();
        //获取day日前的时间
        return localDateTime.plusDays(-day).format(DateTimeFormatter.ofPattern(pattern));
    }

    /**
     * 获取几月前的凌晨0点时间字符串
     * @param pattern 时间格式（例：yyyy-MM-dd HH:mm:ss）
     * @param month 几月前（例：1月前则填1）
     * @return 返回时间字符串
     */
    public static String getMonthsAgoStr(String pattern,int month){
        LocalDateTime localDateTime = TimeUtil.getInitLocalDateTime();
        //获取month月前的时间
        return localDateTime.plusMonths(-month).format(DateTimeFormatter.ofPattern(pattern));
    }

    /**
     * 获取几年前的凌晨0点时间字符串
     * @param pattern 时间格式（例：yyyy-MM-dd HH:mm:ss）
     * @param year 几年前（例：1年前则填1）
     * @return 返回时间字符串
     */
    public static String getYearsAgoStr(String pattern,int year){
        LocalDateTime localDateTime = TimeUtil.getInitLocalDateTime();
        //获取year年前的时间
        return localDateTime.plusYears(-year).format(DateTimeFormatter.ofPattern(pattern));
    }

    /**
     * 字符串转Date
     * @param pattern 时间格式（例：yyyy-MM-dd HH:mm:ss）
     * @param timeStr 时间字符串
     * @return 返回Date对象
     */
    public static Date stringToDate(String pattern, String timeStr){
        LocalDateTime localDateTime = LocalDateTime.parse(timeStr, DateTimeFormatter.ofPattern(pattern));
        ZonedDateTime zonedDateTime = localDateTime.atZone(ZoneId.systemDefault());
        return Date.from(zonedDateTime.toInstant());
    }

    /**
     * Date转字符串
     * @param pattern 时间格式（例：yyyy-MM-dd HH:mm:ss）
     * @param date Date对象
     * @return 返回字符串
     */
    public static String dateToString(String pattern, Date date){
        Instant instant = date.toInstant();
        LocalDateTime localDateTime = instant.atZone(ZoneId.systemDefault()).toLocalDateTime();
        return localDateTime.format(DateTimeFormatter.ofPattern(pattern));
    }

    /**
     * 获取当天凌晨00:00:00的LocalDateTime对象
     * @return 返回当天凌晨00:00:00的LocalDateTime对象
     */
    public static LocalDateTime getInitLocalDateTime(){
        return LocalDate.now().atTime(0, 0, 0);
    }

//    public static void main(String[] args) {
//        String[] daysAgoMonthsArray = TimeUtil.getDaysAgoMonthsArray("yyyy-MM", 7);
//        String monthsAgoFirstDayStr = TimeUtil.getMonthsAgoFirstDayStr("yyyy-MM-dd HH:mm:ss", 2);
//        String daysAgoStr = TimeUtil.getDaysAgoStr("yyyy-MM-dd HH:mm:ss", 20);
//        String monthsAgoStr = TimeUtil.getMonthsAgoStr("yyyy-MM-dd HH:mm:ss", 2);
//        String yearsAgoStr = TimeUtil.getYearsAgoStr("yyyy-MM-dd HH:mm:ss", 1);
//        String s = TimeUtil.dateToString("yyyy-MM-dd HH:mm:ss", new Date());
//        Date date = TimeUtil.stringToDate("yyyy-MM-dd HH:mm:ss", s);
//        for (String month:daysAgoMonthsArray) {
//            System.out.println(month);
//        }
//        System.out.println(monthsAgoFirstDayStr);
//        System.out.println(daysAgoStr);
//        System.out.println(monthsAgoStr);
//        System.out.println(yearsAgoStr);
//        System.out.println(s);
//        System.out.println(date);
//    }
}
