package ${project_package}.controller;

import ${project_package}.qdutils.api.ResultVO;
import ${project_package}.qdutils.api.ResultVOUtil;
import ${project_package}.qdutils.exception.WarningException;
import ${project_package}.qdutils.jpa.JpaUtil;
import ${project_package}.qdutils.ParameterValidator;
import ${project_package}.dto.${entity_name}DTO;
import ${project_package}.entity.${entity_name};
import ${project_package}.service.${entity_name}Service;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import java.util.HashMap;
import java.util.Map;

/**
 * 描述:
 * ${entity_name}Controller
 *
 * @author ${generate_author}
 * @create ${generate_date}
 */
@Api(value = "${entity_instance_name}",description = "${entity_name}模块-API")
@RestController
@RequestMapping("/${entity_instance_name}")
public class ${entity_name}Controller {

    @Autowired
    ${entity_name}Service ${entity_instance_name}Service;

    /**
     * 按键值查询一个实体对象
     * @param paramKey 键
     * @param paramValue 值
     * @return 返回查询DTO对象
     * @throws WarningException 抛出异常
     */
    @GetMapping("/{paramKey}/{paramValue}")
    @ApiOperation("查询一个${entity_name}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "paramKey",value = "进行查询的键，目前仅支持：${entity_id}"),
            @ApiImplicitParam(name = "paramValue",value = "进行查询的值")
    })
    public ResultVO getOne(@PathVariable("paramKey") String paramKey, @PathVariable("paramValue") String paramValue) throws WarningException {
        Map<String, Object> map = ${entity_instance_name}Service.handleParamQueryMap(paramKey, paramValue, new HashMap<>());
        ${entity_name} ${entity_instance_name} = ${entity_instance_name}Service.findOne(map);
        ${entity_name}DTO ${entity_instance_name}DTO = ${entity_instance_name}Service.convertTo${entity_name}DTO(${entity_instance_name}, new ${entity_name}DTO());
        return ResultVOUtil.success(${entity_instance_name}DTO);
    }

    /**
     * 分页查询列表
     * @param map 查询的Map对象
     * @return 返回分页的DTO对象列表
     * @throws WarningException 抛出异常
     */
    @PostMapping("/pageList")
    @ApiOperation("分页查询${entity_name}列表")
    @ApiImplicitParam(name = "map",value = "分页查询的map查询对象，其中“$currentPage”和“$pageSize”是必填参数")
    public ResultVO getPageList(@RequestBody Map<String,Object> map) throws WarningException {
        ParameterValidator.verifyMapParams(map,"$currentPage","$pageSize");
        Pageable pageable = JpaUtil.getPageable(${entity_name}.class, map);
        Map<String,Object> queryMap = new HashMap<>();
        //业务相关查询======开始

        //......这里写业务查询代码

        //业务相关查询======结束
        Page<${entity_name}> pageList = ${entity_instance_name}Service.findPageList(queryMap, pageable);
        Page<${entity_name}DTO> ${entity_instance_name}DTOPage = ${entity_instance_name}Service.convertTo${entity_name}DTOPage(pageList);
        return ResultVOUtil.success(${entity_instance_name}DTOPage);
    }

    /**
    * 添加实体对象
    * @param ${entity_instance_name}DTO ${entity_name}的DTO对象
    * @return 返回添加的实体对象
    */
    @PostMapping("/add")
    @ApiOperation("添加${entity_name}")
    public ResultVO add(@RequestBody ${entity_name}DTO ${entity_instance_name}DTO) {
        ${entity_name} ${entity_instance_name} = ${entity_instance_name}Service.convertTo${entity_name}(${entity_instance_name}DTO, new ${entity_name}());
        ${entity_instance_name}.setId(null);
        ${entity_name} save = ${entity_instance_name}Service.save(${entity_instance_name});
        ${entity_name}DTO ${entity_instance_name}DTOrs = ${entity_instance_name}Service.convertTo${entity_name}DTO(save, new ${entity_name}DTO());
        return ResultVOUtil.success(${entity_instance_name}DTOrs);
    }

    /**
    * 更新实体对象
    * @param ${entity_instance_name}DTO ${entity_name}的DTO对象
    * @return 返回更新的实体对象
    * @throws WarningException 抛出异常
    */
    @PutMapping("/update")
    @ApiOperation("更新${entity_name}")
    public ResultVO update(@RequestBody ${entity_name}DTO ${entity_instance_name}DTO) throws WarningException {
        ${entity_name} ${entity_instance_name} = ${entity_instance_name}Service.findById(${entity_instance_name}DTO.getId());
        if (${entity_instance_name} != null){
            ${entity_name} ${entity_instance_name}New = ${entity_instance_name}Service.convertTo${entity_name}(${entity_instance_name}DTO, ${entity_instance_name});
            ${entity_name} update = ${entity_instance_name}Service.update(${entity_instance_name}New);
            ${entity_name}DTO ${entity_instance_name}DTOrs = ${entity_instance_name}Service.convertTo${entity_name}DTO(update, new ${entity_name}DTO());
            return ResultVOUtil.success(${entity_instance_name}DTOrs);
        }else {
            throw new WarningException("该主键的对象不存在");
        }
    }

    /**
     * 删除实体对象
     * @param ${entity_id} 主键ID
     * @return 返回操作提示
     * @throws WarningException 抛出异常
     */
    @DeleteMapping("/delete")
    @ApiOperation("删除${entity_name}")
    @ApiImplicitParam(name = "${entity_id}",value = "${entity_name}的主键ID")
    public ResultVO delete(@RequestParam(value = "${entity_id}") String ${entity_id}) throws WarningException {
        ParameterValidator.paramMustNotBeNull(${entity_id});
        boolean existsById = ${entity_instance_name}Service.checkIfExistsById(${entity_id});
        if (existsById){
            ${entity_instance_name}Service.delete(${entity_id});
            return ResultVOUtil.success();
        }else {
            throw new WarningException("该主键的对象不存在");
        }
    }
}
