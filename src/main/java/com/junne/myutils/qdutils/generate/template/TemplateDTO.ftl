package ${project_package}.dto;

import java.io.Serializable;
<#if fieldObjectSet??>
<#--循环生成变量-->
<#list fieldObjectSet as fieldObj>
import ${fieldObj["fieldObject"]};
</#list>
</#if>

/**
 * 描述:
 * ${entity_name}DTO
 *
 * @author ${generate_author}
 * @create ${generate_date}
 */
public class ${entity_name}DTO implements Serializable {
    private static final long serialVersionUID = 1L;

<#if fieldsList??>
<#--循环生成变量-->
<#list fieldsList as field>
    /**
     * ${field["fieldName"]}
     */
    private ${field["fieldType"]} ${field["fieldName"]};

</#list>

<#--生成getter和setter方法-->
<#list fieldsList as field>
    public ${field["fieldType"]} get${field["fieldName"]?cap_first}() {
        return ${field["fieldName"]};
    }
    public void set${field["fieldName"]?cap_first}(${field["fieldType"]} ${field["fieldName"]}) {
        this.${field["fieldName"]} = ${field["fieldName"]};
    }

</#list>
</#if>
}
