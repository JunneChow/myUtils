package ${project_package}.repository;

import ${project_package}.entity.${entity_name};
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import org.springframework.data.repository.query.Param;

import javax.persistence.LockModeType;
import javax.persistence.QueryHint;

/**
 * 描述:
 * ${entity_name}Repository
 *
 * @author ${generate_author}
 * @create ${generate_date}
 */
public interface ${entity_name}Repository extends JpaRepositoryImplementation<${entity_name},${entity_id_type}> {

    @Lock(value = LockModeType.PESSIMISTIC_WRITE)
    @QueryHints({@QueryHint(name = "javax.persistence.lock.timeout",value = "3000")})
    @Query("select t from ${entity_name} t where t.${entity_id} = :${entity_id}")
    ${entity_name} findByIdForUpdate(@Param("${entity_id}") ${entity_id_type} ${entity_id});
}

