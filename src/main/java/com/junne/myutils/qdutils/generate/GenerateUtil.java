package com.junne.myutils.qdutils.generate;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import java.io.*;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 描述:
 * 代码辅助生成工具类
 *
 * @author JUNNE
 * @create 2019-12-05 14:28
 */
public class GenerateUtil {

    public static Configuration configuration;

    private static String AUTHOR = "autogenic by Junne";
    private static String TEMPLATE_DIR = null;

    //生成文件的绝对路径
    private static String REPOSITORY_FILE_PATH = null;
    private static String SERVICE_FILE_PATH = null;
    private static String CONTROLLER_FILE_PATH = null;
    private static String DTO_FILE_PATH = null;

    //模板的名称
    private static String REPOSITORY_FILE_TEMPLATE = null;
    private static String SERVICE_FILE_TEMPLATE = null;
    private static String CONTROLLER_FILE_TEMPLATE = null;
    private static String DTO_FILE_TEMPLATE = null;

    private static final String ENTITY_ID = "entityId";
    private static final String ENTITY_NAME = "entityName";

    /**
     * 初始化
     * @throws IOException 抛出异常
     */
    public static void init() throws IOException {
        configuration = new Configuration(Configuration.VERSION_2_3_28);

        String className = GenerateUtil.class.getName();
        String classPath = className.replace(".", File.separator);
        classPath = classPath.substring(0,classPath.lastIndexOf(File.separator));
        String projectPath = System.getProperty("user.dir") + File.separator;
        String templatePath = projectPath + "src" + File.separator + "main" + File.separator + "java"
                + File.separator + classPath + File.separator + "template";

        if (TEMPLATE_DIR != null && !"".equalsIgnoreCase(TEMPLATE_DIR)){
            configuration.setDirectoryForTemplateLoading(new File(TEMPLATE_DIR));
        }else {
            configuration.setDirectoryForTemplateLoading(new File(templatePath));
        }

        REPOSITORY_FILE_TEMPLATE = "TemplateRepository.ftl";
        SERVICE_FILE_TEMPLATE = "TemplateService.ftl";
        CONTROLLER_FILE_TEMPLATE = "TemplateController.ftl";
        DTO_FILE_TEMPLATE = "TemplateDTO.ftl";
    }

    /**
     * 代码生成
     * @param generateConfig 代码生成的配置对象
     */
    public static void codeGenerate(GenerateConfig generateConfig) {
        String projectPackage = generateConfig.projectPackage;
        String entityPackageName = generateConfig.entityPackageName;
        //设置作者名称
        AUTHOR = generateConfig.author;
        //设置自定义的模板路径
        if (generateConfig.templateDir != null && !"".equalsIgnoreCase(generateConfig.templateDir)){
            TEMPLATE_DIR = generateConfig.templateDir;
        }
        //处理多个实体类的代码生成
        Set<Map<String, String>> entitySet = generateConfig.entitySet;
        for (Map<String, String> entityMap:entitySet) {
            String entityId = entityMap.get(ENTITY_ID);
            String entityName = entityMap.get(ENTITY_NAME);
            Class<?> clazz = null;
            String entity = projectPackage + "." + entityPackageName + "." + entityName;
            try {
                clazz = Class.forName(entity);
                GenerateUtil.handleGenerate(projectPackage,clazz,entityId);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
                System.out.println("实体类"+entity+"不存在！");
            } catch (TemplateException | IOException e) {
                e.printStackTrace();
                System.out.println("模板生成异常或创建文件失败！");
            }
        }
    }

    /**
     * 处理代码生成
     * @param projectPackage 类的包名
     * @param clazz 进行代码生成的实体类class
     * @param entityId 进行代码生成的实体类主键
     * @throws IOException 抛出异常
     * @throws TemplateException 抛出异常
     */
    private static void handleGenerate(String projectPackage, Class clazz, String entityId) throws IOException, TemplateException {
        long startTime = System.currentTimeMillis();
        GenerateUtil.init();

        Map<String,Object> params = new HashMap<>();
        params.put("project_package",projectPackage);
        GenerateUtil.handleEntityParams(clazz,entityId,params);

        String packagePath = projectPackage.replace(".", File.separator);
        String projectPath = System.getProperty("user.dir") + File.separator;
        String targetFilePath = projectPath + "src" + File.separator + "main" + File.separator + "java"
                + File.separator + packagePath + File.separator;

        REPOSITORY_FILE_PATH = targetFilePath + "repository" + File.separator + clazz.getSimpleName()+"Repository.java";
        SERVICE_FILE_PATH = targetFilePath + "service" + File.separator + clazz.getSimpleName()+"Service.java";
        CONTROLLER_FILE_PATH = targetFilePath  + "controller" + File.separator + clazz.getSimpleName()+"Controller.java";
        DTO_FILE_PATH = targetFilePath  + "dto" + File.separator + clazz.getSimpleName()+"DTO.java";

        GenerateUtil.generateRepositoryFile(params);
        GenerateUtil.generateServiceFile(params);
        GenerateUtil.generateControllerFile(params);
        GenerateUtil.generateDTOFile(params);
        System.out.println("本次生成代码耗时："+(System.currentTimeMillis()-startTime)+"毫秒！");
    }

    /**
     * 设置实体类相关参数
     * @param clazz 实体类class
     * @param entityId 实体类主键名称
     * @param params 模板参数
     */
    private static void handleEntityParams(Class clazz, String entityId, Map<String,Object> params) {
        String entityName = clazz.getSimpleName();
        Type entityIdFieldType = null;
        List<Map<String, String>> fieldsList = new LinkedList<>();
        Set<Map<String,String>> fieldObjectSet = new HashSet<>();
        for (Class<?> superClass = clazz; superClass != Object.class; superClass = superClass.getSuperclass()) {
            //获取字段信息
            Field[] declaredFields = superClass.getDeclaredFields();
            for (Field field:declaredFields) {
                Map<String,String> fieldsMap = new HashMap<>();
                if (!field.isAccessible()){
                    field.setAccessible(true);
                }
                String fieldName = field.getName();
                if ("serialVersionUID".equals(fieldName)){
                    continue;
                }
                //只生成基本类型的DTO属性
                boolean isBaseType = GenerateUtil.isBaseType(field);
                if (!isBaseType){
                    continue;
                }
                Type fieldGenericType = field.getGenericType();
                String fieldType = fieldGenericType.getTypeName();
                fieldType = fieldType.substring(fieldType.lastIndexOf(".")+1);
                //字段类型
                fieldsMap.put("fieldType",fieldType);
                //字段名称
                fieldsMap.put("fieldName",fieldName);
                //处理主键字段
                if (entityId.equals(fieldName)){
                    entityIdFieldType = field.getGenericType();
                    ((LinkedList<Map<String,String>>) fieldsList).addFirst(fieldsMap);
                }else {
                    fieldsList.add(fieldsMap);
                }
            }
        }
        if (entityIdFieldType != null){
            String typeName = entityIdFieldType.getTypeName();
            String entityIdType = typeName.substring(typeName.lastIndexOf(".")+1, typeName.length());
            params.put("entity_name",entityName);
            params.put("entity_id",entityId);
            params.put("entity_id_type",entityIdType);
            params.put("fieldsList",fieldsList);
            params.put("fieldObjectSet",fieldObjectSet);

            String entityInstanceName = entityName.substring(0, 1).toLowerCase() + entityName.substring(1);
            params.put("entity_instance_name",entityInstanceName);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            params.put("generate_date",simpleDateFormat.format(new Date()));
            params.put("generate_author",AUTHOR);
        }
    }

    private static boolean isBaseType(Field field){
        return GenerateUtil.isTargetClassType(field, Byte.class)
                || GenerateUtil.isTargetClassType(field, Short.class)
                || GenerateUtil.isTargetClassType(field, Integer.class)
                || GenerateUtil.isTargetClassType(field, Long.class)
                || GenerateUtil.isTargetClassType(field, Float.class)
                || GenerateUtil.isTargetClassType(field, Double.class)
                || GenerateUtil.isTargetClassType(field, Boolean.class)
                || GenerateUtil.isTargetClassType(field, Character.class)
                || GenerateUtil.isTargetClassType(field, String.class);
    }

    private static boolean isTargetClassType(Field field, Class targetType) {
        return field.getType() == targetType;
    }

    /**
     * 生成Repository类
     * @param params 模板参数
     * @throws IOException 抛出异常
     * @throws TemplateException 抛出异常
     */
    private static void generateRepositoryFile(Map<String,Object> params) throws IOException, TemplateException {
        if (REPOSITORY_FILE_PATH != null && !"".equals(REPOSITORY_FILE_PATH.trim())){
            Template template = configuration.getTemplate(REPOSITORY_FILE_TEMPLATE,"UTF-8");
            File targetFile = new File(REPOSITORY_FILE_PATH);
            GenerateUtil.generateFile(params,template,targetFile);
        }
    }

    /**
     * 生成Service类
     * @param params 模板参数
     * @throws IOException 抛出异常
     * @throws TemplateException 抛出异常
     */
    private static void generateServiceFile(Map<String,Object> params) throws IOException, TemplateException {
        if (SERVICE_FILE_PATH != null && !"".equals(SERVICE_FILE_PATH.trim())){
            Template template = configuration.getTemplate(SERVICE_FILE_TEMPLATE,"UTF-8");
            File targetFile = new File(SERVICE_FILE_PATH);
            GenerateUtil.generateFile(params,template,targetFile);
        }
    }

    /**
     * 生成Controller类
     * @param params 模板参数
     * @throws IOException 抛出异常
     * @throws TemplateException 抛出异常
     */
    private static void generateControllerFile(Map<String,Object> params) throws IOException, TemplateException {
        if (CONTROLLER_FILE_PATH != null && !"".equals(CONTROLLER_FILE_PATH.trim())){
            Template template = configuration.getTemplate(CONTROLLER_FILE_TEMPLATE,"UTF-8");
            File targetFile = new File(CONTROLLER_FILE_PATH);
            GenerateUtil.generateFile(params,template,targetFile);
        }
    }

    /**
     * 生成DTO类
     * @param params 模板参数
     * @throws IOException 抛出异常
     * @throws TemplateException 抛出异常
     */
    private static void generateDTOFile(Map<String,Object> params) throws IOException, TemplateException {
        if (DTO_FILE_PATH != null && !"".equals(DTO_FILE_PATH.trim())){
            Template template = configuration.getTemplate(DTO_FILE_TEMPLATE,"UTF-8");
            File targetFile = new File(DTO_FILE_PATH);
            GenerateUtil.generateFile(params,template,targetFile);
        }
    }

    /**
     * 生成具体文件
     * @param params 模板参数
     * @param template 模板对象
     * @param targetFile 目标文件对象
     * @throws IOException 抛出异常
     * @throws TemplateException 抛出异常
     */
    private static void generateFile(Map<String,Object> params,Template template,File targetFile) throws IOException, TemplateException {
        if (!targetFile.exists()){
            File parentFile = targetFile.getParentFile();
            if (!parentFile.exists()){
                parentFile.mkdirs();
            }
            targetFile.createNewFile();
            Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(targetFile), StandardCharsets.UTF_8));
            template.process(params,writer);
        }else {
            System.out.println("目标文件【"+targetFile+"】已存在，跳过本次生成操作！");
        }
    }

    /**
     * 代码生成的配置类
     */
    public static class GenerateConfig{
        String projectPackage = null;
        String entityPackageName = null;
        String author = null;
        String templateDir = null;

        Set<Map<String,String>> entitySet = new HashSet<>();

        public GenerateConfig setProjectPackage(String projectPackage){
            this.projectPackage = projectPackage;
            return this;
        }

        public GenerateConfig setEntityPackageName(String entityPackageName){
            this.entityPackageName = entityPackageName;
            return this;
        }

        public GenerateConfig setEntity(String entityId,String entityName){
            Map<String,String> entityMap = new HashMap<>();
            entityMap.put(ENTITY_ID,entityId);
            entityMap.put(ENTITY_NAME,entityName);
            entitySet.add(entityMap);
            return this;
        }

        public GenerateConfig setTemplateDir(String templateDir){
            this.templateDir = templateDir;
            return this;
        }

        public GenerateConfig setAuthor(String author){
            this.author = author;
            return this;
        }

    }

    public static void main(String[] args) throws Exception {
        GenerateConfig generateConfig = new GenerateConfig();
        //配置项目包和实体类包名（项目包+"."+实体类包名=>实体类包）及作者名称
        generateConfig.setProjectPackage("com.junne.myutils")
                .setEntityPackageName("entity")
                .setAuthor("JUNNE");

        //配置要进行代码生成的实体类ID及名称
        generateConfig.setEntity("id","Student");
        //进行代码生成
        GenerateUtil.codeGenerate(generateConfig);
    }
}
