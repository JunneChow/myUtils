package ${project_package}.service;

import ${project_package}.qdutils.exception.WarningException;
import ${project_package}.qdutils.jpa.JpaUtil;
import ${project_package}.qdutils.SpringUtil;
import ${project_package}.dto.${entity_name}DTO;
import ${project_package}.entity.${entity_name};
import ${project_package}.repository.${entity_name}Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 描述:
 * ${entity_name}Service
 *
 * @author ${generate_author}
 * @create ${generate_date}
 */

@Service
public class ${entity_name}Service {

    @Autowired
    private ${entity_name}Repository ${entity_instance_name}Repository;

    /**
     * 处理请求参数Map对象
     * @param paramKey 请求参数键
     * @param paramValue 请求参数值
     * @param map 请求参数Map对象
     * @return 返回用于查询的Map对象
     * @throws WarningException 抛出异常
     */
    public Map<String, Object> handleParamQueryMap(String paramKey, String paramValue,Map<String, Object> map) throws WarningException {
        if ("${entity_id}".equals(paramKey)){
            map.put("${entity_id}",paramValue);
        }else {
            throw new WarningException("请求参数异常");
        }
        return map;
    }

    /**
     * 查询一条记录
     * @param map 查询的map，一般是查询对象的字段及查询值
     * @return 返回最多一条记录
     */
    public ${entity_name} findOne(Map<String, Object> map){
        Pageable pageable = PageRequest.of(0,1);
        Specification<${entity_name}> specification = JpaUtil.getSpecification(${entity_name}.class, map);
        if (specification == null){
            return null;
        }
        Page<${entity_name}> page = ${entity_instance_name}Repository.findAll(specification, pageable);
        return JpaUtil.getOneResult(page);
    }

    /**
     * 分页查询list
     * @param map 查询的map，一般是查询对象的字段及查询值
     * @param pageable Pageable分页对象
     * @return 返回分页的数据集
     */
    public Page<${entity_name}> findPageList(Map<String, Object> map, Pageable pageable){
        Specification<${entity_name}> specification = JpaUtil.getSpecification(${entity_name}.class, map);
        if (specification == null){
            return null;
        }
        return ${entity_instance_name}Repository.findAll(specification, pageable);
    }

    /**
     * 查询返回list
     * @param map 查询的map，一般是查询对象的字段及查询值
     * @return 返回list结果
     */
    public List<${entity_name}> findList(Map<String, Object> map) throws WarningException{
        Sort sort = Sort.by(Sort.Direction.ASC, "${entity_id}");
        String sortStr = (String) map.get("$sort");
        if (sortStr != null && !StringUtils.isEmpty(sortStr)){
            sort = JpaUtil.getSort(${entity_name}.class, sortStr);
        }
        map.remove("$sort");
        Specification<${entity_name}> specification = JpaUtil.getSpecification(${entity_name}.class, map);
        if (specification == null){
            return null;
        }
        return ${entity_instance_name}Repository.findAll(specification,sort);
    }

    /**
     * 按主键判断实体对象是否存在
     * @param ${entity_id} 主键ID
     * @return 返回布尔值
     */
    public boolean checkIfExistsById(${entity_id_type} ${entity_id}){
        return ${entity_instance_name}Repository.existsById(${entity_id});
    }

    /**
     * 按主键查找对象
     * @param ${entity_id} 主键ID
     * @return 返回查询对象
     */
    public ${entity_name} findById(${entity_id_type} ${entity_id}){
        return ${entity_instance_name}Repository.findById(${entity_id}).orElse(null);
    }

    /**
     * 添加实体对象
     * @param ${entity_instance_name} 实体对象
     * @return 返回添加后的实体对象
     */
    @Transactional(rollbackFor = Exception.class)
    public ${entity_name} save(${entity_name} ${entity_instance_name}) {
        return ${entity_instance_name}Repository.save(${entity_instance_name});
    }

    /**
     * 更新实体对象
     * @param ${entity_instance_name} 实体对象
     * @return 返回更新后的实体对象
     */
    @Transactional(rollbackFor = Exception.class)
    public ${entity_name} update(${entity_name} ${entity_instance_name}) throws WarningException{
        //获得行记录的悲观锁
        ${entity_name} byIdForUpdate = ${entity_instance_name}Repository.findByIdForUpdate(${entity_instance_name}.get${entity_id?cap_first}());
        if (byIdForUpdate == null){
            throw new WarningException("当前记录已失效！");
        }
        return ${entity_instance_name}Repository.saveAndFlush(${entity_instance_name});
    }


    /**
     * 按主键删除实体对象
     * @param ${entity_id} 主键ID
     */
    @Transactional(rollbackFor = Exception.class)
    public void delete(${entity_id_type} ${entity_id}) throws WarningException {
        //获得行记录的悲观锁
        ${entity_name} byIdForUpdate = ${entity_instance_name}Repository.findByIdForUpdate(${entity_id});
        if (byIdForUpdate == null){
            throw new WarningException("当前记录已失效！");
        }
        ${entity_instance_name}Repository.delete(byIdForUpdate);
    }

    /**
     * 实体对象分页数据转DTO对象分页数据
     * @param ${entity_name}Page 实体对象分页数据
     * @return 返回DTO对象分页数据
     */
    public Page<${entity_name}DTO> convertTo${entity_name}DTOPage(Page<${entity_name}> ${entity_name}Page){
        if (${entity_name}Page == null){
            return null;
        }
        List<${entity_name}> content = ${entity_name}Page.getContent();
        if (content.size() == 0){
            return null;
        }
        List<${entity_name}DTO> ${entity_name}DTOList = this.convertTo${entity_name}DTOList(content, new ArrayList<>());
        return new PageImpl<>(${entity_name}DTOList,${entity_name}Page.getPageable(),${entity_name}Page.getTotalElements());
    }

    /**
     * 实体对象列表转DTO对象列表
     * @param ${entity_instance_name}List 实体对象列表
     * @param ${entity_instance_name}DTOList DTO对象列表
     * @return 返回DTO对象列表
     */
    public List<${entity_name}DTO> convertTo${entity_name}DTOList(List<${entity_name}> ${entity_instance_name}List,List<${entity_name}DTO> ${entity_instance_name}DTOList){
        if (${entity_instance_name}List == null){
            return null;
        }
        for (${entity_name} ${entity_instance_name}:${entity_instance_name}List) {
            ${entity_name}DTO ${entity_instance_name}DTO = this.convertTo${entity_name}DTO(${entity_instance_name}, new ${entity_name}DTO());
            if (${entity_instance_name}DTO != null){
                ${entity_instance_name}DTOList.add(${entity_instance_name}DTO);
            }
        }
        return ${entity_instance_name}DTOList;
    }

    /**
     * 实体对象转DTO对象
     * @param ${entity_instance_name} 实体对象
     * @param ${entity_instance_name}DTO DTO对象
     * @return 返回DTO对象
     */
    public ${entity_name}DTO convertTo${entity_name}DTO(${entity_name} ${entity_instance_name},${entity_name}DTO ${entity_instance_name}DTO){
        if (${entity_instance_name} == null){
            return null;
        }
        SpringUtil.copyPropertiesIgnoreNull(${entity_instance_name},${entity_instance_name}DTO);
        return ${entity_instance_name}DTO;
    }

    /**
     * DTO对象转实体对象
     * @param ${entity_instance_name}DTO DTO对象
     * @param ${entity_instance_name} 实体对象
     * @return 返回实体对象
     */
    public ${entity_name} convertTo${entity_name}(${entity_name}DTO ${entity_instance_name}DTO,${entity_name} ${entity_instance_name}) {
        if (${entity_instance_name}DTO == null){
            return null;
        }
        SpringUtil.copyPropertiesIgnoreNull(${entity_instance_name}DTO,${entity_instance_name});
        return ${entity_instance_name};
    }
}
