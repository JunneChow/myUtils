package com.junne.myutils.qdutils;

import com.junne.myutils.qdutils.exception.WarningException;

import java.util.Map;

/**
 * 描述:
 * 参数校验
 *
 * @author JUNNE
 * @create 2019-08-28 12:50
 */
public class ParameterValidator {

    /**
     * 校验入参map中的指定参数是否为空
     * @param map map对象
     * @param params 参数名称（字符串，如"currentPage"）
     * @param <T> 泛型
     * @throws WarningException 抛出异常
     */
    public static <T extends Object> void verifyMapParams(Map<String,Object> map,T... params) throws WarningException {
        for (Object obj : params) {
            if(map != null && map.size() > 0){
                if (obj instanceof String) {
                    String paramKey = (String) obj;
                    Object paramValue = map.get(paramKey);
                    if (paramValue == null) {
                        throw new WarningException("必填参数【"+paramKey+"】不能为空");
                    }else if(paramValue instanceof String){
                        String paramValueStr = (String) paramValue;
                        if ("".equals(paramValueStr.trim())) {
                            throw new WarningException("必填参数【"+paramKey+"】不能为空");
                        }
                    }
                }
            }else{
                throw new WarningException("必填参数不能为空");
            }
        }
    }

    /**
     * 校验参数是否为空
     * @param params 参数值
     * @param <T> 泛型
     * @throws WarningException 抛出异常
     */
    public static <T extends Object> void paramMustNotBeNull(T... params) throws WarningException {
        for (Object obj : params) {
            if (obj instanceof String) {
                String str = (String) obj;
                if ("".equals(str.trim())) {
                    throw new WarningException("必填参数不能为空");
                }
            }
            if (obj == null) {
                throw new WarningException("必填参数不能为空");
            }
        }
    }
}
