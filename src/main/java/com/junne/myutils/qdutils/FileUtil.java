package com.junne.myutils.qdutils;

import com.junne.myutils.qdutils.exception.WarningException;

import java.io.*;
import java.nio.channels.FileChannel;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * 描述:
 * 上传工具
 *
 * @author JUNNE
 * @create 2019-09-23 10:37
 */
public class FileUtil {

    /**
     * 文件复制（递归复制，包括空文件夹）
     * @param sourceFilePath 源文件路径
     * @param targetFilePath 目标文件路径
     */
    public static void copyFile(String sourceFilePath,String targetFilePath){
        File sourceFile = new File(sourceFilePath);
        File targetFile = new File(targetFilePath);
        //转换成标准文件路径
        sourceFilePath = sourceFile.getAbsolutePath();
        targetFilePath = targetFile.getAbsolutePath();
        if (sourceFile.exists()){
            //判断是否是文件夹
            if (sourceFile.isDirectory()){
                File[] files = sourceFile.listFiles();
                if (files == null || files.length==0){
                    return;
                }
                for (File tempFile:files) {
                    String tempFilePath = tempFile.getAbsolutePath().substring(sourceFilePath.length());
                    if (tempFile.isDirectory()){
                        //在目标路径下创建相应的文件夹
                        boolean mkdirs = new File(targetFilePath + File.separator + tempFilePath).mkdirs();
                    }
                    //递归复制
                    FileUtil.copyFile(tempFile.getAbsolutePath(),targetFilePath+File.separator+tempFilePath);
                }
            }
            try {
                if (!targetFile.exists() || targetFile.delete()){
                    //只复制文件
                    if (!sourceFile.isDirectory()){
                        boolean newFile = targetFile.createNewFile();
                        try (FileChannel inputFileChannel = new FileInputStream(sourceFile).getChannel(); FileChannel outputFileChannel = new FileOutputStream(targetFile).getChannel()) {
                            outputFileChannel.transferFrom(inputFileChannel, 0, inputFileChannel.size());
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 删除某文件夹内除指定文件夹外的所有文件夹
     * @param parentFolderPath 进行操作的文件夹，只是对该文件夹内的数据进行操作
     * @param exceptFolderName 排除的文件夹，不定项参数，例如："2019-10","2019-11"
     */
    public static void deleteFolderInParent(String parentFolderPath, String... exceptFolderName){
        File file = new File(parentFolderPath);
        if (file.exists() && file.isDirectory()){
            ExceptFileFilter myFileNameFilter = new ExceptFileFilter(exceptFolderName);
            File[] files = file.listFiles(myFileNameFilter);
            if (files != null){
                for (File tempFile : files) {
                    if (tempFile.isDirectory()){
                        FileUtil.deleteFile(tempFile.getAbsolutePath());
                    }
                }
            }
        }
    }

    /**
     * 递归删除文件或文件夹
     * @param filePath 文件或文件夹的路径
     */
    public static void deleteFile(String filePath){
        File file = new File(filePath);
        if (file.exists()){
            if (file.isDirectory()){
                File[] files = file.listFiles();
                if (files != null && files.length > 0){
                    for (File tempFile:files) {
                        if (tempFile.isDirectory()){
                            FileUtil.deleteFile(tempFile.getAbsolutePath());
                        }else {
                            tempFile.delete();
                        }
                    }
                }
            }
            file.delete();
        }
    }

    /**
     * 保存对象内容为txt文件
     * @param absoluteFilePath 文件的绝对路径
     * @param sourceStr 保存的内容字符串
     * @return 返回操作结果（true：成功，false：失败）
     */
    public static Boolean writeToTxt(String absoluteFilePath, String sourceStr) throws WarningException {
        if (sourceStr != null){
            BufferedWriter bufferedWriter;
            try {
                File file = new File(absoluteFilePath);
                if (!file.getParentFile().exists()){
                    file.getParentFile().mkdirs();
                }
                if (!file.exists()){
                    file.createNewFile();
                }else {
                    //删除旧文件
                    file.delete();
                }
                FileOutputStream fileOutputStream = new FileOutputStream(file);
                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream, "UTF-8");
                bufferedWriter = new BufferedWriter(outputStreamWriter);
                bufferedWriter.write(sourceStr);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStreamWriter.close();
                fileOutputStream.close();
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                throw new WarningException("文件操作异常！");
            }
        }
        return false;
    }

    /**
     * 读取TXT文件的字符串
     * @param absoluteFilePath 文件的绝对路径
     * @return 返回读取的字符串
     */
    public static String readFromTxt(String absoluteFilePath) throws WarningException {
        File file = new File(absoluteFilePath);
        StringBuilder stringBuilder = new StringBuilder();
        BufferedReader bufferedReader;
        if (file.exists()){
            try {
                FileInputStream fileInputStream = new FileInputStream(absoluteFilePath);
                InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream, "UTF-8");
                bufferedReader = new BufferedReader(inputStreamReader);
                String line = "";
                while ((line = bufferedReader.readLine()) != null){
                    stringBuilder.append(line);
                }
                bufferedReader.close();
                inputStreamReader.close();
                fileInputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
                throw new WarningException("文件操作异常");
            }
        }else {
            throw new WarningException("找不到该文件");
        }
        return stringBuilder.toString();
    }

    /**
     * 排除指定文件的Filter
     */
    public static class ExceptFileFilter implements FilenameFilter{

        private Set<String> exceptSet = new HashSet<>();

        public ExceptFileFilter(String... exceptFileName){
            if (exceptFileName != null && exceptFileName.length>0){
                Collections.addAll(this.exceptSet, exceptFileName);
            }
        }

        @Override
        public boolean accept(File dir, String name) {
            return !exceptSet.contains(name);
        }
    }
}
