-- 插入系统用户数据
INSERT INTO sys_user(id,enable,update_by,update_time,nick_name,password,salt,state,user_name) SELECT 'super_admin', '1', NULL, NULL, '超级管理员', '1da409d336c15ff52b712b57ad6b2e6a', 'ED49C3FED75A513A79CB8BD1D4715D57', 1, 'superadmin' FROM DUAL WHERE NOT EXISTS (select * from sys_user where id='super_admin');
INSERT INTO sys_user(id,enable,update_by,update_time,nick_name,password,salt,state,user_name) SELECT 'sec_auditor', '1', NULL, NULL, '安全审计员', 'f7f4667b747bd8f7d68f864dc5815253', '74CD4E673566D1010C6875589422F64D', 1, 'secauditor' FROM DUAL WHERE NOT EXISTS (select * from sys_user where id='sec_auditor');
INSERT INTO sys_user(id,enable,update_by,update_time,nick_name,password,salt,state,user_name) SELECT 'sys_admin', '1', NULL, NULL, '系统管理员', '2693010e2906925ba4b669ac7284d0f3', 'BE0DEBF79F63E1F67232E6978EF51D0B', 1, 'sysadmin' FROM DUAL WHERE NOT EXISTS (select * from sys_user where id='sys_admin');

-- 插入角色数据
INSERT INTO sys_role(id,enable,update_by,update_time,description,role_name) SELECT '1', '1', NULL, NULL, '安全审计员，审阅安全审计日志', '安全审计员' FROM DUAL WHERE NOT EXISTS (select * from sys_role where id='1');
INSERT INTO sys_role(id,enable,update_by,update_time,description,role_name) SELECT '2', '1', NULL, NULL, '系统管理员，系统用户维护', '系统管理员' FROM DUAL WHERE NOT EXISTS (select * from sys_role where id='2');

-- 插入权限数据
INSERT INTO sys_permission(id,parent_id,permission_name,css,href,type,permission,sort,enable) SELECT '1', '0', '用户管理', 'menu-icon fa fa-users', '', '1', '', '1',1 FROM DUAL WHERE NOT EXISTS (select * from sys_permission where id='1');
INSERT INTO sys_permission(id,parent_id,permission_name,css,href,type,permission,sort,enable) SELECT '2', '1', '用户查询', 'menu-icon fa fa-user', '/user', '1', '', '2',1 FROM DUAL WHERE NOT EXISTS (select * from sys_permission where id='2');
INSERT INTO sys_permission(id,parent_id,permission_name,css,href,type,permission,sort,enable) SELECT '3', '2', '查询', '', '', '2', 'sys:user:query', '100',1 FROM DUAL WHERE NOT EXISTS (select * from sys_permission where id='3');
INSERT INTO sys_permission(id,parent_id,permission_name,css,href,type,permission,sort,enable) SELECT '4', '2', '新增', '', '', '2', 'sys:user:add', '100',1 FROM DUAL WHERE NOT EXISTS (select * from sys_permission where id='4');
INSERT INTO sys_permission(id,parent_id,permission_name,css,href,type,permission,sort,enable) SELECT '5', '2', '删除', '', '', '2', 'sys:user:del', '100',1 FROM DUAL WHERE NOT EXISTS (select * from sys_permission where id='5');
INSERT INTO sys_permission(id,parent_id,permission_name,css,href,type,permission,sort,enable) SELECT '6', '1', '修改密码', 'menu-icon fa fa-pencil-square-o', '/password', '1', 'sys:user:password', '6',1 FROM DUAL WHERE NOT EXISTS (select * from sys_permission where id='6');
INSERT INTO sys_permission(id,parent_id,permission_name,css,href,type,permission,sort,enable) SELECT '7', '0', '系统设置', 'menu-icon fa fa-gears', '', '1', '', '5',1 FROM DUAL WHERE NOT EXISTS (select * from sys_permission where id='7');
INSERT INTO sys_permission(id,parent_id,permission_name,css,href,type,permission,sort,enable) SELECT '8', '7', '菜单', 'menu-icon fa fa-cog', '/menu', '1', '', '6',1 FROM DUAL WHERE NOT EXISTS (select * from sys_permission where id='8');
INSERT INTO sys_permission(id,parent_id,permission_name,css,href,type,permission,sort,enable) SELECT '9', '8', '查询', '', '', '2', 'sys:menu:query', '100',1 FROM DUAL WHERE NOT EXISTS (select * from sys_permission where id='9');
INSERT INTO sys_permission(id,parent_id,permission_name,css,href,type,permission,sort,enable) SELECT '10', '8', '新增', '', '', '2', 'sys:menu:add', '100',1 FROM DUAL WHERE NOT EXISTS (select * from sys_permission where id='10');
INSERT INTO sys_permission(id,parent_id,permission_name,css,href,type,permission,sort,enable) SELECT '11', '8', '删除', '', '', '2', 'sys:menu:del', '100',1 FROM DUAL WHERE NOT EXISTS (select * from sys_permission where id='11');
INSERT INTO sys_permission(id,parent_id,permission_name,css,href,type,permission,sort,enable) SELECT '12', '7', '角色', 'menu-icon fa fa-user-secret', '/role', '1', '', '7',1 FROM DUAL WHERE NOT EXISTS (select * from sys_permission where id='12');
INSERT INTO sys_permission(id,parent_id,permission_name,css,href,type,permission,sort,enable) SELECT '13', '12', '查询', '', '', '2', 'sys:role:query', '100',1 FROM DUAL WHERE NOT EXISTS (select * from sys_permission where id='13');
INSERT INTO sys_permission(id,parent_id,permission_name,css,href,type,permission,sort,enable) SELECT '14', '12', '新增', '', '', '2', 'sys:role:add', '100',1 FROM DUAL WHERE NOT EXISTS (select * from sys_permission where id='14');
INSERT INTO sys_permission(id,parent_id,permission_name,css,href,type,permission,sort,enable) SELECT '15', '12', '删除', '', '', '2', 'sys:role:del', '100',1 FROM DUAL WHERE NOT EXISTS (select * from sys_permission where id='15');
INSERT INTO sys_permission(id,parent_id,permission_name,css,href,type,permission,sort,enable) SELECT '16', '0', '电子地图', 'menu-icon fa fa-folder-open', '/emap', '1', '', '8',1 FROM DUAL WHERE NOT EXISTS (select * from sys_permission where id='16');
INSERT INTO sys_permission(id,parent_id,permission_name,css,href,type,permission,sort,enable) SELECT '17', '16', '查询', '', '', '2', 'sys:file:query', '100',1 FROM DUAL WHERE NOT EXISTS (select * from sys_permission where id='17');
INSERT INTO sys_permission(id,parent_id,permission_name,css,href,type,permission,sort,enable) SELECT '18', '16', '删除', '', '', '2', 'sys:file:del', '100',1 FROM DUAL WHERE NOT EXISTS (select * from sys_permission where id='18');
INSERT INTO sys_permission(id,parent_id,permission_name,css,href,type,permission,sort,enable) SELECT '19', '0', '数据源监控', 'menu-icon fa fa-eye', '/druid/index.html', '1', '', '9',1 FROM DUAL WHERE NOT EXISTS (select * from sys_permission where id='19');
INSERT INTO sys_permission(id,parent_id,permission_name,css,href,type,permission,sort,enable) SELECT '20', '0', '接口swagger', 'menu-icon fa fa-file-pdf-o', '/swagger-ui.html', '1', '', '10',1 FROM DUAL WHERE NOT EXISTS (select * from sys_permission where id='20');
INSERT INTO sys_permission(id,parent_id,permission_name,css,href,type,permission,sort,enable) SELECT '21', '0', '代码生成', 'menu-icon fa fa-wrench', '/generate', '1', 'generate:edit', '11',1 FROM DUAL WHERE NOT EXISTS (select * from sys_permission where id='21');
INSERT INTO sys_permission(id,parent_id,permission_name,css,href,type,permission,sort,enable) SELECT '22', '0', '公告管理', 'menu-icon fa fa-book', '/notice', '1', '', '12',1 FROM DUAL WHERE NOT EXISTS (select * from sys_permission where id='22');
INSERT INTO sys_permission(id,parent_id,permission_name,css,href,type,permission,sort,enable) SELECT '23', '22', '查询', '', '', '2', 'notice:query', '100',1 FROM DUAL WHERE NOT EXISTS (select * from sys_permission where id='23');
INSERT INTO sys_permission(id,parent_id,permission_name,css,href,type,permission,sort,enable) SELECT '24', '22', '添加', '', '', '2', 'notice:add', '100',1 FROM DUAL WHERE NOT EXISTS (select * from sys_permission where id='24');
INSERT INTO sys_permission(id,parent_id,permission_name,css,href,type,permission,sort,enable) SELECT '25', '22', '删除', '', '', '2', 'notice:del', '100',1 FROM DUAL WHERE NOT EXISTS (select * from sys_permission where id='25');
INSERT INTO sys_permission(id,parent_id,permission_name,css,href,type,permission,sort,enable) SELECT '26', '0', '日志查询', 'menu-icon fa fa-reorder', '/log', '1', 'sys:log:query', '13',1 FROM DUAL WHERE NOT EXISTS (select * from sys_permission where id='26');
INSERT INTO sys_permission(id,parent_id,permission_name,css,href,type,permission,sort,enable) SELECT '27', '0', '邮件管理', 'menu-icon fa fa-envelope', '/mail', '1', '', '14',1 FROM DUAL WHERE NOT EXISTS (select * from sys_permission where id='27');
INSERT INTO sys_permission(id,parent_id,permission_name,css,href,type,permission,sort,enable) SELECT '28', '27', '发送邮件', '', '', '2', 'mail:send', '100',1 FROM DUAL WHERE NOT EXISTS (select * from sys_permission where id='28');
INSERT INTO sys_permission(id,parent_id,permission_name,css,href,type,permission,sort,enable) SELECT '29', '27', '查询', '', '', '2', 'mail:all:query', '100',1 FROM DUAL WHERE NOT EXISTS (select * from sys_permission where id='29');
INSERT INTO sys_permission(id,parent_id,permission_name,css,href,type,permission,sort,enable) SELECT '30', '0', '定时任务管理', 'menu-icon fa fa-tasks', '/job', '1', '', '15',1 FROM DUAL WHERE NOT EXISTS (select * from sys_permission where id='30');
INSERT INTO sys_permission(id,parent_id,permission_name,css,href,type,permission,sort,enable) SELECT '31', '30', '查询', '', '', '2', 'job:query', '100',1 FROM DUAL WHERE NOT EXISTS (select * from sys_permission where id='31');
INSERT INTO sys_permission(id,parent_id,permission_name,css,href,type,permission,sort,enable) SELECT '32', '30', '新增', '', '', '2', 'job:add', '100',1 FROM DUAL WHERE NOT EXISTS (select * from sys_permission where id='32');
INSERT INTO sys_permission(id,parent_id,permission_name,css,href,type,permission,sort,enable) SELECT '33', '30', '删除', '', '', '2', 'job:del', '100',1 FROM DUAL WHERE NOT EXISTS (select * from sys_permission where id='33');
INSERT INTO sys_permission(id,parent_id,permission_name,css,href,type,permission,sort,enable) SELECT '34', '0', 'webSocket', 'menu-icon fa fa-arrow-circle-down', '/wsabout', '1', '', '16',1 FROM DUAL WHERE NOT EXISTS (select * from sys_permission where id='34');
INSERT INTO sys_permission(id,parent_id,permission_name,css,href,type,permission,sort,enable) SELECT '35', '34', '导出', '', '', '2', 'excel:down', '100',1 FROM DUAL WHERE NOT EXISTS (select * from sys_permission where id='35');
INSERT INTO sys_permission(id,parent_id,permission_name,css,href,type,permission,sort,enable) SELECT '36', '34', '页面显示数据', '', '', '2', 'excel:show:datas', '100',1 FROM DUAL WHERE NOT EXISTS (select * from sys_permission where id='36');
INSERT INTO sys_permission(id,parent_id,permission_name,css,href,type,permission,sort,enable) SELECT '37', '0', '字典管理', 'menu-icon fa fa-reddit', '/dict', '1', '', '17',1 FROM DUAL WHERE NOT EXISTS (select * from sys_permission where id='37');
INSERT INTO sys_permission(id,parent_id,permission_name,css,href,type,permission,sort,enable) SELECT '38', '37', '查询', '', '', '2', 'dict:query', '100',1 FROM DUAL WHERE NOT EXISTS (select * from sys_permission where id='38');
INSERT INTO sys_permission(id,parent_id,permission_name,css,href,type,permission,sort,enable) SELECT '39', '37', '新增', '', '', '2', 'dict:add', '100',1 FROM DUAL WHERE NOT EXISTS (select * from sys_permission where id='39');
INSERT INTO sys_permission(id,parent_id,permission_name,css,href,type,permission,sort,enable) SELECT '40', '37', '删除', '', '', '2', 'dict:del', '100',1 FROM DUAL WHERE NOT EXISTS (select * from sys_permission where id='40');

-- 插入用户角色数据
INSERT INTO sys_user_role(user_id,role_id) SELECT 'sec_auditor', '1' FROM DUAL WHERE NOT EXISTS (select * from sys_user_role where user_id='sec_auditor' and role_id='1');
INSERT INTO sys_user_role(user_id,role_id) SELECT 'sys_admin', '2' FROM DUAL WHERE NOT EXISTS (select * from sys_user_role where user_id='sys_admin' and role_id='2');

-- 插入角色权限数据
--insert into sys_role_permission(role_id,permission_id) select '2',p.id from sys_permission p where p.id != '26';
--insert into sys_role_permission(role_id,permission_id) values('1', '26');
-- 提交数据
commit;