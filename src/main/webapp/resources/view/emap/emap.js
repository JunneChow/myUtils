const emap = Vue.component('emap',function (resole, reject) {
    $.get('resources/view/emap/emap.html').then(function (res) {
        resole({
            template:res,
            data:function () {
                return{
                    mapObj:null
                }
            },
            methods:{
                mapShow:function(){
                    let that = this;
                    /*1.基础设置*/
                    let point = new BMap.Point(113.268267, 23.132544); //默认地点
                    //设置地图中心点和放大级数
                    this.mapObj.centerAndZoom(point, 12);
                    this.mapObj.enableScrollWheelZoom();    //启用滚轮放大缩小，默认禁用
                    this.mapObj.enableContinuousZoom();    //启用地图惯性拖拽，默认禁用
                    /*2.控件设置*/
                    this.mapObj.addControl(new BMap.ScaleControl({anchor: BMAP_ANCHOR_BOTTOM_RIGHT}));                    // 右下
                    this.mapObj.addControl(new BMap.NavigationControl());  //添加默认缩放平移控件
                    this.mapObj.addControl(new BMap.NavigationControl({anchor: BMAP_ANCHOR_BOTTOM_RIGHT, type: BMAP_NAVIGATION_CONTROL_ZOOM}));  //右下角，仅包含缩放按钮
                    /*3. 3D2D地图*/
                    //添加3D/2D地图控件类型 start
                    this.mapObj.addControl(new BMap.MapTypeControl());          //添加地图类型控件
                    //添加3D/2D地图控件类型 end
                }
            },
            created:function(){

            },
            mounted:function (){
                this.mapObj = new BMap.Map("baiduMap");
                this.mapShow();
            },
            destroyed:function(){

            }
        });
    })
});