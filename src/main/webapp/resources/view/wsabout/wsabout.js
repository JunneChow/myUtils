const wsabout = Vue.component('wsabout',function (resole, reject) {
    $.get('resources/view/wsabout/wsabout.html').then(function (res) {
        resole({
            template:res,
            data:function () {
                return{
                    ws_client:null
                }
            },
            methods:{
                getRootPath:function(){
                    //获取当前网址，如： http://localhost:8088/test/test.jsp
                    let curPath = window.document.location.href;
                    //获取主机地址之后的目录，如： test/test.jsp
                    let pathName = window.document.location.pathname;
                    let pos = curPath.indexOf(pathName);
                    //获取主机地址，如： http://localhost:8088
                    let localhostPath = curPath.substring(curPath.indexOf("//"),pos);
                    //获取带"/"的项目名，如：/test
                    let projectName = pathName.substring(0, pathName.substr(1).indexOf('/') + 1);
                    return (localhostPath + projectName);//发布前用此
                },
                open_connection:function(){
                    let that = this;
                    let rootPath = this.getRootPath();
                    that.ws_client = new WebSocket("ws:"+rootPath+"/myWebSocketServer/normal");
                    that.ws_client.onopen = function () {
                        let msgData = "[/hello]";
                        that.sendMessage(msgData);
                    };
                    that.ws_client.onmessage = function (MessageEvent) {
                        if (MessageEvent != null && MessageEvent.data != null){
                            that.handleReceiveMessage(MessageEvent.data);
                        }
                    };
                    that.ws_client.onclose = function () {
                        that.closeWebSocket();
                    };
                },
                sendMessage:function (msgData) {
                    this.ws_client.send(msgData);
                },
                handleReceiveMessage:function(msgData){
                    console.log(msgData);
                },
                closeWebSocket:function () {
                    console.log("当前web_socket客户端已关闭连接！");
                }
            },
            created:function(){
                this.open_connection();
            },
            mounted:function (){

            },
            destroyed:function(){
                this.ws_client.close();
            }
        });
    })
});