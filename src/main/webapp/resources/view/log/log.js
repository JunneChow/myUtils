const log = Vue.component('log',function (resolve, reject) {
    $.get('resources/view/log/log.html').then(function (res) {
        resolve({
            template:res,
            data:function () {
                return{
                    loading:false,
                    screenHeight: null,
                    pageParamMap:{
                        $currentPage:0,
                        $pageSize:10,
                        $sort:"actionTime:desc",
                        startTime:null,
                        endTime:null
                    },
                    paginationShow:true,
                    sysLogDTOList:null,
                    totalElements:null
                }
            },
            methods:{
                query:function(){
                    let that = this;
                    this.pageParamMap.$currentPage = 0;
                    this.paginationShow = false;
                    this.getPageSysLog();
                    this.$nextTick(() => {
                        that.paginationShow = true;
                    })
                },
                handleCurrentChange:function(currentPage){
                    --currentPage;
                    this.pageParamMap.$currentPage = currentPage;
                    this.getPageSysLog();
                },
                getPageSysLog:function () {
                    this.loading = true;
                    let that = this;
                    Vue.getPageSysLog(this.$http,this.pageParamMap,function (response) {
                        let data = response.data;
                        if (data.resultCode === "0") {
                            if (data.resultData != null){
                                that.sysLogDTOList = data.resultData.content;
                                that.totalElements = data.resultData.totalElements;
                            }else {
                                that.sysLogDTOList = null;
                                that.totalElements = 0;
                            }
                            that.loading = false;
                        }else {
                            alert(data.resultMsg);
                        }
                    },function (err) {
                        console.log(err);
                    });
                },
                initScreenHeight:function(){
                    window.screenHeight = document.documentElement.clientHeight-224;
                    this.screenHeight = window.screenHeight;
                }
            },
            created:function(){
                this.initScreenHeight();
                this.getPageSysLog();
            },
            mounted:function (){
                let that = this;
                window.onresize = function () {
                    that.initScreenHeight();
                }
            },
            destroyed:function(){

            }
        });
    })
});