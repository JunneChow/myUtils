const student = Vue.component('student',function (resole, reject) {
    $.get('resources/view/student/student.html').then(function (res) {
        resole({
            template:res,
            data:function () {
                return{
                    loading:false,
                    screenHeight:null,
                    pageParamMap:{
                        $currentPage:0,
                        $pageSize:10,
                        $sort:"id"
                    },
                    studentVO:null,
                    studentVOList:null,
                    totalElements:null
                }
            },
            methods:{
                handleCurrentChange:function(currentPage){
                    --currentPage;
                    this.pageParamMap.$currentPage = currentPage;
                    this.getPageStudent();
                },
                getOneStudent:function (paramKey,paramValue) {
                    axios_instance.get('/student/'+paramKey+'/'+paramValue).then(function (res) {
                        console.log(res);
                    }).catch(function (error) {
                        console.log(error);
                    })
                },
                getPageStudent:function () {
                    let that = this;
                    axios_instance.post('/student/pageList',this.pageParamMap).then(function (res) {
                        let data = res.data;
                        if(data.resultCode === '0'){
                            that.studentVOList = data.resultData;
                            console.log(that.studentVOList);
                        }else {
                            that.$message.error(data.resultMsg);
                        }
                    }).catch(function (error) {
                        console.log(error);
                    })
                },
                addStudent:function () {
                    axios_instance.post('/student/add',this.studentVO).then(function (res) {
                        console.log(res);
                    }).catch(function (error) {
                        console.log(error);
                    })
                },
                updateStudent:function () {
                    axios_instance.put('/student/update',this.studentVO).then(function (res) {
                        console.log(res);
                    }).catch(function (error) {
                        console.log(error);
                    })
                },
                deleteStudent:function (id) {
                    axios_instance.delete('/student/delete?id='+id).then(function (res) {
                        console.log(res);
                    }).catch(function (error) {
                        console.log(error);
                    })
                },
                initScreenHeight:function(){
                    window.screenHeight = document.documentElement.clientHeight-224;
                    this.screenHeight = window.screenHeight;
                }
            },
            created:function(){
                this.initScreenHeight();
                this.getPageStudent();
            },
            mounted:function (){
                let that = this;
                window.onresize = function () {
                    that.initScreenHeight();
                }
            },
            destroyed:function(){

            }
        });
    })
});