Vue.use(appService);
const user = Vue.component('user', function (resole, reject) {
    $.get('resources/view/user/user.html').then(function (res) {
        resole({
            template: res,
            data: function () {
                return {
                    loading:false,
                    screenHeight:null,
                    editTitle:"编辑系统用户",
                    editSysUserDialogVisible:false,
                    userSysRoleIdList:null,
                    allSysRoleList:null,
                    checkAllRole:false,
                    isIndeterminate:true,
                    pageParamMap:{
                        $currentPage:0,
                        $pageSize:10,
                        $sort:"id",
                        userName:null,
                        nickName:null,
                        sysRoleId:null
                    },
                    sysUserDTO:{
                        id:null,
                        userName:null,
                        nickName:null,
                        password:null,
                        roleName:null,
                        updateTime:null
                    },
                    sysUserDTOList:null,
                    totalElements:null
                }
            },
            methods: {
                handleCheckAllRole:function(val){
                    if(val){
                        for (let i=0;i<this.allSysRoleList.length;i++){
                            this.userSysRoleIdList.push(this.allSysRoleList[i].id);
                        }
                    }else{
                        this.userSysRoleIdList = [];
                    }
                    this.isIndeterminate = false;
                },
                handleCheckedRole:function(value){
                    let checkedCount = value.length;
                    if (checkedCount > 0){
                        if (checkedCount === this.userSysRoleIdList.length) {
                            this.checkAllRole = true;
                            this.isIndeterminate = checkedCount > 0 && checkedCount < this.allSysRoleList.length;
                        }
                    } else {
                        this.checkAllRole = false;
                        this.isIndeterminate = false;
                    }
                },
                handleCurrentChange:function(currentPage){
                    --currentPage;
                    this.pageParamMap.$currentPage = currentPage;
                    this.getPageSysUser();
                },
                initEditSysUserDialog:function(){
                    this.sysUserDTO = {
                        id:null,
                        userName:null,
                        nickName:null,
                        password:null,
                        roleName:null,
                        updateTime:null
                    };
                    this.userSysRoleIdList = [];
                    this.checkAllRole = false;
                    this.isIndeterminate = true;
                },
                handleAdd:function(){
                    this.loading = true;
                    this.editTitle = "添加系统用户";
                    this.initEditSysUserDialog();
                    this.editSysUserDialogVisible = true;
                    this.loading = false;
                },
                edit:function(id){
                    this.loading = true;
                    this.editTitle = "编辑系统用户";
                    this.initEditSysUserDialog();
                    let that = this;
                    this.getOneSysUser("id",id).then(function (sysUserDTO) {
                        that.getSysRoleListBySysUserId(id).then(function (userSysRoleIdList) {
                            that.editSysUserDialogVisible = true;
                            that.loading = false;
                        });
                    });
                },
                del:function(id){
                    alert(id);
                },
                getSysRoleListBySysUserId:function(sysUserId){
                    let that = this;
                    return new Promise(function (resolve,reject) {
                        Vue.getSysRoleListBySysUserId(that.$http,sysUserId,function (response) {
                            let data = response.data;
                            if (data.resultCode === "0") {
                                that.userSysRoleIdList = [];
                                let arr = data.resultData;
                                for (let i=0;i<arr.length;i++){
                                    that.userSysRoleIdList.push(arr[i].id);
                                }
                                resolve(data.resultData);
                            }else {
                                alert(data.resultMsg);
                            }
                        },function (err) {
                            console.log(err);
                        });
                    });
                },
                getAllSysRoleList:function(){
                    let that = this;
                    return new Promise(function (resolve,reject) {
                        Vue.getAllSysRoleList(that.$http,function (response) {
                            let data = response.data;
                            if (data.resultCode === "0") {
                                that.allSysRoleList = data.resultData;
                                resolve(data.resultData);
                            }else {
                                alert(data.resultMsg);
                            }
                        },function (err) {
                            console.log(err);
                        });
                    });
                },
                getOneSysUser:function (paramKey,paramValue) {
                    let that = this;
                    return new Promise(function (resolve,reject) {
                        Vue.getOneSysUser(that.$http,paramKey,paramValue,function (response) {
                            let data = response.data;
                            if (data.resultCode === "0") {
                                that.sysUserDTO = data.resultData;
                                resolve(data.resultData);
                            }else {
                                alert(data.resultMsg);
                            }
                        },function (err) {
                            console.log(err);
                        });
                    });
                },
                getPageSysUser:function () {
                    this.loading = true;
                    let that = this;
                    Vue.getPageSysUser(this.$http,this.pageParamMap,function (response) {
                        let data = response.data;
                        if (data.resultCode === "0") {
                            if (data.resultData != null){
                                that.sysUserDTOList = data.resultData.content;
                                that.totalElements = data.resultData.totalElements;
                            }else {
                                that.sysUserDTOList = null;
                                that.totalElements = 0;
                            }
                            that.loading = false;
                        }else {
                            alert(data.resultMsg);
                        }
                    },function (err) {
                        console.log(err);
                    });
                },
                addSysUser:function () {
                    let that = this;
                    Vue.addSysUser(this.$http,this.sysUserDTO,function (response) {
                        let data = response.data;
                        if (data.resultCode === "0") {
                            that.sysUserDTO = data.resultData;
                        }else {
                            alert(data.resultMsg);
                        }
                    },function (err) {
                        console.log(err);
                    });
                },
                updateSysUser:function () {
                    let that = this;
                    Vue.updateSysUser(this.$http,this.sysUserDTO,function (response) {
                        let data = response.data;
                        if (data.resultCode === "0") {
                            that.sysUserDTO = data.resultData;
                        }else {
                            alert(data.resultMsg);
                        }
                    },function (err) {
                        console.log(err);
                    });
                },
                deleteSysUser:function (id) {
                    Vue.deleteSysUser(this.$http,id,function (response) {
                        let data = response.data;
                        if (data.resultCode === "0") {
                            alert(data.resultMsg);
                        }else {
                            alert(data.resultMsg);
                        }
                    },function (err) {
                        console.log(err);
                    });
                },
                initScreenHeight:function(){
                    window.screenHeight = document.documentElement.clientHeight-224;
                    this.screenHeight = window.screenHeight;
                }
            },
            created: function () {
                this.initScreenHeight();
                this.getAllSysRoleList();
                this.getPageSysUser();
            },
            mounted: function () {
                let that = this;
                window.onresize = function () {
                    that.initScreenHeight();
                }
            },
            destroyed: function () {

            }
        });
    })
});