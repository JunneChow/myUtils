const notice = Vue.component('notice',function (resole, reject) {
    $.get('resources/view/notice/notice.html').then(function (res) {
        resole({
            template:res,
            data:function () {
                return{
                    geoMapChart:null,
                    lineChart:null,
                    doughnutChart:null
                }
            },
            methods:{
                initGeoMapChart:function () {
                    let that = this;
                    this.geoMapChart = echarts.init(document.getElementById("geoMapChart"));
                    this.geoMapChart.showLoading();
                    $.get('resources/json/guangdon_geo.json',function (geoJson) {
                        that.geoMapChart.hideLoading();
                        echarts.registerMap('GD', geoJson);
                        that.geoMapChart.setOption({
                            title: {
                                text: '广东地图',
                                textStyle: {
                                    fontWeight: "normal",
                                    color: "#fffb0f",
                                    fontSize: 16
                                }
                            },
                            tooltip: {
                                trigger: 'item'
                            },
                            series: [{
                                name: '广东地图',
                                type: 'map',
                                mapType: 'GD', // 自定义扩展图表类型
                                itemStyle: {
                                    normal: {
                                        areaColor: '#2A3E61',
                                        borderColor: '#6C99DD',
                                    },
                                    emphasis: {
                                        areaColor: '#6C99DD',
                                        label: { show: true }
                                    }
                                },
                                label: {
                                    emphasis: {
                                        color: 'white',
                                        fontSize: 15,
                                        fontWeight: 'bold'
                                    }
                                },
                            }]
                        });
                    });
                },
                initLineChart:function () {
                    this.lineChart = echarts.init(document.getElementById("lineChart"));
                    this.lineChart.showLoading();
                    this.lineChart.setOption({
                        title: {
                            text: '客流统计',
                            textStyle: {
                                fontWeight: "normal",
                                color: "#fffb0f",
                                fontSize: 16
                            }
                        },
                        color:'#6C99DD',
                        xAxis: {
                            type: 'category',
                            boundaryGap: false,
                            data: ['1', '2', '3', '4', '5', '6', '7','8','9','10','11','12'],
                            axisLabel: {
                                textStyle: {
                                    color: '#6C99DD'
                                }
                            }
                        },
                        yAxis: {
                            type: 'value',
                            axisLabel: {
                                textStyle: {
                                    color: '#6C99DD'
                                }
                            },
                            splitLine: {
                                lineStyle: {
                                    color: 'gray'
                                },
                                width:0.3
                            }
                        },
                        series: [{
                            data: [0, 0, 0, 0, 0, 160, 250, 500, 1000, 1300, 1900, 1400],
                            type: 'line',
                            smooth:true,
                            symbol: 'none',
                            areaStyle: {
                                color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                                    offset: 0,
                                    color: '#6C99DD'
                                }, {
                                    offset: 1,
                                    color: '#252D45'
                                }])
                            }
                        }]
                    });
                    this.lineChart.hideLoading();
                }
            },
            created:function(){

            },
            mounted:function (){
                this.initGeoMapChart();
                this.initLineChart();
            },
            destroyed:function(){

            }
        });
    })
});