const job = Vue.component('job',function (resole, reject) {
    $.get('resources/view/job/job.html').then(function (res) {
        resole({
            template:res,
            data:function () {
                return{
                    loading:false,
                    screenHeight: null,
                    editTitle:"编辑系统作业",
                    editSysJobDialogVisible:false,
                    isSysJobEdit:false,
                    pageParamMap:{
                        $currentPage:0,
                        $pageSize:10,
                        $sort:"jobName:desc",
                        jobName:null,
                        springBeanName:null,
                        methodName:null
                    },
                    sysJobDTO:{
                        id:null,
                        jobName:null,
                        description:null,
                        cron:null,
                        springBeanName:null,
                        methodName:null,
                        status:null
                    },
                    paginationShow:true,
                    sysJobDTOList:null,
                    totalElements:null
                }
            },
            methods:{
                query:function(){
                    let that = this;
                    this.pageParamMap.$currentPage = 0;
                    this.paginationShow = false;
                    this.getPageSysJob();
                    this.$nextTick(() => {
                        that.paginationShow = true;
                    })
                },
                handleCurrentChange:function(currentPage){
                    --currentPage;
                    this.pageParamMap.$currentPage = currentPage;
                    this.getPageSysLog();
                },
                statusFormat:function(row) {
                    if (row.status === "1") {
                        return "正在运行";
                    } else  {
                        return "已经停止"
                    }
                },
                initEditSysJobDialog:function(){
                    this.sysJobDTO = {
                        id:null,
                        jobName:null,
                        description:null,
                        cron:null,
                        springBeanName:null,
                        methodName:null,
                        status:null
                    };
                },
                handleAdd:function(){
                    this.loading = true;
                    this.editTitle = "添加系统作业";
                    this.isSysJobEdit = false;
                    this.initEditSysJobDialog();
                    this.editSysJobDialogVisible = true;
                    this.loading = false;
                },
                handleEdit:function(index, row){
                    this.loading = true;
                    this.editTitle = "编辑系统作业";
                    this.isSysJobEdit = true;
                    this.initEditSysJobDialog();
                    let that = this;
                    this.getOneSysJob("id",row.id).then(function (sysJobDTO) {
                        that.editSysJobDialogVisible = true;
                        that.loading = false;
                    });
                },
                saveOrUpdate:function(){
                    this.loading = true;
                    if (this.isSysJobEdit) {
                        this.updateSysJob();
                    }else {
                        this.addSysJob();
                    }
                    this.editSysJobDialogVisible = false;
                    this.loading = false;
                },
                handleStart:function(index, row){
                    this.loading = true;
                    let that = this;
                    this.startSysJob(row.id).then(function (sysJobDTO) {
                        that.getPageSysJob();
                        that.loading = false;
                    });
                },
                handleStop:function(index, row){
                    this.loading = true;
                    let that = this;
                    this.stopSysJob(row.id).then(function (sysJobDTO) {
                        that.getPageSysJob();
                        that.loading = false;
                    });
                },
                handleDelete:function(index, row){
                    this.deleteSysJob(row.id);
                },
                getOneSysJob:function (paramKey,paramValue) {
                    let that = this;
                    return new Promise(function (resolve,reject) {
                        Vue.getOneSysJob(that.$http,paramKey,paramValue,function (response) {
                            let data = response.data;
                            if (data.resultCode === "0") {
                                that.sysJobDTO = data.resultData;
                                resolve(data.resultData);
                            }else {
                                alert(data.resultMsg);
                            }
                        },function (err) {
                            console.log(err);
                        });
                    });
                },
                getPageSysJob:function () {
                    this.loading = true;
                    let that = this;
                    Vue.getPageSysJob(this.$http,this.pageParamMap,function (response) {
                        let data = response.data;
                        if (data.resultCode === "0") {
                            if (data.resultData != null){
                                that.sysJobDTOList = data.resultData.content;
                                that.totalElements = data.resultData.totalElements;
                            }else {
                                that.sysJobDTOList = null;
                                that.totalElements = 0;
                            }
                            that.loading = false;
                        }else {
                            alert(data.resultMsg);
                        }
                    },function (err) {
                        console.log(err);
                    });
                },
                addSysJob:function () {
                    let that = this;
                    Vue.addSysJob(this.$http,this.sysJobDTO,function (response) {
                        let data = response.data;
                        if (data.resultCode === "0") {
                            that.sysJobDTO = data.resultData;
                            that.$nextTick(function () {
                                that.getPageSysJob();
                            })
                        }else {
                            alert(data.resultMsg);
                        }
                    },function (err) {
                        console.log(err);
                    });
                },
                updateSysJob:function () {
                    let that = this;
                    Vue.updateSysJob(this.$http,this.sysJobDTO,function (response) {
                        let data = response.data;
                        if (data.resultCode === "0") {
                            that.sysJobDTO = data.resultData;
                            that.$nextTick(function () {
                                that.getPageSysJob();
                            })
                        }else {
                            alert(data.resultMsg);
                        }
                    },function (err) {
                        console.log(err);
                    });
                },
                deleteSysJob:function (id) {
                    let that = this;
                    Vue.deleteSysJob(this.$http,id,function (response) {
                        let data = response.data;
                        if (data.resultCode === "0") {
                            that.$nextTick(function () {
                                that.getPageSysJob();
                            })
                        }else {
                            alert(data.resultMsg);
                        }
                    },function (err) {
                        console.log(err);
                    });
                },
                startSysJob:function (id) {
                    let that = this;
                    return new Promise(function (resolve,reject) {
                        Vue.startSysJob(that.$http,id,function (response) {
                            let data = response.data;
                            if (data.resultCode === "0") {
                                that.sysJobDTO = data.resultData;
                                resolve(data.resultData);
                            }else {
                                alert(data.resultMsg);
                            }
                        },function (err) {
                            console.log(err);
                        });
                    });
                },
                stopSysJob:function (id) {
                    let that = this;
                    return new Promise(function (resolve,reject) {
                        Vue.stopSysJob(that.$http,id,function (response) {
                            let data = response.data;
                            if (data.resultCode === "0") {
                                that.sysJobDTO = data.resultData;
                                resolve(data.resultData);
                            }else {
                                alert(data.resultMsg);
                            }
                        },function (err) {
                            console.log(err);
                        });
                    });
                },
                initScreenHeight:function(){
                    window.screenHeight = document.body.clientHeight-224;
                    this.screenHeight = window.screenHeight;
                }
            },
            created:function(){
                this.initScreenHeight();
                this.getPageSysJob();
            },
            mounted:function (){
                let that = this;
                window.onresize = function () {
                    that.initScreenHeight();
                }
            },
            destroyed:function(){

            }
        });
    })
});