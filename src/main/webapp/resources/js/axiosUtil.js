function getRootPath() {
    //获取当前网址，如： http://localhost:8088/test/test.jsp
    let curPath = window.document.location.href;
    //获取主机地址之后的目录，如： test/test.jsp
    let pathName = window.document.location.pathname;
    let pos = curPath.indexOf(pathName);
    //获取主机地址，如： http://localhost:8088
    let localhostPath = curPath.substring(0, pos);
    //获取带"/"的项目名，如：/test
    let projectName = pathName.substring(0, pathName.substr(1).indexOf('/') + 1);
    return (localhostPath + projectName);//发布前用此
}
// 是否允许跨域
axios.defaults.withCredentials=true;
// axios初始化：延迟时间，主路由地址
const axios_instance = axios.create({
    baseURL: getRootPath()
});

//请求拦截器
axios_instance.interceptors.request.use(function(config){
    //设置api请求头部信息
    config.headers['Api-Request'] = 'yes';
    return config;
},function(error){
    //请求错误时做些事
    console.log(error);
    return Promise.reject(error);
});
//响应拦截器
axios_instance.interceptors.response.use(function(response){
    //设置响应处理
    if (response.status === 200) {
        if (response.data.resultCode==="401") {
            alert(response.data.resultMsg);
            window.location.replace("login.html");
        }else if (response.data.resultCode==="302"){
            alert(response.data.resultMsg);
            window.location.replace(response.data.resultData);
        }else {
            return response;
        }
    }
},function(error){
    let response = error.response;
    if (response.status === 302 && response.data.resultCode==="302"){
        alert(response.data.resultMsg);
        window.location.replace(response.data.resultData);
    }else if (response.status === 401) {
        alert("当前用户未登录或登录已失效！");
        window.location.replace("login.html");
    }else {
        //请求错误时做些事
        console.log(error);
        alert("后台服务异常,请联系管理员！");
        return Promise.reject(error);
    }
});

// 是否销毁拦截器
// 1.给拦截器起个名称 var myInterceptors = axios_instance.interceptors.requesst.use();
// 2.axios_instance.interceptors.request.eject(myInterceptor);