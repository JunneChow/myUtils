Vue.prototype.$http = axios_instance;
Vue.use(appService);

const app = new Vue({
    el:'#app',
    data:function () {
        return{
            accountInfo:{
                account: "",
                password: ""
            },
            rememberMe: false,
            verifyCode: '',
            verifyCodeImgSrc: null
        }
    },
    methods: {
        getVerifyCode:function(){
            let that = this;
            return new Promise(function (resolve, reject) {
                Vue.getVerifyCode(that.$http,0,function (res) {
                    resolve(res);
                },function (err) {
                    console.log(err);
                });
            });
        },
        changeVerifyCode: function () {
            let that = this;
            this.getVerifyCode().then(function (res) {
                that.verifyCodeImgSrc = res.data.resultData;
                that.verifyCode = null;
            });
        },
        getRsaPublicKey:function(){
            let that = this;
            return new Promise(function (resolve, reject) {
                Vue.getRsaPublicKey(that.$http,function (res) {
                    let data = res.data;
                    if (data.resultCode === "0") {
                        resolve(data.resultData);
                    }else {
                        console.log(data.resultMsg);
                    }
                },function (err) {
                    console.log(err);
                })
            });
        },
        login:function () {
            let that = this;
            this.getRsaPublicKey().then(function (rsaData) {
                let account = that.accountInfo.account;
                let rsaEncrypt = new JSEncrypt();
                rsaEncrypt.setPublicKey(rsaData);
                let password = rsaEncrypt.encrypt(that.accountInfo.password);
                Vue.login(that.$http,account,password,that.verifyCode,function (res) {
                    let data = res.data;
                    if (data.resultCode === "0") {
                        if (that.rememberMe){
                            window.localStorage.setItem("accountInfo", JSON.stringify(that.accountInfo));
                        } else {
                            window.localStorage.removeItem("accountInfo");
                        }
                        console.log(data.resultData);
                        window.location.href = "index.html";
                    }else {
                        that.changeVerifyCode();
                        console.log(data.resultMsg);
                        alert(data.resultMsg);
                    }
                },function (err) {
                    that.changeVerifyCode();
                    console.log(err);
                })
            });
        }
    },
    created:function (){
        let accountInfo = JSON.parse(window.localStorage.getItem("accountInfo"));
        if (accountInfo != null){
            this.accountInfo = accountInfo;
            this.rememberMe = true;
        }
        this.changeVerifyCode();
    },
    mounted:function (){

    },
    destroy:function (){

    }
});