Vue.use(VueRouter);
Vue.use(appService);
Vue.prototype.$http = axios_instance;
Vue.prototype.$echarts = echarts;

// 组件传递
const eventBus = new Vue();

Vue.component('menu-list', {
    template: `
        <li v-bind:id="menu.id" class="">
            <a v-on:click="selectMenu(menu)" v-bind:class="menu.childList.length>0?'dropdown-toggle':''">
                <i v-bind:class="menu.css"></i>
                <span class="menu-text">{{menu.permissionName}}</span>
            </a>
            <b class="arrow"></b>
            <ul v-if="menu.childList.length>0" class="submenu">
                <menu-list v-for="subMenu in menu.childList" v-bind:menu="subMenu" v-bind:key="subMenu.id"></menu-list>
            </ul>
        </li>`,
    props: ['menu'],
    data:function () {
        return{

        }
    },
    methods:{
        selectMenu:function (menu) {
            $("li").removeClass("active");
            $("#"+menu.id).addClass("active");
            eventBus.$emit('selectMenu',menu);
        }
    }
});

const routes = [
    {path:'/user',component:user},
    {path:'/password',component:password},
    {path:'/menu',component:menu},
    {path:'/role',component:role},
    {path:'/emap',component:emap},
    {path:'/generate',component:generate},
    {path:'/notice',component:notice},
    {path:'/log',component:log},
    {path:'/mail',component:mail},
    {path:'/job',component:job},
    {path:'/wsabout',component:wsabout},
    {path:'/dict',component:dict},
    {path:'/other',component:other}
];

const router = new VueRouter({
    routes:routes
});

const app = new Vue({
    router:router,
    el:'#app',
    data:function () {
        return{
            menuList:null,
            menu:null,
            currentMenu:null,
            isShowRouterPage:true,
            iframeSrc:null,
            sysUser:{
                id:null,
                userName:null,
                nickname:null
            },
            ws_client:null
        }
    },
    methods: {
        getRootPath:function() {
            //获取当前网址，如： http://localhost:8088/test/test.jsp
            let curPath = window.document.location.href;
            //获取主机地址之后的目录，如： test/test.jsp
            let pathName = window.document.location.pathname;
            let pos = curPath.indexOf(pathName);
            //获取主机地址，如： http://localhost:8088
            let localhostPath = curPath.substring(0, pos);
            //获取带"/"的项目名，如：/test
            let projectName = pathName.substring(0, pathName.substr(1).indexOf('/') + 1);
            return (localhostPath + projectName);//发布前用此
        },
        openWebSocket:function(){
            let that = this;
            let rootPath = this.getRootPath();
            this.isLogin().then(function (sysUser) {
                let userId = sysUser.id;
                rootPath = rootPath.substr(rootPath.indexOf("//"),rootPath.length);
                that.ws_client = new WebSocket("ws:"+rootPath+"/myWebSocketServer/"+userId);
                that.ws_client.onopen = function () {
                    let msgData = "[/hello]";
                    that.sendMessage(msgData);
                };
                that.ws_client.onmessage = function (MessageEvent) {
                    if (MessageEvent != null && MessageEvent.data != null){
                        that.handleReceiveMessage(MessageEvent.data);
                    }
                };
                that.ws_client.onclose = function () {
                    that.closeWebSocket();
                };
            }).catch(function () {

            });
        },
        sendMessage:function (msgData) {
            this.ws_client.send(msgData);
        },
        handleReceiveMessage:function(msgData){
            if (msgData.indexOf("{") === 0 && msgData.indexOf("}")>0) {
                let msgObject = JSON.parse(msgData);
                if (msgObject != null && msgObject.resultCode === '1') {
                    this.$alert(msgObject.resultMsg, '系统提示', {
                        confirmButtonText: '确定',
                        callback: action = function () {
                            location.replace("login.html");
                        }
                    });
                }
            }
        },
        closeWebSocket:function () {
            console.log("当前登录用户的web_socket客户端已关闭连接！");
        },
        getMenuList:function(){
            let that = this;
            return new Promise(function (resolve,reject) {
                Vue.getMenuList(that.$http,function (res) {
                    let data = res.data;
                    if (data.resultCode === "0") {
                        that.menuList = res.data.resultData;
                        if (that.menuList != null && that.menuList.length>0){
                            let targetMenu = that.menuList[0];
                            while (targetMenu.childList != null && targetMenu.childList.length>0){
                                targetMenu = targetMenu.childList[0];
                                if (targetMenu.childList.length===0) {
                                    break;
                                }
                            }
                            that.goToPage(targetMenu.href);
                            that.currentMenu = targetMenu;
                        }
                        resolve(that.menuList);
                    }else {
                        alert(data.resultMsg);
                    }
                },function (err) {
                    console.log(err);
                });
            });
        },
        selectMenu:function(menu){
            if (menu.href != null && menu.href.length > 0){
                this.currentMenu = menu;
            }
            if(menu != null && menu.href != null && menu.childList.length===0){
                this.goToPage(menu.href);
            }
        },
        checkIfExistInRoutes:function(path){
            if ("other" === path){
                return false;
            }
            let isExist = false;
            routes.some(function (value,index) {
                if (value.path === path){
                    isExist = true;
                    return isExist;
                }
            });
            return isExist;
        },
        goToPage:function (path) {
            let currentPath = router.history.current.path;
            if(path !== currentPath){
                if(this.checkIfExistInRoutes(path)){
                    this.isShowRouterPage = true;
                    router.push(path).catch(function (err) {
                        console.log(err);
                    });
                }else {
                    this.isShowRouterPage = false;
                    if("/other" !== currentPath){
                        router.push("/other").catch(function (err) {
                            console.log(err);
                        });
                    }
                    let rootPath = this.getRootPath();
                    this.iframeSrc = rootPath+path;
                }
            }
        },
        isLogin:function(){
            let that = this;
            return new Promise(function (resolve,reject){
                Vue.isLogin(that.$http,function (res) {
                    let data = res.data;
                    if (data.resultCode === "0") {
                        that.sysUser = data.resultData;
                        resolve(that.sysUser);
                    }else {
                        window.location.href = "index.html";
                    }
                },function (err) {
                    console.log(err);
                });
            });
        },
        logout:function () {
            let that = this;
            location.replace("login.html");
            Vue.logout(this.$http,this.sysUser.id,function (res) {
                let data = res.data;
                if (data.resultCode === "0") {
                    that.sysUser = null;
                }else {
                    alert(data.resultMsg);
                }
            },function (err) {
                console.log(err);
            });
        }
    },
    created:function (){
        let that = this;
        this.openWebSocket();
        this.getMenuList();
        //绑定菜单点击事件
        eventBus.$on('selectMenu',function (menu) {
            that.selectMenu(menu);
        });
    },
    mounted:function (){

    },
    destroy:function (){

    }
});