const qs = Qs;
const appService = {};
appService.install = function (Vue) {

    //=================================公共接口-开始=================================
    //获取验证码
    Vue.getVerifyCode = function($http,verifyCodeType,successFn,errorFn){
        $http.get('/common/getVerifyCode?verifyCodeType='+verifyCodeType).then(
            successFn, errorFn
        );
    };

    //获取RSA加密公钥
    Vue.getRsaPublicKey = function ($http, successFn, errorFn) {
        $http.get('/common/getRsaPublicKey').then(
            successFn, errorFn
        );
    };

    //是否登录接口
    Vue.isLogin = function ($http, successFn, errorFn) {
        $http.get('/common/isLogin').then(
            successFn, errorFn
        );
    };

    //登录接口
    Vue.login = function($http,account,password,verifyCode,successFn,errorFn){
        let params = {
            account:account,
            password:password,
            verifyCode:verifyCode
        };
        $http.post('/common/login',qs.stringify(params)).then(
            successFn, errorFn
        );
    };

    //登出接口
    Vue.logout = function ($http, id, successFn, errorFn) {
        $http.get('common/logout?id=' + id).then(
            successFn, errorFn
        );
    };

    //获取系统菜单
    Vue.getMenuList = function ($http,successFn,errorFn) {
        $http.get('/common/menuList').then(
            successFn, errorFn
        );
    };
    //=================================公共接口-结束=================================
    //=================================系统角色接口-开始=================================
    //获取系统角色列表
    Vue.getAllSysRoleList = function ($http,successFn,errorFn) {
        $http.get('/sysRole/list').then(
            successFn, errorFn
        );
    };

    //按系统用户ID获取系统角色列表
    Vue.getSysRoleListBySysUserId = function ($http,sysUserId,successFn,errorFn) {
        $http.get('/sysRole/list?sysUserId='+sysUserId).then(
            successFn, errorFn
        );
    };
    //=================================系统角色接口-结束=================================
    //=================================系统权限接口-开始=================================
    //获取系统权限列表
    Vue.getAllSysPermissionList = function ($http,successFn,errorFn) {
        $http.get('/sysPermission/list').then(
            successFn, errorFn
        );
    };

    //按系统用户ID获取系统权限列表
    Vue.getSysPermissionListBySysUserId = function ($http,sysUserId,successFn,errorFn) {
        $http.get('/sysPermission/list?sysUserId='+sysUserId).then(
            successFn, errorFn
        );
    };
    //=================================系统权限接口-结束=================================
    //=================================系统用户接口-开始=================================
    Vue.getOneSysUser = function ($http,paramKey,paramValue,successFn,errorFn) {
        $http.get('/sysUser/'+paramKey+'/'+paramValue).then(
            successFn, errorFn
        );
    };

    Vue.getPageSysUser = function ($http,pageParamMap,successFn,errorFn) {
        $http.post('/sysUser/pageList',pageParamMap).then(
            successFn, errorFn
        );
    };

    Vue.addSysUser = function ($http,sysUserDTO,successFn,errorFn) {
        $http.post('/sysUser/add',sysUserDTO).then(
            successFn, errorFn
        );
    };

    Vue.updateSysUser = function ($http,sysUserDTO,successFn,errorFn) {
        $http.put('/sysUser/update',sysUserDTO).then(
            successFn, errorFn
        );
    };

    Vue.deleteSysUser = function ($http,id,successFn,errorFn) {
        $http.delete('/sysUser/delete?id='+id).then(
            successFn, errorFn
        );
    };
    //=================================系统用户接口-结束=================================
    //=================================系统日志接口-开始=================================
    Vue.getPageSysLog = function ($http,pageParamMap,successFn,errorFn) {
        $http.post('/sysLog/pageList',pageParamMap).then(
            successFn, errorFn
        );
    };
    //=================================系统日志接口-结束=================================
    //=================================系统作业接口-开始=================================
    Vue.getOneSysJob = function ($http,paramKey,paramValue,successFn,errorFn) {
        $http.get('/sysJob/'+paramKey+'/'+paramValue).then(
            successFn, errorFn
        );
    };

    Vue.getPageSysJob = function ($http,pageParamMap,successFn,errorFn) {
        $http.post('/sysJob/pageList',pageParamMap).then(
            successFn, errorFn
        );
    };

    Vue.addSysJob = function ($http,sysUserDTO,successFn,errorFn) {
        $http.post('/sysJob/add',sysUserDTO).then(
            successFn, errorFn
        );
    };

    Vue.updateSysJob = function ($http,sysUserDTO,successFn,errorFn) {
        $http.put('/sysJob/update',sysUserDTO).then(
            successFn, errorFn
        );
    };

    Vue.deleteSysJob = function ($http,id,successFn,errorFn) {
        $http.delete('/sysJob/delete?id='+id).then(
            successFn, errorFn
        );
    };

    Vue.startSysJob = function ($http,id,successFn,errorFn) {
        $http.get('/sysJob/start?id='+id).then(
            successFn, errorFn
        );
    };

    Vue.stopSysJob = function ($http,id,successFn,errorFn) {
        $http.get('/sysJob/stop?id='+id).then(
            successFn, errorFn
        );
    };
    //=================================系统作业接口-结束=================================
};